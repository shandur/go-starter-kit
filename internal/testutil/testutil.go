package testutil

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/stretchr/testify/require"

	"gitlab.mooncascade.net/roman/go-starter-kit/internal/handler"
	"gitlab.mooncascade.net/roman/go-starter-kit/internal/uuid"
)

// MakeRequest makes a test HTTP request, asserting no errors are encountered
// along the way. Returns status code and response body (or nil).
func MakeRequest(
	t *testing.T, ts *httptest.Server, method, path string, body *string,
) (int, handler.Response) {
	return doRequest(context.Background(), t, ts, nil, method, path, body)
}

// MakeRequestRaw is similar to MakeRequest except
// it accepts `http.Request` and returns raw `http.ResponseWriter` along with `handler.Response`.
func MakeRequestRaw(
	t *testing.T, ts *httptest.Server, req *http.Request,
) (int, handler.Response, *http.Response) {
	// Perform request
	resp, err := http.DefaultClient.Do(req)
	require.NoError(t, err)

	// Parse response
	respBody, err := io.ReadAll(resp.Body)
	require.NoError(t, err)
	defer resp.Body.Close()

	var responseWrapper handler.Response
	if len(respBody) > 0 {
		err = json.Unmarshal(respBody, &responseWrapper)
		require.NoError(t, err)
	}

	return resp.StatusCode, responseWrapper, resp
}

// MakeRequestWithHeaders is the same as MakeRequest except
// it accepts request headers.
func MakeRequestWithHeaders(
	t *testing.T, ts *httptest.Server, method, path string, body *string, header http.Header,
) (int, handler.Response) {
	// Create request
	var requestBody io.Reader
	if body != nil {
		requestBody = strings.NewReader(*body)
	}
	req, err := http.NewRequest(
		method, fmt.Sprintf("%s%s", ts.URL, path), requestBody,
	)
	require.NoError(t, err)

	req.Header = header

	// Perform request
	resp, err := http.DefaultClient.Do(req)
	require.NoError(t, err)

	// Parse response
	respBody, err := io.ReadAll(resp.Body)
	require.NoError(t, err)
	defer resp.Body.Close()
	var responseWrapper handler.Response
	if len(respBody) > 0 {
		err = json.Unmarshal(respBody, &responseWrapper)
		require.NoError(t, err)
	}

	return resp.StatusCode, responseWrapper
}

// MustUUID ensures UUID is generated.
// It calls `t.Fatalf()` if error
func MustUUID(t *testing.T) string {
	uuid, err := uuid.NewRandom()

	if err != nil {
		t.Fatalf("MustUUID failed: %v", err)
	}

	return uuid
}

// EncodeRequest encodes the given struct into JSON and returns as a string.
// It calls `t.Fatal` on error.
func EncodeRequest(t *testing.T, i interface{}) string {
	t.Helper()

	raw, err := json.Marshal(i)
	if err != nil {
		t.Fatalf("failed to encode handler request: %v", err)
	}

	return string(raw)
}

// DecodeResponse decodes the given (JSON) byte sequence and returns as `handler.Response`.
// It calls `t.Fatal` on error.
func DecodeResponse(t *testing.T, raw []byte) handler.Response {
	t.Helper()

	var response handler.Response
	err := json.Unmarshal(raw, &response)

	if err != nil {
		t.Fatalf("failed to decode handler response: %v", err)
	}

	return response
}

// doRequest is an internal helper function to make a test HTTP request, asserting no errors are encountered
// along the way. Returns status code and response body (or nil).
func doRequest(
	ctx context.Context,
	t *testing.T,
	ts *httptest.Server,
	header http.Header,
	method,
	path string,
	body *string,
) (int, handler.Response) {
	// Create request
	var requestBody io.Reader
	if body != nil {
		requestBody = strings.NewReader(*body)
	}

	req, err := http.NewRequestWithContext(ctx, method, fmt.Sprintf("%s%s", ts.URL, path), requestBody)
	require.NoError(t, err)

	if header != nil {
		req.Header = header
	}

	// Perform request
	resp, err := http.DefaultClient.Do(req)
	require.NoError(t, err)

	// Parse response
	respBody, err := io.ReadAll(resp.Body)
	require.NoError(t, err)
	defer resp.Body.Close()

	var responseWrapper handler.Response
	if len(respBody) > 0 {
		err = json.Unmarshal(respBody, &responseWrapper)
		require.NoError(t, err)
	}

	return resp.StatusCode, responseWrapper
}
