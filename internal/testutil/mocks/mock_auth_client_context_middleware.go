package mocks

import (
	"net/http"

	"github.com/google/uuid"
	"go.uber.org/zap"

	"gitlab.mooncascade.net/roman/go-starter-kit/internal/authorization"
	"gitlab.mooncascade.net/roman/go-starter-kit/internal/handler"
	"gitlab.mooncascade.net/roman/go-starter-kit/internal/logging"
	"gitlab.mooncascade.net/roman/go-starter-kit/storage"
)

// MockAuthClientContextMiddleware mocks `AuthClientContextMiddleware` and uses the given user UUID
// to populate user details in context: user ID and user role.
// If the given UUID is `nil`, the key is not set.
func MockAuthClientContextMiddleware(dbManager storage.DBManager, loggedInUserUUID *uuid.UUID) func(next http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			logger := logging.GetLogger(r.Context())

			ctx := r.Context()
			if loggedInUserUUID != nil {
				var err error
				ctx, err = authorization.AddUserDetailsToContext(r.Context(), dbManager, *loggedInUserUUID)
				if err != nil {
					logger.Error(
						"mock_auth_context_middleware: error adding user details to context",
						zap.String("user_id", loggedInUserUUID.String()),
						zap.Error(err),
					)
					handler.WriteErrorResponse(
						r.Context(), w,
						http.StatusUnauthorized,
						handler.ErrNotAuthorized,
						handler.ErrNotAuthenticatedCode,
					)
					return
				}
			}

			next.ServeHTTP(w, r.Clone(ctx))
		})
	}
}
