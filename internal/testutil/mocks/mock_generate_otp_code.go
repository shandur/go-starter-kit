package mocks

type MockGenerateOTP struct {
	Result string
	Error  error
}

func (m *MockGenerateOTP) GenerateOTP() (string, error) {
	return m.Result, m.Error
}
