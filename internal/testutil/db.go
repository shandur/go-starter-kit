package testutil

import (
	"context"

	"github.com/jackc/pgx/v4/pgxpool"
)

// TruncateUsers is a helper function to clean up `users` table.
func TruncateUsers(ctx context.Context, conn *pgxpool.Pool) error {
	_, err := conn.Exec(ctx, `
			truncate users cascade;
		`)
	return err
}

// TruncateUserAuthSessions is a helper function to clean up `user_auth_sessions` table.
func TruncateUserAuthSessions(ctx context.Context, conn *pgxpool.Pool) error {
	_, err := conn.Exec(ctx, `
			truncate user_auth_sessions cascade;
		`)
	return err
}
