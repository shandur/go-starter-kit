package strutil

// HasNewString checks if the new slice contains any string not present in the old slice.
func HasNewString(new []string, old []string) bool {
	if len(new) == 0 {
		return false
	}
	if len(old) == 0 {
		return true
	}
	m := map[string]struct{}{}
	for _, o := range old {
		m[o] = struct{}{}
	}
	for _, n := range new {
		if _, found := m[n]; !found {
			return true
		}
	}
	return false
}

// Unique set of strings would return result without
// duplicates.
func Unique(ss []string) []string {
	sMap := map[string]struct{}{}
	for _, s := range ss {
		sMap[s] = struct{}{}
	}

	var out []string
	for s := range sMap {
		out = append(out, s)
	}

	return out
}

// Exclude the provided string from the set of strings.
func Exclude(ss []string, excl string) []string {
	var out []string
	for _, s := range ss {
		if s == excl {
			continue
		}

		out = append(out, s)
	}
	return out
}

// SameContent checks if two slices of string has the same values.
func SameContent(in1, in2 []string) bool {
	if len(in1) != len(in2) {
		return false
	}

	m := make(map[string]int, len(in1))
	for _, v1 := range in1 {
		m[v1]++
	}

	for _, v2 := range in2 {
		if v := m[v2]; v == 0 {
			return false
		}
		m[v2]--
	}
	return true
}

// Contains checks for the existence of a string in a string slice.
func Contains(needle string, haystack []string) bool {
	for _, s := range haystack {
		if s == needle {
			return true
		}
	}
	return false
}

// StringPointerToString avoids potential nil pointer exceptions.
// e.g. https://play.golang.org/p/xmDfqThM0Oy
func StringPointerToString(s *string) string {
	if s == nil {
		return ""
	}
	return *s
}

// StringToPointer converts string to string pointer.
func StringToPointer(s string) *string {
	if s == "" {
		return nil
	}

	return &s
}
