package strutil

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestHasNewString(t *testing.T) {
	s0 := []string{"a1", "b2", "c3"}
	s1 := []string{"b2", "a1"}
	s2 := []string{"c3", "a2", "b1"}

	tests := []struct {
		name     string
		new      []string
		old      []string
		expected bool
	}{
		{
			name:     "new slice is empty",
			new:      []string{},
			old:      s0,
			expected: false,
		},
		{
			name:     "old slice is empty",
			new:      s0,
			old:      []string{},
			expected: true,
		},
		{
			name:     "new slice does not contain new strings",
			new:      s1,
			old:      s0,
			expected: false,
		},
		{
			name:     "new slice contains new strings",
			new:      s2,
			old:      s0,
			expected: true,
		},
	}

	for _, testCase := range tests {
		testCase := testCase
		t.Run(testCase.name, func(t *testing.T) {
			actual := HasNewString(testCase.new, testCase.old)
			require.Equal(t, testCase.expected, actual)
		})
	}
}

func TestSameContent(t *testing.T) {
	tests := []struct {
		name     string
		in1      []string
		in2      []string
		expected bool
	}{
		{
			name:     "both slices are empty",
			in1:      []string{},
			in2:      nil,
			expected: true,
		},
		{
			name:     "in1 slice is empty",
			in1:      nil,
			in2:      []string{"value"},
			expected: false,
		},
		{
			name:     "in2 slice is empty",
			in1:      []string{"value"},
			in2:      nil,
			expected: false,
		},
		{
			name:     "in1 slice has more values",
			in1:      []string{"value1", "value2"},
			in2:      []string{"value1"},
			expected: false,
		},
		{
			name:     "in2 slice has more values",
			in1:      []string{"value1"},
			in2:      []string{"value1", "value2"},
			expected: false,
		},
		{
			name:     "in1 has different value duplicated",
			in1:      []string{"value1", "value1"},
			in2:      []string{"value2", "value2"},
			expected: false,
		},
		{
			name:     "in1 has more values1 than in2",
			in1:      []string{"value1", "value1", "value2"},
			in2:      []string{"value1", "value2", "value2"},
			expected: false,
		},
		{
			name:     "in1 is the same as in2 in different order",
			in1:      []string{"value1", "value2"},
			in2:      []string{"value2", "value1"},
			expected: true,
		},
		{
			name:     "in1 is the same as in2",
			in1:      []string{"value1", "value2"},
			in2:      []string{"value1", "value2"},
			expected: true,
		},
	}

	for _, testCase := range tests {
		testCase := testCase
		t.Run(testCase.name, func(t *testing.T) {
			actual := SameContent(testCase.in1, testCase.in2)
			require.Equal(t, testCase.expected, actual)
		})
	}
}

func TestStringPointerToString(t *testing.T) {
	var strPointer *string
	testStr := "test"
	require.Equal(t, "", StringPointerToString(strPointer))
	require.Equal(t, "test", StringPointerToString(&testStr))
}
