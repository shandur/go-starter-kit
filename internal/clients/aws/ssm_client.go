package aws

import (
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/ssm"
	"github.com/pkg/errors"
)

type SSMClient struct {
	awsSSMClient *ssm.SSM
}

// TODO: Integrate SSM client into SSM secret config.
func NewAWSSSMClient(awsRegion string) (*SSMClient, error) {
	session, err := session.NewSession(&aws.Config{Region: aws.String(awsRegion)})
	if err != nil {
		return nil, errors.Wrap(err, "NewAWSSSMClient session.NewSession error")
	}

	awsSSMClient := ssm.New(session)

	client := SSMClient{
		awsSSMClient: awsSSMClient,
	}

	return &client, nil
}

// GetParametersByPath returns key-values for the given path (path is included in key names).
func (c *SSMClient) GetParametersByPath(path string) (map[string]string, error) {
	// Validate.
	if path == "" {
		return nil, errors.New("AWSSSMClient GetParametersByPath path missing error")
	}

	// Get parameters from AWS.
	result := map[string]string{}
	err := c.setParameters(result, path, nil)

	return result, err
}

func (c *SSMClient) setParameters(result map[string]string, path string, nextToken *string) error {
	input := ssm.GetParametersByPathInput{
		Path:           aws.String(path),
		NextToken:      nextToken,
		Recursive:      aws.Bool(true),
		WithDecryption: aws.Bool(true),
	}

	output, err := c.awsSSMClient.GetParametersByPath(&input)
	if err != nil {
		return errors.Wrap(err, "AWSSSMClient setParameters awsSSMClient.GetParametersByPath error")
	}

	if output == nil {
		return nil
	}

	for _, parameter := range output.Parameters {
		if parameter.Name == nil || parameter.Value == nil {
			continue
		}

		result[*parameter.Name] = *parameter.Value
	}

	if output.NextToken == nil {
		return nil
	}

	return c.setParameters(result, path, output.NextToken)
}
