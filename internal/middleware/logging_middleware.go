package middleware

import (
	"errors"
	"net/http"
	"time"

	"github.com/go-chi/chi/v5/middleware"
	"gitlab.mooncascade.net/roman/go-starter-kit/internal/handler"
	"go.uber.org/zap"
)

func ZapLogger(logger *zap.Logger) func(next http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		fn := func(w http.ResponseWriter, r *http.Request) {
			ww := middleware.NewWrapResponseWriter(w, r.ProtoMajor)

			fields := []zap.Field{
				zap.String("path", r.URL.Path),
				zap.String("method", r.Method),
				zap.String("proto", r.Proto),
				zap.String("request_id", middleware.GetReqID(r.Context())),
				zap.String("client_ip", r.Context().Value(handler.ContextUserRealIPKey).(string)),
			}

			logger.Info("API Request", fields...)

			start := time.Now()
			defer func() {
				status := ww.Status()
				durationMs := float64(time.Since(start)) / float64(time.Millisecond)
				fields = append(fields,
					zap.Int("status", status),
					zap.Float64("duration_ms", durationMs),
					zap.Int("size_bytes", ww.BytesWritten()),
				)

				if status >= http.StatusInternalServerError {
					logger.Error("API Response", fields...)
				} else {
					logger.Info("API Response", fields...)
				}
			}()

			next.ServeHTTP(ww, r)
		}
		return http.HandlerFunc(fn)
	}
}

func RecoverAndLog(logger *zap.Logger) func(next http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		fn := func(w http.ResponseWriter, r *http.Request) {
			defer func() {
				if rvr := recover(); rvr != nil && rvr != http.ErrAbortHandler {
					var err error
					switch t := rvr.(type) {
					case string:
						err = errors.New(t)
					case error:
						err = t
					default:
						err = errors.New("unexpected error")
					}
					logger.Error(
						"Panic recovered",
						zap.Error(err),
						zap.Stack("stack"),
						zap.String("request_id", middleware.GetReqID(r.Context())),
					)

					w.WriteHeader(http.StatusInternalServerError)
				}
			}()

			next.ServeHTTP(w, r)
		}
		return http.HandlerFunc(fn)
	}
}
