package middleware

import (
	"net/http"
	"regexp"
	"strings"

	"gitlab.mooncascade.net/roman/go-starter-kit/internal/authorization"
	"gitlab.mooncascade.net/roman/go-starter-kit/internal/handler"
	"gitlab.mooncascade.net/roman/go-starter-kit/storage"
)

const (
	uuidPlaceholder = "{uuid}"
)

var (
	uuidRegex = regexp.MustCompile(
		`[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}`,
	)
)

// UserRoleAuthorizationMiddleware checks the authenticated's users role
// for access to API endpoints. Default behavior is to deny access, unless permissions
// are explicitly defined in the configuration file [1].
// [1] internal/authorization/data/user_role_permissions.yaml
func UserRoleAuthorizationMiddleware(
	permissions authorization.UserRolePermissions,
) func(next http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		fn := func(w http.ResponseWriter, r *http.Request) {
			method := strings.ToLower(r.Method)
			path := r.URL.Path
			userRole, ok := r.Context().Value(handler.ContextUserRoleKey).(storage.UserRole)
			if !ok {
				handler.WriteErrorResponse(
					r.Context(), w,
					http.StatusUnauthorized,
					handler.ErrNotAuthorized,
					handler.ErrNotAuthorizedCode,
				)
				return
			}

			// Replace UUIDs in path with placeholder
			uuids := uuidRegex.FindAll([]byte(path), -1)
			for _, id := range uuids {
				path = strings.ReplaceAll(path, string(id), uuidPlaceholder)
			}

			// Check if the user's role is allowed to perform this action
			authorized := false
			authorizedPathMethods, ok := permissions.Paths[path]
			if ok {
				authorizeduserRoles, ok := authorizedPathMethods[method]
				if ok {
					authorizeduserRole, ok := authorizeduserRoles[userRole]
					authorized = ok && authorizeduserRole
				}
			}

			// Return 401 if not authorized, or if permissions cannot be found
			if !authorized {
				handler.WriteErrorResponse(
					r.Context(), w,
					http.StatusUnauthorized,
					handler.ErrNotAuthorized,
					handler.ErrNotAuthorizedCode,
				)
				return
			}

			next.ServeHTTP(w, r)
		}
		return http.HandlerFunc(fn)
	}
}
