package middleware

import (
	"net/http"

	"github.com/google/uuid"
	"go.uber.org/zap"

	"gitlab.mooncascade.net/roman/go-starter-kit/internal/authorization"
	"gitlab.mooncascade.net/roman/go-starter-kit/internal/handler"
	"gitlab.mooncascade.net/roman/go-starter-kit/internal/logging"
	"gitlab.mooncascade.net/roman/go-starter-kit/storage"
)

// AuthClientContextMiddleware middleware extract auth cookie,
// resolves authenticated user details, and sets to Context.
// If cookie is missing, 401 Unauthorised is returned.
func AuthClientContextMiddleware(dbManager storage.DBManager, authCookie *authorization.AuthCookie) func(next http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		fs := func(w http.ResponseWriter, r *http.Request) {
			logger := logging.GetLogger(r.Context())

			cookie, err := authCookie.GetCookie(r)
			if err != nil {
				logger.Error("auth_context_middleware: error fetching auth cookie", zap.Error(err))
				handler.WriteErrorResponse(r.Context(), w, http.StatusUnauthorized, handler.ErrNotAuthorized, handler.ErrNotAuthenticatedCode)
				return
			}

			userID, err := uuid.Parse(cookie.Value)
			if err != nil {
				logger.Error("auth_context_middleware: user ID is invalid", zap.Error(err), zap.String("user_id", cookie.Value))
				handler.WriteErrorResponse(r.Context(), w, http.StatusUnauthorized, handler.ErrNotAuthorized, handler.ErrNotAuthenticatedCode)
				return
			}

			ctx, err := authorization.AddUserDetailsToContext(r.Context(), dbManager, userID)
			if err != nil {
				logger.Error("auth_context_middleware: error adding user details to context", zap.String("user_id", userID.String()), zap.Error(err))
				handler.WriteErrorResponse(r.Context(), w, http.StatusUnauthorized, handler.ErrNotAuthorized, handler.ErrNotAuthenticatedCode)
				return
			}

			next.ServeHTTP(w, r.Clone(ctx))
		}

		return http.HandlerFunc(fs)
	}
}
