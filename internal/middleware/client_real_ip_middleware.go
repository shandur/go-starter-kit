package middleware

import (
	"context"
	"net/http"

	"github.com/pkg/errors"
	"go.uber.org/zap"

	"gitlab.mooncascade.net/roman/go-starter-kit/internal/handler"
	"gitlab.mooncascade.net/roman/go-starter-kit/internal/ip"
	"gitlab.mooncascade.net/roman/go-starter-kit/internal/logging"
)

// ClientRealIPMiddleware middleware detects and sets client IP address
// and pass it in Context downstream.
func ClientRealIPMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		logger := logging.GetLogger(r.Context())

		// Get client IP.
		clientIP, err := ip.GetClientIP(r)
		if err != nil {
			logger.Error("httphandler.GetClientIP failed to get client IP", zap.Error(err))

			handler.WriteErrorResponse(r.Context(), w, http.StatusInternalServerError, errors.New("Failed to define user IP"), handler.ErrInternalCode)
			return
		}

		// Set user's IP in the context.
		ctx := context.WithValue(r.Context(), handler.ContextUserRealIPKey, clientIP)

		next.ServeHTTP(w, r.WithContext(ctx))
	})
}
