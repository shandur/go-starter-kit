package logging

import (
	"context"
	"log"

	"github.com/go-chi/chi/v5/middleware"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

func init() { // nolint: gochecknoinits
	replaceGlobalLogger(false)
}

// GetLogger returns the global Logger, which can be reconfigured with ReplaceGlobals.
// It's safe for concurrent use.
func GetLogger(optionalCtx ...context.Context) *zap.Logger {
	fields := getFieldsFromContext(optionalCtx...)
	return zap.L().With(fields...)
}

// Sugar wraps the Logger to provide a more ergonomic, but slightly slower,
// API. Sugaring a Logger is quite inexpensive, so it's reasonable for a
// single application to use both Loggers and SugaredLoggers, converting
// between them on the boundaries of performance-sensitive code.
func GetSugaredLogger(optionalCtx ...context.Context) *zap.SugaredLogger {
	return GetLogger(optionalCtx...).Sugar()
}

func EnableDebugLogs() {
	replaceGlobalLogger(true)
}

func DisableDebugLogs() {
	replaceGlobalLogger(false)
}

func replaceGlobalLogger(debug bool) {
	encoderConfig := zap.NewProductionEncoderConfig()
	encoderConfig.EncodeTime = zapcore.ISO8601TimeEncoder

	config := zap.NewProductionConfig()
	config.EncoderConfig = encoderConfig
	if debug {
		config.Level = zap.NewAtomicLevelAt(zap.DebugLevel)
	}

	logger, err := config.Build(zap.AddStacktrace(zap.PanicLevel))
	if err != nil {
		log.Fatalf("Could not initialize logger: %s", err.Error())
	}

	zap.ReplaceGlobals(zap.New(logger.Core()))
}

func getFieldsFromContext(optionalCtx ...context.Context) []zapcore.Field {
	ctx := context.Background()
	if len(optionalCtx) > 0 {
		ctx = optionalCtx[0]
	}

	fields := []zapcore.Field{}

	requestID := middleware.GetReqID(ctx)
	if len(requestID) > 0 {
		fields = append(fields, zap.String("request_id", requestID))
	}

	return fields
}
