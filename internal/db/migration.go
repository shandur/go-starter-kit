package db

import (
	"database/sql"
	"fmt"
	"strconv"
	"strings"
	"time"

	"github.com/golang-migrate/migrate/v4"
	"github.com/golang-migrate/migrate/v4/database/postgres"
	_ "github.com/golang-migrate/migrate/v4/source/file" // This is needed for the database driver
	_ "github.com/lib/pq"                                // This is needed for the database driver
	"github.com/pkg/errors"
	"gitlab.mooncascade.net/roman/go-starter-kit/config"
	"gitlab.mooncascade.net/roman/go-starter-kit/internal/logging"
	"go.uber.org/zap"
)

const (
	// migrationVer is a desired migration version.
	// Migrator will try to migrate to the specified version and check
	// if current set version align with the desired one.
	migrationVer = "20210501120805"
)

const (
	defaultRetryInterval = 6 * time.Second
)

// MigrationConfig that could be used to overwrite the default behaviour.
type MigrationConfig struct {
	Path          string
	Version       string
	MaxRetries    int
	RetryInterval time.Duration
}

// Migrate runs and checks the migration if `runMigration` is true, otherwise
// it simply checks the migration.
func Migrate(env *config.Config, storageDB *sql.DB, runMigration bool) error {
	migrationConfig := MigrationConfig{
		Path:          env.Storage.Postgres.Migration.Path,
		MaxRetries:    env.Storage.Postgres.Migration.MaxRetries,
		RetryInterval: time.Duration(env.Storage.Postgres.Migration.RetryIntervalSecs) * time.Second,
	}

	if runMigration {
		err := RunMigration(storageDB, migrationConfig)
		if err != nil {
			return errors.Wrapf(err, "couldn't run migration %s", migrationConfig.Version)
		}
	}

	err := CheckMigration(storageDB, migrationConfig)
	if err != nil {
		return errors.Wrap(err, "error checking migration")
	}
	return nil
}

// Normalize the MigrationConfig and define
// default values if not provided.
func (m *MigrationConfig) Normalize() {
	// not setting default value for max retries since
	// zero is a valid value.
	if strings.TrimSpace(m.Version) == "" {
		m.Version = migrationVer
	}
	if m.RetryInterval == 0 {
		m.RetryInterval = defaultRetryInterval
	}
}

// RunMigration using the provided postgres sql.DB.
func RunMigration(db *sql.DB, config MigrationConfig) error {
	config.Normalize()

	intVer, err := strconv.ParseInt(config.Version, 10, 64)
	if err != nil {
		return err
	}

	driver, err := postgres.WithInstance(db, &postgres.Config{})
	if err != nil {
		return errors.Wrap(err, "couldn't initialize migration driver")
	}
	m, err := migrate.NewWithDatabaseInstance(
		"file://"+config.Path,
		"postgres", driver)
	if err != nil {
		return errors.Wrap(err, "couldn't prepare migration instance")
	}

	err = m.Migrate(uint(intVer))
	switch {
	case err == migrate.ErrNoChange:
		return nil
	case err != nil:
		return errors.Wrapf(err, "couldn't migrate from path %s", config.Path)
	}

	return nil
}

// CheckMigration returns error if current version doesn't match
// the expected version to prevent the application running with
// incompatible schema.
func CheckMigration(db *sql.DB, config MigrationConfig) error {
	logger := logging.GetLogger()
	config.Normalize()

	intVer, err := strconv.ParseInt(config.Version, 10, 64)
	if err != nil {
		return err
	}

	driver, err := postgres.WithInstance(db, &postgres.Config{})
	if err != nil {
		return errors.Wrap(err, "couldn't initialize migration driver")
	}

	m, err := migrate.NewWithDatabaseInstance(
		"file://"+config.Path,
		"postgres", driver)
	if err != nil {
		return errors.Wrap(err, "couldn't prepare migration instance")
	}

	retries := 0
	for {
		logger.Debug("migration retries", zap.Int("count", retries))

		var e error
		currVer, dirty, err := m.Version()

		switch {
		case err != nil:
			e = fmt.Errorf("couldn't fetch migration version: %s", err)
		case currVer != uint(intVer):
			e = fmt.Errorf("version mismatch: current %d - expected %d", currVer, intVer)
		case dirty:
			e = errors.New("migration is not finished")
		default:
			e = nil
		}
		if e == nil {
			break
		}

		// Retry.
		if retries >= config.MaxRetries {
			return e
		}

		retries++
		time.Sleep(config.RetryInterval)
		continue
	}

	return nil
}
