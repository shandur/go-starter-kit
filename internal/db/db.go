package db

import (
	"context"
	"time"

	"gitlab.mooncascade.net/roman/go-starter-kit/config"
	"gitlab.mooncascade.net/roman/go-starter-kit/storage/postgres"
)

// ToUTC is a helper method to standardize the timestamps to UTC format
// since we insert into postgres without a timestamp.
func ToUTC(timestamp time.Time) time.Time {
	return timestamp.UTC().Round(time.Microsecond)
}

// GetPostgresClient sets up a db connection.
// `conn.Close()` should be handled at callsite.
func GetPostgresClient(ctx context.Context, env *config.Config) (*postgres.Client, error) {
	return postgres.New(ctx, postgres.Options{
		Host:     env.Storage.Postgres.Host,
		Port:     env.Storage.Postgres.Port,
		Username: env.Storage.Postgres.Username,
		Password: env.Storage.Postgres.Password,
		DBName:   env.Storage.Postgres.Name,
		SSLMode:  env.Storage.Postgres.SSL,
	})
}
