package validator

import (
	"fmt"
	"reflect"
	"strings"

	"github.com/go-playground/validator/v10"
	"github.com/google/uuid"
)

// NewValidator creates a new *validator.Validate.
// It extends a validator with a tag extraction function.
func NewValidator() *validator.Validate {
	v := validator.New()

	// Extract the name of `json` tag of the field that's been validated.
	// E.g., Name `json:"name"` - we will extract and use `name` instead of 'Name' in `validationError.Field()` method.
	// Otherwise, `validator` will use actual struct name, i.e., `Name`.
	v.RegisterTagNameFunc(func(field reflect.StructField) string {
		name := strings.SplitN(field.Tag.Get("json"), ",", 2)[0]
		if name == "-" {
			return ""
		}
		return name
	})

	// Register proper validation for UUID if struct field type is uuid.UUID.
	v.RegisterValidation("uuid", func(fl validator.FieldLevel) bool { // nolint: errcheck
		_, err := uuid.Parse(fmt.Sprintf("%v", fl.Field()))

		return err == nil
	})

	return v
}
