package authorization

import (
	"strings"
	"testing"

	"github.com/stretchr/testify/require"

	"gitlab.mooncascade.net/roman/go-starter-kit/storage"
)

var (
	validMethods = []string{"post", "get", "patch", "delete"}
)

func TestLoadPermissions(t *testing.T) {
	permissions, err := LoadPermissions()
	require.NoError(t, err)

	// assert structure is correct - not intended to test every permission
	require.Greater(t, len(permissions.Paths), 0)
	for path, methodPermissions := range permissions.Paths {
		require.True(t, strings.HasPrefix(path, "/v1/"))

		for method, userRoles := range methodPermissions {
			require.Contains(t, validMethods, method)
			for userRole := range userRoles {
				require.Contains(t, storage.ValidUserRoles, userRole)
			}
		}
	}
}
