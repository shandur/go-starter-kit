package authorization

import (
	_ "embed" // enable embed directive

	"gopkg.in/yaml.v3"

	"gitlab.mooncascade.net/roman/go-starter-kit/storage"
)

var (
	//go:embed data/user_role_permissions.yaml
	permissionsRaw string
)

type UserRolePermissions struct {
	Paths map[string]map[string]map[storage.UserRole]bool `yaml:"paths"`
}

func LoadPermissions() (UserRolePermissions, error) {
	var perms UserRolePermissions
	err := yaml.Unmarshal([]byte(permissionsRaw), &perms)
	return perms, err
}
