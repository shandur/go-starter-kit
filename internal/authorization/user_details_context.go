package authorization

import (
	"context"

	"github.com/google/uuid"
	"github.com/pkg/errors"

	"gitlab.mooncascade.net/roman/go-starter-kit/internal/handler"
	"gitlab.mooncascade.net/roman/go-starter-kit/storage"
)

// AddUserDetailsToContext adds user ID and role as values to the
// provided context object. An error will be returned if the user cannot be found.
func AddUserDetailsToContext(ctx context.Context, dbManager storage.DBManager, userID uuid.UUID) (context.Context, error) {
	user, err := dbManager.GetUserByUUID(ctx, userID)
	if err != nil {
		return ctx, errors.Wrap(err, "failed to find user")
	}

	// Set authenticated user ID and user role in Context.
	// Can be retrieved by using:
	// `context.Value(handler.ContextUserIDKey).(uuid.UUID)`
	// `context.Value(handler.ContextUserRoleKey).(storage.UserRole)`
	ctx = context.WithValue(ctx, handler.ContextUserIDKey, user.ID)
	ctx = context.WithValue(ctx, handler.ContextUserRoleKey, storage.UserRole(user.Role))

	return ctx, nil
}
