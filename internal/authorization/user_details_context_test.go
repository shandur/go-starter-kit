package authorization

import (
	"context"
	"testing"

	"github.com/google/uuid"
	"github.com/stretchr/testify/require"

	"gitlab.mooncascade.net/roman/go-starter-kit/config"
	"gitlab.mooncascade.net/roman/go-starter-kit/internal/db"
	"gitlab.mooncascade.net/roman/go-starter-kit/internal/handler"
	"gitlab.mooncascade.net/roman/go-starter-kit/internal/testutil"
	"gitlab.mooncascade.net/roman/go-starter-kit/storage"
)

type testFixtures struct {
	dbManager storage.DBManager
	cleanup   func()
}

func setupTests(t *testing.T) *testFixtures {
	t.Helper()

	// Read configs.
	ctx := context.Background()
	env, err := config.ReadEnvVars(ctx)
	require.NoError(t, err, "parsing environment variables")

	// Setup database.
	pgClient, err := db.GetPostgresClient(ctx, &env)
	require.NoError(t, err, "creating postgres client")

	sb := storage.NewStatementBuilder()
	conn := pgClient.Conn

	runMigration := true
	err = db.Migrate(&env, pgClient.DB.DB, runMigration)
	require.NoError(t, err, "running migration")

	// Setup DB Manager.
	dbManager, err := storage.NewDBManager(storage.DBConfig{
		Conn:             conn,
		StatementBuilder: sb,
	})
	require.NoError(t, err, "failed to create DB manager")

	// Cleanup.
	cleanup := func() {
		err := testutil.TruncateUsers(ctx, conn)
		require.NoError(t, err, "Could not clean up users tables")

		conn.Close()
	}

	return &testFixtures{
		dbManager: dbManager,
		cleanup:   cleanup,
	}
}

func TestAddUserDetailsToContext(t *testing.T) {
	// Arrange.
	fixtures := setupTests(t)
	defer fixtures.cleanup()

	// Create user
	userRole := storage.UserRoleTypeModerator
	user, err := fixtures.dbManager.CreateUser(context.Background(), storage.User{
		FirstName:    "FirstName1",
		LastName:     "LastName1",
		EmailAddress: "user1@test,com",
		Role:         string(userRole),
	})
	require.NoError(t, err)

	// Act.
	ctx, err := AddUserDetailsToContext(context.Background(), fixtures.dbManager, user.ID)

	// Assert.
	require.NoError(t, err)
	require.Equal(t, user.ID, ctx.Value(handler.ContextUserIDKey).(uuid.UUID))
	require.Equal(t, userRole, ctx.Value(handler.ContextUserRoleKey).(storage.UserRole))
}
