package authorization

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestAuthCookie_SetCookie(t *testing.T) {
	// Arrange.
	accessToken := "access_token_1"
	appDomain := "app.domain.com"
	expiresIn := 90000

	t.Run("set non-secure cookie", func(t *testing.T) {
		recorder := httptest.NewRecorder()

		// Act.
		authCookie := NewAuthCookie(appDomain, false)
		authCookie.SetCookie(recorder, accessToken, expiresIn)

		// Assert.
		result := recorder.Result()
		require.NoError(t, result.Body.Close())

		cookie := result.Header.Get("Set-Cookie")

		require.NotNil(t, cookie)
		require.Contains(t, cookie, fmt.Sprintf("%s=%s", AuthCookieUserKey, accessToken))
		require.Contains(t, cookie, fmt.Sprintf("Domain=%s", appDomain))
		require.Contains(t, cookie, fmt.Sprintf("Max-Age=%d", expiresIn))
		require.Contains(t, cookie, "HttpOnly")
		require.NotContains(t, cookie, "Secure;")
		require.Contains(t, cookie, "SameSite=None")
	})

	t.Run("set secure cookie", func(t *testing.T) {
		recorder := httptest.NewRecorder()

		// Act.
		authCookie := NewAuthCookie(appDomain, true)
		authCookie.SetCookie(recorder, accessToken, expiresIn)

		// Assert.
		result := recorder.Result()
		require.NoError(t, result.Body.Close())

		cookie := result.Header.Get("Set-Cookie")

		require.NotNil(t, cookie)
		require.Contains(t, cookie, fmt.Sprintf("%s=%s", AuthCookieUserKey, accessToken))
		require.Contains(t, cookie, fmt.Sprintf("Domain=%s", appDomain))
		require.Contains(t, cookie, fmt.Sprintf("Max-Age=%d", expiresIn))
		require.Contains(t, cookie, "HttpOnly")
		require.Contains(t, cookie, "Secure")
		require.Contains(t, cookie, "SameSite=None")
	})
}

func TestAuthCookie_GetCookie(t *testing.T) {
	// Arrange.
	accessToken := "access_token_1"
	appDomain := "https://domain.com"

	req, err := http.NewRequest(http.MethodGet, appDomain, nil)
	require.NoError(t, err)

	req.AddCookie(&http.Cookie{
		Name:  AuthCookieUserKey,
		Value: accessToken,
	})

	// Act.
	authCookie := NewAuthCookie(appDomain, true)
	cookie, err := authCookie.GetCookie(req)
	require.NoError(t, err)

	// Assert.
	require.NotNil(t, cookie)
	require.Equal(t, cookie.Name, AuthCookieUserKey)
	require.Equal(t, cookie.Value, accessToken)
}

func TestAuthCookie_FlushCookie(t *testing.T) {
	// Arrange.
	appDomain := "app.domain.com"

	recorder := httptest.NewRecorder()

	// Act.
	authCookie := NewAuthCookie(appDomain, true)
	authCookie.FlushCookie(recorder)

	// Assert.
	result := recorder.Result()
	require.NoError(t, result.Body.Close())

	cookie := result.Header.Get("Set-Cookie")

	require.NotNil(t, cookie)
	require.Contains(t, cookie, fmt.Sprintf("%s=;", AuthCookieUserKey)) // empty token
	require.Contains(t, cookie, fmt.Sprintf("Domain=%s", appDomain))
	require.Contains(t, cookie, "Max-Age=0") // should expire immediately
	require.Contains(t, cookie, "HttpOnly")
	require.Contains(t, cookie, "Secure")
	require.Contains(t, cookie, "SameSite=None")
}
