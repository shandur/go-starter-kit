package authorization

import (
	"net/http"
	"time"
)

const (
	// AuthCookieUserKey is used to store JWT access token of the user.
	// Ref on prefixes: https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Set-Cookie#cookie_prefixes
	AuthCookieUserKey = "context_user_access_token"

	// AuthCookieDefaultMaxAgeSec defines default Max-Age for auth cookie.
	AuthCookieDefaultMaxAgeSec = 86400 // 1 day
)

// AuthCookie manages an authentication cookie to set/get user's access token.
// It scopes cookie to the specified domain and set it as `Secure` if `IsSecure` is true.
type AuthCookie struct {
	appDomain string
	isSecure  bool
}

func NewAuthCookie(appDomain string, isSecure bool) *AuthCookie {
	return &AuthCookie{
		appDomain: appDomain,
		isSecure:  isSecure,
	}
}

// SetCookie stores JWT access token into secure HttpOnly cookie for the specified domain.
// If `IsSecure` is true, the cookie will have `Secure` property set to true.
// Ref: https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Set-Cookie
func (c *AuthCookie) SetCookie(w http.ResponseWriter, userUUID string, expiresIn int) {
	http.SetCookie(w, &http.Cookie{
		Name:   AuthCookieUserKey,
		Value:  userUUID,
		Domain: c.appDomain,

		// A path that must exist in the requested URL, or the browser won't send the Cookie header.
		Path: "/",

		// Cookie is only sent to the server when a request is made with the `https:` scheme (except on localhost).
		Secure: c.isSecure,

		// Forbids JavaScript from accessing the cookie.
		HttpOnly: true,

		// Controls whether a cookie is sent with cross-origin requests, providing some protection against cross-site request forgery attacks (CSRF).
		// The browser sends the cookie only for same-site requests (that is, requests originating from the same site that set the cookie).
		// If the request originated from a different URL than the current one, no cookies with the SameSite=Strict attribute are sent.
		SameSite: http.SameSiteNoneMode,

		// The maximum lifetime of the cookie as an HTTP-date timestamp. See Date for the required formatting.
		// When an Expires date is set, the deadline is relative to the client the cookie is being set on, not the server.
		Expires: time.Now().Add(time.Duration(expiresIn) * time.Second),

		// Number of seconds until the cookie expires. A zero or negative number will expire the cookie immediately.
		// If both Expires and Max-Age are set, Max-Age has precedence.
		MaxAge: expiresIn,
	})
}

// GetCookie returns auth cookie from the request.
// If cookie is not found, `http.ErrNoCookie` is returned.
// If multiple cookies match the given name, only one cookie will be returned.
func (c *AuthCookie) GetCookie(r *http.Request) (*http.Cookie, error) {
	return r.Cookie(AuthCookieUserKey)
}

// FlushCookie set `Max-Age=-1` to immediately expire the auth cookie.
func (c *AuthCookie) FlushCookie(w http.ResponseWriter) {
	http.SetCookie(w, &http.Cookie{
		Name:   AuthCookieUserKey,
		Value:  "",
		Domain: c.appDomain,

		// A path that must exist in the requested URL, or the browser won't send the Cookie header.
		Path: "/",

		// Cookie is only sent to the server when a request is made with the `https:` scheme (except on localhost).
		Secure: c.isSecure,

		// Forbids JavaScript from accessing the cookie.
		HttpOnly: true,

		// Controls whether a cookie is sent with cross-origin requests, providing some protection against cross-site request forgery attacks (CSRF).
		// The browser sends the cookie only for same-site requests (that is, requests originating from the same site that set the cookie).
		// If the request originated from a different URL than the current one, no cookies with the SameSite=Strict attribute are sent.
		SameSite: http.SameSiteNoneMode,

		// Number of seconds until the cookie expires.
		// A zero or negative number will expire the cookie immediately.
		MaxAge: -1,
	})
}
