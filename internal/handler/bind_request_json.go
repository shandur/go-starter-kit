package handler

import (
	"encoding/json"
	"net/http"

	"github.com/pkg/errors"
)

const (
	// The size limit of the incoming request payload (32 MB).
	maxRequestPayloadSize = int64(32 << 20)
)

// BindRequestJSON reads the request (with http.MaxBytesReader) and decode the request payload
// into the given struct using `json.Decoder`.
func BindRequestJSON(w http.ResponseWriter, r *http.Request, i interface{}) error {
	decoder := json.NewDecoder(http.MaxBytesReader(w, r.Body, maxRequestPayloadSize))
	defer r.Body.Close()

	// Decode JSON into the given struct.
	err := decoder.Decode(&i)
	if err != nil {
		return errors.Wrap(err, "decoder.Decode error")
	}

	return nil
}
