package handler

import (
	"net/url"
	"strconv"
	"strings"

	"gitlab.mooncascade.net/roman/go-starter-kit/storage"
)

type CommonQueryParams struct {
	// Sorting instructions
	Sort storage.SortOptions

	// Basic inefficient pagination
	// TODO: cursor based pagination
	Limit  uint64
	Offset uint64
}

// ParseCommonQueryParams parses sorting and pagination arguments from the
// provided values. Invalid parameters are ignored.
func ParseCommonQueryParams(inputParams url.Values) CommonQueryParams {
	var queryParams CommonQueryParams

	// Pagination
	// Errors are ignored when parsing uints - the error case is the same
	// as the default case, it returns 0.
	limitRaw := inputParams.Get("limit")
	if limitRaw != "" {
		queryParams.Limit, _ = strconv.ParseUint(limitRaw, 10, 64)
	}
	offsetRaw := inputParams.Get("offset")
	if offsetRaw != "" {
		queryParams.Offset, _ = strconv.ParseUint(offsetRaw, 10, 64)
	}

	// Sorting
	// Input is expected to be comma separated field names, with + or - prefixes
	// which determine sort order (ascending or descending respectively). If
	// the prefix is omitted, ascending is assumed.
	// E.g. to sort by name asc, date desc: ?sort=+name,-date
	sortInput := inputParams.Get("sort")
	if sortInput != "" {
		queryParams.Sort = make([]storage.SortPair, 0)
		sortSplit := strings.Split(sortInput, ",")
		for _, sortInstruction := range sortSplit {
			// Set direction, default to ascending
			direction := storage.SortDirectionAscending
			if sortInstruction[:1] == "-" {
				direction = storage.SortDirectionDescending
			}
			// Trim prefix if present
			sortField := strings.TrimLeftFunc(
				sortInstruction, func(r rune) bool {
					return r == '-' || r == '+'
				},
			)
			queryParams.Sort = append(
				queryParams.Sort, storage.SortPair{
					Field:     sortField,
					Direction: direction,
				},
			)
		}
	}

	return queryParams
}
