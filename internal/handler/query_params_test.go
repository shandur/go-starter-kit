package handler

import (
	"net/url"
	"testing"

	"github.com/stretchr/testify/require"

	"gitlab.mooncascade.net/roman/go-starter-kit/storage"
)

func TestParseValidCommonQueryParams(t *testing.T) {
	input := make(url.Values)
	input["limit"] = []string{"5"}
	input["offset"] = []string{"10"}
	input["sort"] = []string{"+name,-date"}

	params := ParseCommonQueryParams(input)
	require.Equal(t, uint64(5), params.Limit)
	require.Equal(t, uint64(10), params.Offset)
	require.Equal(t, 2, len(params.Sort))
	require.Equal(t, "name", params.Sort[0].Field)
	require.Equal(t, storage.SortDirectionAscending, params.Sort[0].Direction)
	require.Equal(t, "date", params.Sort[1].Field)
	require.Equal(t, storage.SortDirectionDescending, params.Sort[1].Direction)
}

func TestParseCommonQueryParamsDefaults(t *testing.T) {
	input := make(url.Values)
	input["limit"] = []string{"-5"}
	input["offset"] = []string{"foo"}
	input["sort"] = []string{"name"}

	params := ParseCommonQueryParams(input)
	require.Equal(t, uint64(0), params.Limit)
	require.Equal(t, uint64(0), params.Offset)
	require.Equal(t, 1, len(params.Sort))
	require.Equal(t, "name", params.Sort[0].Field)
	require.Equal(t, storage.SortDirectionAscending, params.Sort[0].Direction)
}
