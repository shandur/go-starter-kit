package handler

type RequestContextKey string

const (
	// ContextUserRealIPKey represents a key to carry a real user IP address.
	ContextUserRealIPKey = RequestContextKey("context_user_real_ip")

	// ContextUserIDKey is used to keep authenticated user's ID.
	ContextUserIDKey = RequestContextKey("context_user_id")

	// ContextUserRoleKey is used to store an authenticated user's role.
	ContextUserRoleKey = RequestContextKey("context_role_type")
)
