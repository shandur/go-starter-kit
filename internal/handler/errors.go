package handler

import "errors"

var (
	// ErrNotAuthorized defines an authorisation error.
	ErrNotAuthorized = errors.New("user not authorised")
)
