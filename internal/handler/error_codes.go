package handler

const (
	// ErrInternalCode represents an internal application error.
	ErrInternalCode = "internal_error"

	// ErrBadInput represents failure due to bad input.
	ErrBadInput = "bad_input"

	// ErrNotFound represents data that cannot be found.
	ErrNotFound = "not_found"

	// ErrClientCode represents a client-related error.
	ErrClientCode = "client_error"

	// ErrNotAuthenticatedCode represents authentication error.
	ErrNotAuthenticatedCode = "not_authenticated"

	// ErrNotAuthorizedCode represents authorization error.
	ErrNotAuthorizedCode = "not_authorized"

	// ErrOTPCode represents an error related to OTP.
	ErrOTPCode = "otp_error"
)
