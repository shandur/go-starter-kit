package handler

import (
	"github.com/go-playground/validator/v10"
)

// ValidateRequest validates the given request as a struct using `validate` tags.
func ValidateRequest(v *validator.Validate, request interface{}) validator.ValidationErrors {
	err := v.Struct(request)
	if err == nil {
		return nil
	}

	validationErrors := err.(validator.ValidationErrors)

	return validationErrors
}
