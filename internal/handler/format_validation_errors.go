package handler

import (
	"github.com/go-playground/validator/v10"
)

// FormatValidationErrors takes the validation errors and format them as a list of `ResponseValidationError`.
// It uses `field` to store the field name and `tag` to identify the validation rule.
func FormatValidationErrors(validationErrors validator.ValidationErrors) []ResponseError {
	if validationErrors == nil {
		return nil
	}

	// Form and collect ValidationErrors.
	var errors []ResponseError
	for _, validationError := range validationErrors {
		// The field that causes an error.
		field := validationError.Field()
		// The validation rule tag.
		tag := validationError.Tag()

		errors = append(
			errors,
			ResponseError{
				Code: tag,
				Source: ResponseErrorSource{
					Pointer: ConvertToErrorSourcePointer(field),
				},
			},
		)
	}

	return errors
}
