package handler

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/go-playground/validator/v10"
	"github.com/pkg/errors"
	"gitlab.mooncascade.net/roman/go-starter-kit/internal/logging"
	"go.uber.org/zap"
)

// Response is the service response that keeps handler's payload and errors.
//
// Inspired by JSON:API specification: https://jsonapi.org/format/#document-top-level
type Response struct {
	// Data usually stores handler's response.
	Data interface{} `json:"data,omitempty"`

	// List of application errors.
	Errors []ResponseError `json:"errors,omitempty"`
}

// ResponseError represents any business logic error.
//
// Ref: https://jsonapi.org/format/#errors
type ResponseError struct {
	// A short, human-readable summary of the problem.
	Title string `json:"title"`

	// An application-specific error code, expressed as a string value, e.g., "out_of_range" etc.
	Code string `json:"code"`

	// An object containing references to the source of the error.
	Source ResponseErrorSource `json:"source,omitempty"`
}

type ResponseErrorSource struct {
	// A JSON Pointer [RFC6901] to the associated entity in the request document
	// [e.g. "/data" for a primary data object, or "/data/attributes/title" for a specific attribute].
	Pointer string `json:"pointer,omitempty"`
}

// ConvertToErrorSourcePointer is a helper function to encapsulate format
// of `ResponseErrorSource.Pointer` field.
func ConvertToErrorSourcePointer(sourcePointer string) string {
	return fmt.Sprintf("/data/attributes/%s", sourcePointer)
}

// WriteSuccessResponseJSON prepares and JSON-encodes the service response, and returns
// with the given HTTP status.
func WriteSuccessResponseJSON(ctx context.Context, w http.ResponseWriter, statusCode int, response interface{}) {
	var r *Response
	if response != nil {
		r = &Response{
			Data: response,
		}
	}

	writeResponseJSON(ctx, w, r, statusCode)
}

// WriteResponseJSON JSON-encodes the provided response, and returns with the given HTTP status.
func WriteResponseJSON(ctx context.Context, w http.ResponseWriter, statusCode int, response Response) {
	writeResponseJSON(ctx, w, &response, statusCode)
}

// WriteValidationErrorResponse formats the validation errors, JSON-encodes response and
// set HTTP status to `http.StatusBadRequest`.
func WriteValidationErrorResponse(ctx context.Context, w http.ResponseWriter, validationErrors validator.ValidationErrors) {
	formattedErrors := FormatValidationErrors(validationErrors)
	r := Response{
		Errors: formattedErrors,
	}

	writeResponseJSON(ctx, w, &r, http.StatusBadRequest)
}

// WriteErrorResponse logs the original error, sets the given HTTP status code,
// JSON-encodes and returns the response.
func WriteErrorResponse(ctx context.Context, w http.ResponseWriter, statusCode int, err error, errCode string) {
	logger := logging.GetLogger(ctx)
	r := Response{
		Errors: []ResponseError{
			{
				Title: err.Error(),
				Code:  errCode,
			},
		},
	}

	// Log if experiencing a 5xx
	if statusCode >= http.StatusInternalServerError {
		logger.Error("handler internal error", zap.Error(errors.Cause(err)))
	}

	writeResponseJSON(ctx, w, &r, statusCode)
}

// writeResponseJSON is an internal function that sets the HTTP status, JSON-encodes and writes the response.
func writeResponseJSON(ctx context.Context, w http.ResponseWriter, r *Response, statusCode int) {
	logger := logging.GetLogger(ctx)

	var (
		payload []byte
		err     error
	)

	if r != nil {
		payload, err = json.Marshal(r)
		if err != nil {
			logger.Error("service failed to encode JSON response", zap.Error(err))
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
	}

	// Set JSON Content-Type header.
	// Note: Chi compression middleware requires this header to apply compression.
	w.Header().Set("Content-Type", "application/json")

	// Write HTTP status.
	w.WriteHeader(statusCode)

	if payload != nil {
		// Write the response payload.
		_, err = w.Write(payload)
		if err != nil {
			logger.Error("service failed to write the response", zap.Error(err))
		}
	}
}
