package uuid

import (
	"github.com/google/uuid"
)

// NewRandom returns UUID V4 formatted as a string.
func NewRandom() (string, error) {
	id, err := uuid.NewRandom()
	if err != nil {
		return "", err
	}

	return id.String(), nil
}

// IsValidUUID returns true if given string is a UUID
func IsValidUUID(u string) bool {
	_, err := uuid.Parse(u)
	return err == nil
}
