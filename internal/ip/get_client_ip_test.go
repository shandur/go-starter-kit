package ip

import (
	"net/http"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestGetClientIPXForwardIPAddressSuccess(t *testing.T) {
	// Arrange.
	request := http.Request{
		Header: http.Header{
			"X-Forwarded-For": {"1.2.3.4"},
		},
	}

	// Act.
	result, err := GetClientIP(&request)

	// Assert.
	require.NoError(t, err)
	require.Equal(t, "1.2.3.4", result)
}

func TestGetClientIPXForwardIPAddressListSuccess(t *testing.T) {
	// Arrange.
	request := http.Request{
		Header: http.Header{
			"X-Forwarded-For": {"1.2.3.4,55.66.77.88"},
		},
	}

	// Act.
	result, err := GetClientIP(&request)

	// Assert.
	require.NoError(t, err)
	require.Equal(t, "1.2.3.4", result)
}

func TestGetClientIPXForwardIPAddressEmpty(t *testing.T) {
	// Arrange.
	request := http.Request{
		RemoteAddr: "1.2.3.4:8080",
		Header: http.Header{
			"X-Forwarded-For": {""},
		},
	}

	// Act.
	result, err := GetClientIP(&request)

	// Assert.
	require.NoError(t, err)
	require.Equal(t, "1.2.3.4", result)
}

func TestGetClientIPFromHostAndPortSuccess(t *testing.T) {
	// Arrange.
	request := http.Request{
		RemoteAddr: "1.2.3.4:8080",
	}

	// Act.
	result, err := GetClientIP(&request)

	// Assert.
	require.NoError(t, err)
	require.Equal(t, "1.2.3.4", result)
}

func TestGetClientIPFromHostAndPortError(t *testing.T) {
	// Arrange.
	request := http.Request{
		// Port is missing.
		RemoteAddr: "1.1.1.1",
	}

	// Act.
	result, err := GetClientIP(&request)

	// Assert.
	require.Error(t, err)
	require.Empty(t, result)
}

func TestGetClientIPReturnsEmptySuccess(t *testing.T) {
	// Arrange.
	request := http.Request{
		// Returns "<nil>" as empty IP.
		RemoteAddr: "localhost:80",
	}

	// Act.
	result, err := GetClientIP(&request)

	// Assert.
	require.NoError(t, err)
	require.Empty(t, result)
}
