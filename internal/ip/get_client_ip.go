package ip

import (
	"net"
	"net/http"
	"strings"

	"github.com/pkg/errors"
)

// GetClientIP returns client IP address. If no IP address is presented, empty string is returned.
//
// First of all, it tries to get IP from "X-Forwarded-For" header which implies proxy integrity.
// Then, it tries to get the IP address from RemoteAddr (ip:port).
func GetClientIP(r *http.Request) (string, error) {
	// See: https://docs.aws.amazon.com/elasticloadbalancing/latest/classic/x-forwarded-headers.html#x-forwarded-for
	forward := r.Header.Get("X-Forwarded-For")
	if forward != "" {
		// The first ID in the list is a user's IP.
		forwardedIPs := strings.Split(forward, ",")
		if len(forwardedIPs) != 0 {
			return forwardedIPs[0], nil
		}
	}

	ip, _, err := net.SplitHostPort(r.RemoteAddr)
	if err != nil {
		return "", errors.Wrap(err, "net.SplitHostPort error")
	}

	userIP := net.ParseIP(ip)
	if userIP.String() != "<nil>" {
		return userIP.String(), nil
	}

	return "", nil
}
