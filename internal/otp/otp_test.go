package otp

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestGenerateOTP(t *testing.T) {
	// Act.
	otp, err := GenerateOTP()

	// Assert.
	require.NoError(t, err)
	require.Len(t, otp, 6)
}
