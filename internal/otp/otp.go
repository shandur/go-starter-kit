package otp

import (
	"crypto/rand"
	"math/big"
	"strconv"
)

type GenerateOTPFunc func() (string, error)

// TODO: add argument to control OTP length.
// GenerateOTP generates six-digit code.
func GenerateOTP() (string, error) {
	nBig, err := rand.Int(rand.Reader, big.NewInt(999999))
	if err != nil {
		return "", err
	}

	return strconv.FormatInt(nBig.Int64(), 10), nil
}
