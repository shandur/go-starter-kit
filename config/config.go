package config

import (
	"context"
	"fmt"

	"github.com/plaid/go-envvar/envvar"
)

// NewConfig parses the available environment variables.
// If SSM_PATH is defined, variables that are known to the SSM secret behind the
// provided path would be filled. Otherwise returns error.
func NewConfig(ctx context.Context) (Config, error) {
	var ev Config
	if err := envvar.Parse(&ev); err != nil {
		return Config{}, err
	}

	if ev.Service.SSMPath == "" {
		return ev, nil
	}

	params, err := fetchSecrets(ctx, ev.Service.SSMPath)
	if err != nil {
		return Config{}, fmt.Errorf("NewConfig SSM: couldn't fetch secrets: %s", err)
	}

	err = PopulateSecrets(&ev, func(key string) (string, error) {
		val, ok := params[key]
		if !ok {
			return "", fmt.Errorf("NewConfig SSM: path %s: undefined value for %s", ev.Service.SSMPath, key)
		}

		return val, nil
	})
	if err != nil {
		return Config{}, fmt.Errorf("NewConfig couldn't populate secrets: %s", err)
	}

	return ev, nil
}

// ReadEnvVars parses the available environment variables.
// If SSM_PATH is defined, variables that are known to the SSM secret behind the
// provided path would be filled. Otherwise returns error.
func ReadEnvVars(ctx context.Context) (Config, error) {
	var ev Config
	if err := envvar.Parse(&ev); err != nil {
		return Config{}, err
	}

	if ev.Service.SSMPath == "" {
		return ev, nil
	}

	params, err := fetchSecrets(ctx, ev.Service.SSMPath)
	if err != nil {
		return Config{}, fmt.Errorf("ReadEnvVars SSM: couldn't fetch secrets: %s", err)
	}

	err = PopulateSecrets(&ev, func(key string) (string, error) {
		val, ok := params[key]
		if !ok {
			return "", fmt.Errorf("ReadEnvVars SSM: path %s: undefined value for %s", ev.Service.SSMPath, key)
		}

		return val, nil
	})
	if err != nil {
		return Config{}, fmt.Errorf("ReadEnvVars couldn't populate secrets: %s", err)
	}

	return ev, nil
}
