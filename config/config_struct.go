package config

// Config is a map of environment variables with their default values.
// If value doesn't have `default` defined - it's required.
type Config struct {
	Service struct {
		// APIDomain is the domain of the API service.
		APIDomain string `envvar:"SERVICE_API_DOMAIN" default:"localhost"`
		// Address on which HTTP API is exposed.
		Addr string `envvar:"HTTP_ADDR" default:":8080"`
		// Graceful shutdown timeout in seconds.
		GraceShutSecs int `envvar:"GRACE_SHUT_TIMEOUT_SECS" default:"10"`
		// Environment label.
		Environment string `envvar:"ENVIRONMENT" default:"local"`
		// If defined - secret variables would be filled from
		// the SSM resource using the corresponding
		// resources names referring by the value passed with
		// secret tag.
		// I.e. SSM_PATH+$(value from `secret` tag)
		SSMPath string `envvar:"SSM_PATH" default:""`
		// If MigrationMode is enabled -
		// application will run in job mode running migrations.
		// I.e. it will migrate the database to the version provided in
		// DB_MIGRATION_VERSION and will exit out.
		MigrationMode bool `envvar:"RUN_MIGRATION" default:"false"`
		// Timeout in seconds, used with ctx.Done() to signal that processing
		// should be stopped for a particular request.
		HTTPRequestTimeoutSecs int `envvar:"HTTP_REQUEST_TIMEOUT_SECS" default:"50"`
		// Timeout in seconds from when a client connection is accepted to when
		// the request body is fully read.
		ServerReadTimeoutSecs int `envvar:"SERVER_READ_TIMEOUT_SECS" default:"60"`
		// Timeout in seconds of the lifetime of ServeHTTP - from the end of the
		// request header read to the end of the response write
		ServerWriteTimeoutSecs int `envvar:"SERVER_READ_TIMEOUT_SECS" default:"60"`
	}
	Storage struct {
		Postgres struct {
			// Database username.
			Username string `envvar:"DB_USERNAME"`
			// Database password.
			//
			// TODO: Should be fixed once access to SSM from the actual application is fixed.
			// Password string `envvar:"DB_PASSWORD" secret:"DB_PASSWORD" default:""`
			Password string `envvar:"DB_PASSWORD"`
			// Database name.
			Name string `envvar:"DB_NAME"`
			// Database host.
			Host string `envvar:"DB_HOST"`
			// Database port.
			Port string `envvar:"DB_PORT" default:"5432"`
			// Database ssl.
			SSL string `envvar:"DB_SSL" default:"require"`

			Migration struct {
				// Path defines where the migration files could be found.
				Path string `envvar:"DB_MIGRATION_PATH" default:""`
				// How many times to retry migration in case of failure.
				MaxRetries int `envvar:"DB_MIGRATION_MAX_RETRIES" default:"0"`
				// What is the expected interval between retries.
				RetryIntervalSecs int `envvar:"DB_MIGRATION_RETRY_INTERVAL_SEC" default:"6"`
			}
		}
	}
	Authentication struct {
		// MockUserUUID is user's UUID which will be set in Context under `handler.ContextUserID` key.
		// This env var is intended to be used for local development only!
		MockUserUUID string `envvar:"AUTH_MOCK_USER_ID" default:""`
		// MockOTPCode is mocked OTP code used to login.
		// This env var is intended to be used for local development only!
		MockOTPCode string `envvar:"AUTH_MOCK_OTP_CODE" default:""`
	}
	Observability struct {
		// Whether or not to enable debug logging.
		EnableDebugLogs bool `envvar:"ENABLE_DEBUG_LOGS" default:"false"`
	}
}

func (c *Config) IsLocalEnvironment() bool {
	return c.Service.Environment == "local"
}
