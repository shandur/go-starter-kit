package config

import (
	"context"

	"strings"

	"fmt"

	"reflect"

	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/ssm"
	"github.com/pkg/errors"
)

const secretTag = "secret"

// SecretPopulateFn is used to populate the EnvVars with secret values.
// Expected to return value or error for provided key which matches the name
// passed with the tag.
type SecretPopulateFn func(key string) (val string, err error)

// PopulateSecrets into the provided struct with populated fields that have
// `secret` tag assigned.
// `secret` is supported only for string fields.
func PopulateSecrets(s interface{}, fn SecretPopulateFn) error {
	switch {
	case s == nil:
		return errors.New("expected non-nil value")
	case fn == nil:
		return errors.New("fn for secret is required")
	}

	t := reflect.TypeOf(s)
	if t.Kind() != reflect.Ptr || t.Elem().Kind() != reflect.Struct {
		return errors.New("expected pointer to struct")
	}

	return populateSecrets(reflect.ValueOf(s), fn)
}

func populateSecrets(v reflect.Value, fn SecretPopulateFn) error {
	t := v.Type()

	// to support nested pointers to structs
	if t.Kind() == reflect.Ptr {
		if v.IsNil() {
			v.Set(reflect.New(v.Type().Elem()))
		}

		v = v.Elem()
		t = v.Type()
	}

	if v.Kind() != reflect.Struct {
		return errors.New("struct is expected")
	}

	// iterate through the fields.
	for i := 0; i < t.NumField(); i++ {
		fv := v.Field(i)
		ft := t.Field(i)

		// recursively go deeper if struct.
		if ft.Type.Kind() == reflect.Struct || (ft.Type.Kind() == reflect.Ptr && fv.Type().Elem().Kind() == reflect.Struct) {
			if err := populateSecrets(fv, fn); err != nil {
				return fmt.Errorf("%s: %s", ft.Name, err)
			}
			continue
		}

		// check if has secret tag.
		name, ok := ft.Tag.Lookup(secretTag)
		if !ok {
			continue
		}
		if name == "" {
			return fmt.Errorf("%s: empty value for tag `%s`", ft.Name, secretTag)
		}

		// make sure it's a string
		if ft.Type.Kind() != reflect.String {
			return fmt.Errorf("%s is not of kind string", ft.Name)
		}

		// get the value
		val, err := fn(name)
		if err != nil {
			return err
		}

		if !fv.CanSet() {
			return fmt.Errorf("cannot set value for %s: not exported?", ft.Name)
		}

		// set the value
		fv.SetString(val)
	}

	return nil
}

func fetchSecrets(ctx context.Context, path string) (map[string]string, error) {
	s, err := session.NewSession()
	if err != nil {
		return nil, err
	}

	var (
		nextToken *string
		ssmParams []*ssm.Parameter
	)

	// Doing an initial fetch of secrets.
	// We can only fetch at most 10 secrets at a time.
	// If there are more than 10 secrets, a nextToken is provided,
	// which we can be used in subsequent requests to fetch remaining secrets.
	res, err := fetchParametersFromSSM(ctx, s, path, nextToken)
	if err != nil {
		return nil, errors.Wrap(err, "fetchSecrets fetchParametersFromSSM error")
	}

	ssmParams = append(ssmParams, res.Parameters...)
	nextToken = res.NextToken

	// We should fetch secrets until `nextToken` is empty to collect the full list of parameters.
	for nextToken != nil {
		res, err = fetchParametersFromSSM(ctx, s, path, nextToken)
		if err != nil {
			return nil, errors.Wrap(err, "fetchSecrets fetchParametersFromSSM error")
		}

		ssmParams = append(ssmParams, res.Parameters...)
		nextToken = res.NextToken
	}

	params := map[string]string{}
	for _, p := range ssmParams {
		params[strings.TrimPrefix(*p.Name, path)] = *p.Value
	}

	return params, nil
}

func fetchParametersFromSSM(ctx context.Context, session *session.Session, path string, nextToken *string) (*ssm.GetParametersByPathOutput, error) {
	var (
		decr      = true
		recursive = true
	)

	return ssm.New(session).GetParametersByPathWithContext(ctx, &ssm.GetParametersByPathInput{
		WithDecryption: &decr,
		Path:           &path,
		Recursive:      &recursive,
		NextToken:      nextToken,
	})
}
