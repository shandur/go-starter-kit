package config

import (
	"errors"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestPopulateSecrets(t *testing.T) {
	t.Parallel()
	tt := []struct {
		name   string
		data   func(t *testing.T) (interface{}, func(*testing.T, error))
		secret SecretPopulateFn
	}{{
		name: "returns error if nil struct is passed",
		data: func(t *testing.T) (interface{}, func(*testing.T, error)) {
			return nil, func(t *testing.T, err error) {
				require.EqualError(t, err, "expected non-nil value")
			}
		},
	}, {
		name: "returns error if no fn is passed",
		data: func(t *testing.T) (interface{}, func(*testing.T, error)) {
			return &struct{}{}, func(t *testing.T, err error) {
				require.EqualError(t, err, "fn for secret is required")
			}
		},
	}, {
		name: "returns error if not a pointer is passed",
		data: func(t *testing.T) (interface{}, func(*testing.T, error)) {
			return struct{}{}, func(t *testing.T, err error) {
				require.EqualError(t, err, "expected pointer to struct")
			}
		},
		secret: func(key string) (string, error) {
			return "", nil
		},
	}, {
		name: "returns error if pointer to not struct is passed",
		data: func(t *testing.T) (interface{}, func(*testing.T, error)) {
			a := ""
			return &a, func(t *testing.T, err error) {
				require.EqualError(t, err, "expected pointer to struct")
			}
		},
		secret: func(key string) (string, error) {
			return "", nil
		},
	}, {
		name: "returns error if secret tag is set not on string",
		data: func(t *testing.T) (interface{}, func(*testing.T, error)) {
			return &struct {
					A int64 `secret:"A"`
				}{}, func(t *testing.T, err error) {
					require.EqualError(t, err, "A is not of kind string")
				}
		},
		secret: func(key string) (string, error) {
			return "", nil
		},
	}, {
		name: "returns error if secret fn returns error",
		data: func(t *testing.T) (interface{}, func(*testing.T, error)) {
			return &struct {
					A string `secret:"A"`
				}{}, func(t *testing.T, err error) {
					require.EqualError(t, err, "oops")
				}
		},
		secret: func(key string) (string, error) {
			return "", errors.New("oops")
		},
	}, {
		name: "should work",
		data: func(t *testing.T) (interface{}, func(*testing.T, error)) {
			a := struct {
				A string `secret:"A"`
			}{}
			return &a, func(t *testing.T, err error) {
				require.NoError(t, err)
				require.Equal(t, "a", a.A)
			}
		},
		secret: func(key string) (string, error) {
			return "a", nil
		},
	}, {
		name: "skips fields without tag",
		data: func(t *testing.T) (interface{}, func(*testing.T, error)) {
			a := struct {
				A struct {
					C string `secret:"C"`
					B string
				}
			}{}
			return &a, func(t *testing.T, err error) {
				require.NoError(t, err)
				require.Equal(t, a.A.C, "c")
				require.Equal(t, a.A.B, "")
			}
		},
		secret: func(key string) (string, error) {
			return "c", nil
		},
	}, {
		name: "returns error if field is not exported",
		data: func(t *testing.T) (interface{}, func(*testing.T, error)) {
			a := struct {
				A struct {
					b string `secret:"B"`
				}
			}{}
			return &a, func(t *testing.T, err error) {
				require.EqualError(t, err, "A: cannot set value for b: not exported?")
			}
		},
		secret: func(key string) (string, error) {
			return "b", nil
		},
	}, {
		name: "returns error if name is not provided",
		data: func(t *testing.T) (interface{}, func(*testing.T, error)) {
			a := struct {
				A struct {
					b string `secret:""`
				}
			}{}
			return &a, func(t *testing.T, err error) {
				require.EqualError(t, err, "A: b: empty value for tag `secret`")
			}
		},
		secret: func(key string) (string, error) {
			return "b", nil
		},
	}, {
		name: "should work for nested structs",
		data: func(t *testing.T) (interface{}, func(*testing.T, error)) {
			a := struct {
				A struct {
					B string `secret:"B"`
				}
			}{}
			return &a, func(t *testing.T, err error) {
				require.NoError(t, err)
				require.Equal(t, "b", a.A.B)
			}
		},
		secret: func(key string) (string, error) {
			return "b", nil
		},
	}, {
		name: "should work for nested pointers to structs",
		data: func(t *testing.T) (interface{}, func(*testing.T, error)) {
			a := struct {
				A *struct {
					B string `secret:"B"`
				}
			}{}
			return &a, func(t *testing.T, err error) {
				require.NoError(t, err)
				require.Equal(t, "b", a.A.B)
			}
		},
		secret: func(key string) (string, error) {
			return "b", nil
		},
	}}

	for _, test := range tt {
		td, ran := test.data(t)
		err := PopulateSecrets(td, test.secret)
		ran(t, err)
	}
}
