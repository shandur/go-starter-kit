package main

import (
	"context"
	"os"
	"os/signal"
	"syscall"
)

// ProcessManager defines a global context that can be used for canceling the context
// and shutdown signal channel to handle shutdown signals.
type ProcessManager struct {
	Context  context.Context
	Shutdown chan os.Signal
	Cancel   context.CancelFunc
}

// NewProcessManager creates a process manager with a context and shutdown signal handler.
func NewProcessManager() ProcessManager {
	var pm ProcessManager
	pm.Context, pm.Cancel = context.WithCancel(context.Background())

	// Listen for shutdown signals.
	pm.Shutdown = make(chan os.Signal, 1)
	signal.Notify(pm.Shutdown, syscall.SIGTERM)
	signal.Notify(pm.Shutdown, syscall.SIGINT)

	return pm
}
