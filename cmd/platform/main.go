package main

import (
	"context"
	"net/http"
	"time"

	"gitlab.mooncascade.net/roman/go-starter-kit/config"
	httphandler "gitlab.mooncascade.net/roman/go-starter-kit/handler/http"
	"gitlab.mooncascade.net/roman/go-starter-kit/internal/db"
	"gitlab.mooncascade.net/roman/go-starter-kit/internal/logging"
	"go.uber.org/zap"
)

// nolint: go-lint
func main() {
	logger := logging.GetLogger()

	// Propagate this process manager to services
	// to respect the graceful shutdown.
	globalPM := NewProcessManager()

	// Parse the environment variables for further usage.
	env, err := config.NewConfig(globalPM.Context)
	if err != nil {
		logger.Fatal("Config: couldn't parse", zap.Error(err))
	}

	// If debug logs are desired, enable and reload logger.
	if env.Observability.EnableDebugLogs {
		logging.EnableDebugLogs()
		logger = logging.GetLogger()
	}
	logger.Debug("Config: OK")

	// Initialize storage.
	storage, err := db.GetPostgresClient(globalPM.Context, &env)
	if err != nil {
		logger.Fatal("Storage failed to initialize", zap.Error(err))
	}
	logger.Debug("Storage: OK")

	// We are not handling the error on shutdown
	defer storage.Close() // nolint: errcheck

	storageDB, err := storage.Client()
	if err != nil {
		logger.Fatal("Migration: storage: couldn't fetch client", zap.Error(err))
	}

	// Please read comment for env.Service.MigrationMode
	// to learn more about Migration Mode.
	err = db.Migrate(&env, storageDB, env.Service.MigrationMode)
	if err != nil {
		logger.Fatal("Migration failure", zap.Error(err))
	}

	h, err := httphandler.New(env, storage.Conn)
	if err != nil {
		logger.Fatal("HTTP Handler", zap.Error(err))
	}
	logger.Debug("HTTP Handler: OK")

	// Initialize the server.
	// TODO: Use ProcessManager context.
	srv := http.Server{
		Addr:         env.Service.Addr,
		Handler:      h,
		ReadTimeout:  time.Duration(env.Service.ServerReadTimeoutSecs) * time.Second,
		WriteTimeout: time.Duration(env.Service.ServerWriteTimeoutSecs) * time.Second,
	}

	go func() {
		if err := srv.ListenAndServe(); err != nil {
			logger.Error("HTTP Server: stopped:", zap.Error(err))
			globalPM.Cancel()
			return
		}
	}()

	logger.Info("HTTP Server: up and running", zap.String("address", srv.Addr))

	// wait for either shutdown or global context cancellation.
	logger.Info("Listening for the shutdown.")
	select {
	case <-globalPM.Shutdown:
		logger.Info("Shutdown signal received.")
		globalPM.Cancel()
	case <-globalPM.Context.Done():
		logger.Info("Global context was cancelled.")
	}

	logger.Info("Shutting down.")

	srvContext, srvCancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer srvCancel()

	if err := srv.Shutdown(srvContext); err != nil {
		logger.Error("HTTP Server: couldn't shutdown", zap.Error(err))
	}
	logger.Debug("HTTP Server: Stopped")

	// wait for few seconds to make sure that http router is closed and
	// that workers are also done.
	<-time.After(time.Duration(env.Service.GraceShutSecs) * time.Second)

	logger.Debug("Bye.")
}
