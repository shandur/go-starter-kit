GOCMD=go
GORUN=$(GOCMD) run
GOTEST=$(GOCMD) test
DOCKER_EXEC_API=docker-compose exec api


golangci-lint = docker run -it --rm \
	-v $(PWD):/platform \
	-w /platform \
	golangci/golangci-lint:v1.39-alpine

# Make eats one dollar sign.
curr_date = $$(date +%Y%m%d%H%M%S)

.PHONY: up
up:
	docker network create starter || true
	docker-compose up --build -d

.PHONY: db-up
db-up:
	docker network create starter || true
	docker-compose up -d db

.PHONY: down
down:
	docker-compose down -v

.PHONY: logs
logs:
	docker-compose logs -f

.PHONY: psql
psql:
	docker-compose run db psql -h db -U starter starter

.PHONY: tidy
tidy:
	@echo "> Tidying..."
	@go mod tidy
	@go mod vendor
	@go fmt ./...
	@echo "> Done!"

.PHONY: fmt
fmt:
	@go fmt ./...

.PHONY: lint
lint:
	@echo "> Linting..."
	$(golangci-lint) golangci-lint run -c .golangci.yml -v
	@echo "> Done!"

.PHONY: test
test:
	@echo "> Running tests against local DB..."
	@set -a && . ./.env/api-local.env && set +a && \
	go test -cover -race -p 1 $(ARGS) ./...
	@echo "> Done!"

.PHONY: run
run:
	@set -a && . ./.env/api-local.env && set +a && \
	go run cmd/platform/*.go

.PHONY: migration
migration:
	@touch ./storage/postgres/migration/$(curr_date)_$(name).up.sql
	@echo "> Migration is generated: ./storage/postgres/migration/$(curr_date)_$(name).up.sql"
	@echo "-- No rollbacks." > ./storage/postgres/migration/$(curr_date)_$(name).down.sql
	@echo "> Migration is generated: ./storage/postgres/migration/$(curr_date)_$(name).down.sql"
