package storage

import (
	"context"
	"testing"

	sq "github.com/Masterminds/squirrel"
	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/stretchr/testify/require"
	"gitlab.mooncascade.net/roman/go-starter-kit/config"
	"gitlab.mooncascade.net/roman/go-starter-kit/internal/db"
)

type cleanupFunc func()

// userFixtures encapsulates dependencies used across tests
type userFixtures struct {
	dbManager  DBManager
	firstUser  User
	secondUser User
	cleanup    cleanupFunc
}

// setupUserTests creates a postgres client, runs a migration, clears out the `users` table
// and returns TextFixtures for common dependencies in tests
func setupUserTests(t *testing.T) userFixtures {
	ctx := context.Background()
	env, err := config.ReadEnvVars(ctx)
	require.NoError(t, err, "parsing environment variables")
	pgClient, err := db.GetPostgresClient(ctx, &env)
	require.NoError(t, err, "creating postgres client")
	conn := pgClient.Conn

	runMigration := true
	err = db.Migrate(&env, pgClient.DB.DB, runMigration)
	require.NoError(t, err, "running migration")

	statementBuilder := sq.StatementBuilder.PlaceholderFormat(sq.Dollar)
	dbManagerConfig := DBConfig{Conn: conn, StatementBuilder: &statementBuilder}
	dbManager, err := NewDBManager(dbManagerConfig)
	require.NoError(t, err, "could not create db manager")

	cleanup := func() {
		err = usersTestTruncate(ctx, conn)
		require.NoError(t, err, "Could not clean up users table")

		err = userAuthSessionsTestTruncate(ctx, conn)
		require.NoError(t, err, "Could not clean up user_auth_sessions table")

		conn.Close()
	}

	firstUser := User{
		FirstName:    "First",
		LastName:     "User",
		EmailAddress: "user1@test.com",
	}

	secondUser := User{
		FirstName:    "Second",
		LastName:     "User",
		EmailAddress: "user2@test.com",
	}

	return userFixtures{
		dbManager:  dbManager,
		firstUser:  firstUser,
		secondUser: secondUser,
		cleanup:    cleanup,
	}
}

func TestUserInsert(t *testing.T) {
	t.Run("Should create new user", func(t *testing.T) {
		// Arrange.
		fixtures := setupUserTests(t)
		defer fixtures.cleanup()

		user := fixtures.firstUser
		ctx := context.Background()

		// Act.
		newUser, err := fixtures.dbManager.CreateUser(ctx, user)

		// Assert.
		require.NoError(t, err)
		require.NotEmpty(t, newUser.ID)
		require.Equal(t, newUser.FirstName, user.FirstName)
		// If no role is set, user should default to Guest
		require.Equal(t, string(UserRoleTypeGuest), newUser.Role)
	})

	t.Run("errors when creating a duplicate user with existing email", func(t *testing.T) {
		// Arrange.
		fixtures := setupUserTests(t)
		defer fixtures.cleanup()

		user := fixtures.firstUser
		ctx := context.Background()

		// Act.
		newUser, err := fixtures.dbManager.CreateUser(ctx, user)

		// Assert.
		require.NoError(t, err)
		require.NotEmpty(t, newUser.ID)
		require.Equal(t, newUser.EmailAddress, user.EmailAddress)

		// Act.
		// Errors when attempting to create a second user with the same email
		secondUser := fixtures.secondUser
		secondUser.EmailAddress = user.EmailAddress

		_, err = fixtures.dbManager.CreateUser(ctx, secondUser)

		// Assert.
		require.Error(t, err)

		// Act.
		// Errors when attempting to create a second user with same
		// email that is formatted differently.
		secondUser.EmailAddress = "UsEr2@tEsT.CoM"
		_, err = fixtures.dbManager.CreateUser(ctx, user)

		// Assert.
		require.EqualError(t, err, ErrAlreadyExists.Error())
	})
}

func TestGetUser(t *testing.T) {
	t.Run("Should return user by UUID", func(t *testing.T) {
		// Arrange.
		fixtures := setupUserTests(t)
		defer fixtures.cleanup()
		user := fixtures.firstUser

		ctx := context.Background()
		newUser, err := fixtures.dbManager.CreateUser(ctx, user)
		require.NoError(t, err)
		userUUID := newUser.ID
		require.NotEmpty(t, userUUID)

		// Find the user we just created
		sameUser, err := fixtures.dbManager.GetUserByUUID(ctx, userUUID)
		require.NoError(t, err)
		require.Equal(t, sameUser.FirstName, user.FirstName)
	})
	t.Run("Should return user by Email", func(t *testing.T) {
		fixtures := setupUserTests(t)
		defer fixtures.cleanup()
		user := fixtures.firstUser
		dbManager := fixtures.dbManager

		ctx := context.Background()
		newUser, err := dbManager.CreateUser(ctx, user)
		require.NoError(t, err)
		userEmail := newUser.EmailAddress
		require.NotEmpty(t, userEmail)

		// Find the user we just created
		sameUser, err := dbManager.GetUserByEmail(ctx, userEmail)
		require.NoError(t, err)
		require.Equal(t, sameUser.FirstName, user.FirstName)

		// Delete the user and ensure we can no longer retrieve them by email
		err = dbManager.RemoveUser(ctx, sameUser.ID)
		require.NoError(t, err)

		_, err = dbManager.GetUserByEmail(ctx, userEmail)
		require.Equal(t, err, ErrNotFound)
	})
}

func TestUserRemove(t *testing.T) {
	t.Run("removes a user", func(t *testing.T) {
		fixtures := setupUserTests(t)
		defer fixtures.cleanup()
		user := fixtures.firstUser
		dbManager := fixtures.dbManager

		ctx := context.Background()
		newUser, err := dbManager.CreateUser(ctx, user)
		require.NoError(t, err)
		require.NotEmpty(t, newUser.ID)

		userUUID := newUser.ID
		err = dbManager.RemoveUser(ctx, userUUID)
		require.NoError(t, err)

		_, err = dbManager.GetUserByUUID(ctx, userUUID)
		require.Equal(t, err, ErrNotFound)
	})

	t.Run("removes a user and user auth session", func(t *testing.T) {
		// Arrange,
		fixtures := setupUserTests(t)
		defer fixtures.cleanup()

		user := fixtures.firstUser
		dbManager := fixtures.dbManager

		ctx := context.Background()
		newUser, err := dbManager.CreateUser(ctx, user)
		require.NoError(t, err)
		require.NotEmpty(t, newUser.ID)

		userAuthSession := UserAuthSession{
			UserID: newUser.ID,
		}
		_, err = dbManager.UpsertUserAuthSession(ctx, userAuthSession)
		require.NoError(t, err)

		// Act.
		userUUID := newUser.ID
		err = dbManager.RemoveUser(ctx, userUUID)
		require.NoError(t, err)

		// Assert.
		_, err = dbManager.GetUserByUUID(ctx, userUUID)
		require.Equal(t, err, ErrNotFound)

		_, err = dbManager.GetUserAuthSessionByUserID(ctx, userUUID)
		require.Equal(t, err, ErrNotFound)
	})
}

func TestUserEdit(t *testing.T) {
	t.Run("edits a user", func(t *testing.T) {
		// Arrange.
		fixtures := setupUserTests(t)
		defer fixtures.cleanup()

		user := fixtures.firstUser
		dbManager := fixtures.dbManager

		ctx := context.Background()
		newUser, err := dbManager.CreateUser(ctx, user)
		require.NoError(t, err)
		require.NotEmpty(t, newUser.ID)

		userUUID := newUser.ID
		newName := "UpdatedName"
		userUpdate := UserUpdate{
			ID:        userUUID,
			FirstName: &newName,
		}

		// Act.
		updatedUser, err := dbManager.EditUser(ctx, userUpdate)

		// Asserts.
		require.NoError(t, err)
		require.Equal(t, updatedUser.FirstName, newName)
		// Assert other fields didn't change
		require.Equal(t, updatedUser.LastName, newUser.LastName)
	})
}

func TestUserFind(t *testing.T) {
	t.Run("finds users by IDs", func(t *testing.T) {
		// Arrange.
		fixtures := setupUserTests(t)
		defer fixtures.cleanup()

		user := fixtures.firstUser
		dbManager := fixtures.dbManager

		ctx := context.Background()
		newUser, err := dbManager.CreateUser(ctx, user)
		require.NoError(t, err)
		require.NotEmpty(t, newUser.ID)

		secondUser, err := dbManager.CreateUser(ctx, fixtures.secondUser)
		require.NoError(t, err)

		// Act.
		// Find should return both users
		userQuery := UsersQuery{
			IDs: []uuid.UUID{newUser.ID, secondUser.ID},
		}
		returnedUsers, err := dbManager.FindUsers(ctx, userQuery)

		// Assert.
		require.NoError(t, err)
		require.Equal(t, 2, len(returnedUsers))
	})

	t.Run("finds users by emails", func(t *testing.T) {
		// Arrange
		fixtures := setupUserTests(t)
		defer fixtures.cleanup()

		user := fixtures.firstUser
		dbManager := fixtures.dbManager

		// Act
		ctx := context.Background()
		newUser, err := dbManager.CreateUser(ctx, user)
		require.NoError(t, err)
		require.NotEmpty(t, newUser.ID)

		// Assert,
		secondUser, err := dbManager.CreateUser(ctx, fixtures.secondUser)
		require.NoError(t, err)

		userQuery := UsersQuery{
			Emails: []string{newUser.EmailAddress, secondUser.EmailAddress},
		}
		returnedUsers, err := dbManager.FindUsers(ctx, userQuery)
		require.NoError(t, err)
		require.Equal(t, 2, len(returnedUsers))
	})
}

func usersTestTruncate(ctx context.Context, conn *pgxpool.Pool) error {
	_, err := conn.Exec(ctx, `truncate users cascade;`)
	return err
}
