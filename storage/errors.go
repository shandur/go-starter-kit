package storage

import "errors"

var (
	ErrNotFound      = errors.New("row(s) not found")
	ErrAlreadyExists = errors.New("entity already exists")
)
