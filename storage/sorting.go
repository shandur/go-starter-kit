package storage

import (
	"fmt"

	sq "github.com/Masterminds/squirrel"
	"github.com/iancoleman/strcase"
)

type SortDirection string

const (
	SortDirectionAscending  SortDirection = "ASC"
	SortDirectionDescending SortDirection = "DESC"
)

type SortPair struct {
	Field     string
	Direction SortDirection
}

type SortOptions []SortPair

// generateOrderBy adds ORDER BY clause(s) to the provided query builder
// using the provided sort options. An instance of the entity being
// queried is used to filter out any invalid column names from the sort
// options.
func generateOrderBy(
	query sq.SelectBuilder, sortOptions SortOptions, entity interface{},
) sq.SelectBuilder {
	// Generate column names from structure of entity
	validColumns := getColumnNames(entity)

	var orderBys []string
	for _, sortPair := range sortOptions {
		// Convert to snake_case for compatibility with DB column names
		field := strcase.ToSnake(sortPair.Field)
		// Only add ORDER BY clause if the field name is a valid column name
		if _, exists := validColumns[field]; exists {
			orderBys = append(
				orderBys, fmt.Sprintf("%s %s", field, sortPair.Direction),
			)
		}
	}

	if orderBys != nil {
		query = query.OrderBy(orderBys...)
	}
	return query
}
