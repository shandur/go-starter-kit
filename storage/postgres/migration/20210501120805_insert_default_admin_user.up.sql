-- Inserting default admin user.
-- It might be required to solve `chicken vs egg` problem - we should have initial user in the system.
BEGIN;

WITH
-- Create default admin user.
new_users AS (
  INSERT INTO users (
  id,
  first_name,
  last_name,
  email_address,
  role,
  created_at,
  modified_at
  ) VALUES(
    '6310c823-4aec-4ec4-8a79-c4286a9ba6b1',
    'Admin',
    'Internal',
    'admin@mooncascade.com',
    'admin',
    NOW(),
    NOW()
  ) ON CONFLICT("email_address") DO UPDATE SET email_address=EXCLUDED.email_address
  RETURNING *
)
-- Create default user auth session.
INSERT INTO user_auth_sessions (
      user_id,
      created_at,
      modified_at
  )
SELECT id as user_id,
NOW() as created_at,
NOW() as modified_at
FROM new_users;

COMMIT;
