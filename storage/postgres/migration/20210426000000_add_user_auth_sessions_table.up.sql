-- `user_auth_sessions` table keeps user's OTP code details and refresh tokens issued on successful login.
CREATE TABLE IF NOT EXISTS user_auth_sessions (
	id uuid DEFAULT uuid_generate_v4 () PRIMARY KEY,

	-- The ID of the user that intitiates an auth session.
	user_id uuid references users(id) NOT NULL UNIQUE,

	-- The timestamp when the last OTP has been sent.
	otp_code_last_sent_at timestamp without time zone NULL,

	otp_code varchar(8) NULL,

	created_at timestamp without time zone NOT NULL,
	modified_at timestamp without time zone NOT NULL
);
