CREATE extension IF NOT EXISTS "uuid-ossp";

CREATE TABLE IF NOT EXISTS users (
    id uuid DEFAULT uuid_generate_v4() PRIMARY KEY,
    first_name text NOT NULL,
    last_name text NOT NULL,

    email_address text NOT NULL UNIQUE,

    "role" varchar(80),

    -- created_at and modified_at are internal timestamps to specify when the 
    -- row was created in DB.
    created_at timestamp without time zone NOT NULL,
    modified_at timestamp without time zone
);
