package postgres

import (
	"context"
	"database/sql"
	"fmt"
	"time"

	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/jmoiron/sqlx"
	"github.com/pkg/errors"
	"gitlab.mooncascade.net/roman/go-starter-kit/internal/logging"
	"go.uber.org/zap"
)

const (
	pingTimeout   = 5 * time.Second
	retryInterval = 3 * time.Second
	maxRetries    = 5
)

// Options contains the postgres connection options.
type Options struct {
	Host     string
	Port     string
	DBName   string
	Username string
	Password string
	SSLMode  string
}

// Validate validates the postgres connection options.
func (o *Options) Validate() error {
	switch {
	case o.Username == "":
		return errors.New("username is required")
	case o.Password == "":
		return errors.New("password is required")
	case o.Host == "":
		return errors.New("host is required")
	case o.Port == "":
		return errors.New("port is required")
	case o.DBName == "":
		return errors.New("DBName is required")
	case o.SSLMode != "require" && o.SSLMode != "disable":
		return errors.New("invalid SSLMode")
	}

	return nil
}

func formatDSN(o *Options) string {
	return fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=%s",
		o.Host, o.Port, o.Username, o.Password, o.DBName, o.SSLMode)
}

// Client contains the postgres DB connection.
type Client struct {
	Conn *pgxpool.Pool
	DB   *sqlx.DB
}

// New creates a new postgres DB connection.
func New(ctx context.Context, opt Options) (*Client, error) {
	logger := logging.GetLogger(ctx)

	// validate options
	if err := opt.Validate(); err != nil {
		return nil, err
	}

	// connect to DB and verify with a ping
	ctx, cancel := context.WithTimeout(ctx, pingTimeout)
	defer cancel()

	var conn *pgxpool.Pool
	var err error
	retries := 0
	for {
		logger.Debug("postgres retries", zap.Int("count", retries))
		conn, err = openAndPing(ctx, &opt)
		if err == nil {
			break
		}

		// retry
		if retries >= maxRetries {
			return nil, err
		}
		retries++
		time.Sleep(retryInterval)
		continue
	}

	psqlInfo := formatDSN(&opt)
	db, err := CreateSQLDB(ctx, psqlInfo)
	if err != nil {
		return nil, err
	}

	return &Client{Conn: conn, DB: db}, nil
}

func openAndPing(ctx context.Context, opt *Options) (*pgxpool.Pool, error) {
	logger := logging.GetLogger(ctx)
	connectionString := fmt.Sprintf("postgres://%s:%s@%s:%s/%s?sslmode=%s",
		opt.Username, opt.Password, opt.Host, opt.Port, opt.DBName, opt.SSLMode)

	config, err := pgxpool.ParseConfig(connectionString)
	if err != nil {
		logger.Error("postgres: error parsing dsn", zap.Error(err))
		return nil, errors.Wrap(err, "parsing dsn")
	}

	pool, err := pgxpool.ConnectConfig(ctx, config)
	if err != nil {
		logger.Error("postgres: error creating connection pool", zap.Error(err))
		return nil, errors.Wrap(err, "creating connpool")
	}
	logger.Debug("postgres: created the connection pool")
	err = pool.Ping(ctx)
	if err != nil {
		logger.Error("Error pinging postgres", zap.Error(err))
		return nil, errors.Wrap(err, "pinging postgres")
	}
	logger.Debug("postgres: pinged successfully")

	return pool, nil
}

// Client returns native sql.DB client.
func (c *Client) Client() (*sql.DB, error) {
	if c.DB == nil {
		return nil, errors.New("client is not defined")
	}

	return c.DB.DB, nil
}

// Close closes the postgres DB connection.
func (c *Client) Close() error {
	return c.DB.Close()
}

// CreateSQLDB creates a native SQL connection for migrations.
func CreateSQLDB(ctx context.Context, psqlInfo string) (*sqlx.DB, error) {
	return sqlx.ConnectContext(ctx, "postgres", psqlInfo)
}
