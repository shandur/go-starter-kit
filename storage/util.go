package storage

import (
	"reflect"
	"time"

	"github.com/patrickmn/go-cache"
)

var (
	columnNameCache = cache.New(5*time.Minute, 10*time.Minute)
)

// getColumnNames uses reflect to return a map of column names
// based on the `db` struct tag.
func getColumnNames(entity interface{}) map[string]bool {
	entityType := reflect.TypeOf(entity) // e.g. storage.Project
	cacheKey := entityType.String()
	cachedColumnNames, found := columnNameCache.Get(cacheKey)
	if found {
		return cachedColumnNames.(map[string]bool)
	}

	columns := make(map[string]bool)
	for i := 0; i < entityType.NumField(); i++ {
		columnName := entityType.Field(i).Tag.Get("db")
		columns[columnName] = true
	}

	columnNameCache.Set(cacheKey, columns, cache.NoExpiration)
	return columns
}
