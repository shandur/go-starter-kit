package storage

import (
	"context"
	"time"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4"
	"github.com/pkg/errors"

	"gitlab.mooncascade.net/roman/go-starter-kit/internal/db"
)

// UserAuthSession represents authentication details of a user.
type UserAuthSession struct {
	ID                uuid.UUID  `db:"id"`
	UserID            uuid.UUID  `db:"user_id"`
	OTPCode           *string    `db:"otp_code"`
	OTPCodeLastSentAt *time.Time `db:"otp_code_last_sent_at"`
	CreatedAt         time.Time  `db:"created_at"`
	ModifiedAt        time.Time  `db:"modified_at"`
}

// UpsertUserAuthSession upserts a user auth session, i.e,
// if a row for such a `user_id` exists, it updates a row.
// Otherwise, a new row will be created.
func (d *dbManager) UpsertUserAuthSession(ctx context.Context, authSession UserAuthSession) (*UserAuthSession, error) {
	createdAt := db.ToUTC(time.Now())
	modifiedAt := createdAt

	if authSession.OTPCodeLastSentAt != nil {
		otpLastCodeSentAt := db.ToUTC(*authSession.OTPCodeLastSentAt)
		authSession.OTPCodeLastSentAt = &otpLastCodeSentAt
	}

	insertQuery, args, err := d.StatementBuilder.
		Insert("user_auth_sessions").
		Columns("user_id", "otp_code", "otp_code_last_sent_at", "created_at", "modified_at").
		Values(authSession.UserID, authSession.OTPCode, authSession.OTPCodeLastSentAt, createdAt, modifiedAt).
		Suffix(`ON CONFLICT (user_id) DO UPDATE SET
		otp_code=COALESCE(NULLIF(EXCLUDED.otp_code, ''), user_auth_sessions.otp_code),
			otp_code_last_sent_at=COALESCE(EXCLUDED.otp_code_last_sent_at, user_auth_sessions.otp_code_last_sent_at),
			modified_at=NOW()
		`).
		Suffix(`RETURNING id, user_id, otp_code, otp_code_last_sent_at, created_at, modified_at`).
		ToSql()
	if err != nil {
		return nil, errors.Wrap(err, "constructing user auth session upsert query")
	}

	var newAuthSession UserAuthSession
	err = d.Conn.QueryRow(ctx, insertQuery, args...).
		Scan(
			&newAuthSession.ID, &newAuthSession.UserID, &newAuthSession.OTPCode,
			&newAuthSession.OTPCodeLastSentAt,
			&newAuthSession.CreatedAt, &newAuthSession.ModifiedAt,
		)
	if err != nil {
		return nil, errors.Wrap(err, "upserting user auth session")
	}
	return &newAuthSession, nil
}

func (d *dbManager) GetUserAuthSessionByUserID(ctx context.Context, userUUID uuid.UUID) (*UserAuthSession, error) {
	var userAuthSession UserAuthSession
	err := d.Conn.QueryRow(ctx, `
		SELECT uas.id, uas.user_id, uas.otp_code, uas.otp_code_last_sent_at, uas.created_at, uas.modified_at
		FROM user_auth_sessions uas
		WHERE uas.user_id = $1
	`, userUUID).
		Scan(
			&userAuthSession.ID, &userAuthSession.UserID, &userAuthSession.OTPCode,
			&userAuthSession.OTPCodeLastSentAt,
			&userAuthSession.CreatedAt, &userAuthSession.ModifiedAt,
		)
	if err != nil {
		if err == pgx.ErrNoRows {
			return nil, ErrNotFound
		}
		return nil, errors.Wrapf(err, "finding user auth session")
	}
	return &userAuthSession, nil
}

// RemoveUserAuthSessionByUserID soft-deletes a user auth session.
// Requires passing a transaction - currently provided by RemoveUser().
func (d *dbManager) RemoveUserAuthSessionByUserID(
	ctx context.Context, tx pgx.Tx, userUUID uuid.UUID,
) error {
	removeQuery, args, err := d.StatementBuilder.
		Delete("user_auth_sessions").
		Where("user_id = ?", userUUID).
		ToSql()
	if err != nil {
		return errors.Wrap(err, "constructing remove statement")
	}

	commandTag, err := tx.Exec(ctx, removeQuery, args...)
	if err != nil {
		return errors.Wrap(err, "removing user auth session")
	}
	if commandTag.RowsAffected() == 0 {
		return ErrNotFound
	}

	return nil
}
