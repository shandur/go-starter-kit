package storage

import (
	"context"
	"testing"
	"time"

	sq "github.com/Masterminds/squirrel"
	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/stretchr/testify/require"

	"gitlab.mooncascade.net/roman/go-starter-kit/config"
	"gitlab.mooncascade.net/roman/go-starter-kit/internal/db"
	"gitlab.mooncascade.net/roman/go-starter-kit/internal/strutil"
)

// userFixtures encapsulates dependencies used across tests.
type userAuthSessionFixtures struct {
	dbManager DBManager
	user      User
	cleanup   cleanupFunc
}

// setupUserAuthSessionTests creates a postgres client, runs a migration,
// clears out the `users`, `user_auth_sessions` tables
// and returns TextFixtures for common dependencies in tests.
func setupUserAuthSessionTests(t *testing.T) userAuthSessionFixtures {
	ctx := context.Background()
	env, err := config.ReadEnvVars(ctx)
	require.NoError(t, err, "parsing environment variables")
	pgClient, err := db.GetPostgresClient(ctx, &env)
	require.NoError(t, err, "creating postgres client")
	conn := pgClient.Conn

	runMigration := true
	err = db.Migrate(&env, pgClient.DB.DB, runMigration)
	require.NoError(t, err, "running migration")

	statementBuilder := sq.StatementBuilder.PlaceholderFormat(sq.Dollar)
	dbManagerConfig := DBConfig{Conn: conn, StatementBuilder: &statementBuilder}
	dbManager, err := NewDBManager(dbManagerConfig)
	require.NoError(t, err, "could not create db manager")

	err = usersTestTruncate(ctx, conn)
	require.NoError(t, err, "Could not clean up users table")

	cleanup := func() {
		err = usersTestTruncate(ctx, conn)
		require.NoError(t, err, "Could not clean up users table")

		err = userAuthSessionsTestTruncate(ctx, conn)
		require.NoError(t, err, "Could not clean up user_auth_sessions table")

		conn.Close()
	}

	userDetails := User{
		FirstName:    "FirstName1",
		LastName:     "LastName1",
		EmailAddress: "user1@test.com",
	}

	return userAuthSessionFixtures{
		dbManager: dbManager,
		user:      userDetails,
		cleanup:   cleanup,
	}
}

func TestUpsertUserAuthSession(t *testing.T) {
	t.Run("Should create new user auth session", func(t *testing.T) {
		// Assert.
		fixtures := setupUserAuthSessionTests(t)
		defer fixtures.cleanup()

		ctx := context.Background()

		user, err := fixtures.dbManager.CreateUser(ctx, fixtures.user)
		require.NoError(t, err)

		otpLastSentAt := time.Now().UTC()
		authSession := UserAuthSession{
			UserID:            user.ID,
			OTPCode:           strutil.StringToPointer("OTPCode1"),
			OTPCodeLastSentAt: &otpLastSentAt,
		}

		// Act.
		newAuthSession, err := fixtures.dbManager.UpsertUserAuthSession(ctx, authSession)
		require.NoError(t, err)
		require.NotEmpty(t, newAuthSession.ID)

		require.Equal(t, authSession.OTPCode, newAuthSession.OTPCode)
		require.Equal(t, db.ToUTC(*authSession.OTPCodeLastSentAt).String(), db.ToUTC(*newAuthSession.OTPCodeLastSentAt).String())
	})

	t.Run("Should update the existing user auth session", func(t *testing.T) {
		// Assert.
		fixtures := setupUserAuthSessionTests(t)
		defer fixtures.cleanup()

		ctx := context.Background()

		user, err := fixtures.dbManager.CreateUser(ctx, fixtures.user)
		require.NoError(t, err)

		otpLastSentAt := time.Now().UTC()
		authSession := UserAuthSession{
			UserID:            user.ID,
			OTPCode:           strutil.StringToPointer("OTPCode1"),
			OTPCodeLastSentAt: &otpLastSentAt,
		}

		// Act.
		initialAuthSession, err := fixtures.dbManager.UpsertUserAuthSession(ctx, authSession)

		// Assert.
		require.NoError(t, err)
		require.NotEmpty(t, initialAuthSession.ID)
		require.Equal(t, db.ToUTC(*authSession.OTPCodeLastSentAt).String(), db.ToUTC(*initialAuthSession.OTPCodeLastSentAt).String())

		// Act.
		authSession.OTPCode = strutil.StringToPointer("OTPCode2")
		otpLastSentAt = authSession.OTPCodeLastSentAt.Add(time.Hour)
		authSession.OTPCodeLastSentAt = &otpLastSentAt

		// Attempt to upsert an auth session for the same user but with different data.
		updatedAuthSession, err := fixtures.dbManager.UpsertUserAuthSession(ctx, authSession)

		// Assert.
		require.NoError(t, err)
		require.EqualValues(t, authSession.OTPCode, updatedAuthSession.OTPCode)
		require.Equal(t, db.ToUTC(*authSession.OTPCodeLastSentAt).String(), db.ToUTC(*updatedAuthSession.OTPCodeLastSentAt).String())
		require.NotEqualValues(t, initialAuthSession.OTPCode, updatedAuthSession.OTPCode)
		require.NotEqualValues(t, db.ToUTC(*initialAuthSession.OTPCodeLastSentAt).String(), db.ToUTC(*updatedAuthSession.OTPCodeLastSentAt).String())
	})
}

func TestGetUserAuthSessionByUserID(t *testing.T) {
	t.Run("Should return ErrNotFound error if session is not found", func(t *testing.T) {
		// Assert.
		fixtures := setupUserAuthSessionTests(t)
		defer fixtures.cleanup()

		ctx := context.Background()

		// Act.
		userUUID, err := uuid.NewRandom()
		require.NoError(t, err)

		authSession, err := fixtures.dbManager.GetUserAuthSessionByUserID(ctx, userUUID)

		// Assert.
		require.Equal(t, ErrNotFound, err)
		require.Empty(t, authSession)
	})

	t.Run("Should return ErrNotFound error if user is removed", func(t *testing.T) {
		// Assert.
		fixtures := setupUserAuthSessionTests(t)
		defer fixtures.cleanup()

		ctx := context.Background()

		user, err := fixtures.dbManager.CreateUser(ctx, fixtures.user)
		require.NoError(t, err)

		otpLastSentAt := time.Now().UTC()
		authSession := UserAuthSession{
			UserID:            user.ID,
			OTPCode:           strutil.StringToPointer("OTPCode1"),
			OTPCodeLastSentAt: &otpLastSentAt,
		}
		_, err = fixtures.dbManager.UpsertUserAuthSession(ctx, authSession)
		require.NoError(t, err)

		err = fixtures.dbManager.RemoveUser(ctx, user.ID)
		require.NoError(t, err)

		// Act.
		userAuthSession, err := fixtures.dbManager.GetUserAuthSessionByUserID(ctx, authSession.UserID)

		// Assert.
		require.Equal(t, ErrNotFound, err)
		require.Empty(t, userAuthSession)
	})

	t.Run("Should return user auth session", func(t *testing.T) {
		// Assert.
		fixtures := setupUserAuthSessionTests(t)
		defer fixtures.cleanup()

		ctx := context.Background()

		user, err := fixtures.dbManager.CreateUser(ctx, fixtures.user)
		require.NoError(t, err)

		secondUser, err := fixtures.dbManager.CreateUser(ctx, User{
			FirstName:    "FirstName2",
			LastName:     "LastName2",
			EmailAddress: "user2@test.com",
		})
		require.NoError(t, err)

		otpLastSentAt := time.Now().UTC()
		authSession := UserAuthSession{
			UserID:            user.ID,
			OTPCode:           strutil.StringToPointer("OTPCode1"),
			OTPCodeLastSentAt: &otpLastSentAt,
		}
		newAuthSession, err := fixtures.dbManager.UpsertUserAuthSession(ctx, authSession)
		require.NoError(t, err)

		secondUserAuthSession := *newAuthSession
		secondUserAuthSession.UserID = secondUser.ID
		secondUserAuthSession.OTPCode = strutil.StringToPointer("OTPCode2")
		_, err = fixtures.dbManager.UpsertUserAuthSession(ctx, secondUserAuthSession)
		require.NoError(t, err)

		// Act.
		userAuthSession, err := fixtures.dbManager.GetUserAuthSessionByUserID(ctx, authSession.UserID)

		// Assert.
		require.NoError(t, err)
		require.EqualValues(t, newAuthSession.ID, userAuthSession.ID)
		require.EqualValues(t, newAuthSession.OTPCode, userAuthSession.OTPCode)
		require.EqualValues(t, newAuthSession.UserID, userAuthSession.UserID)
		require.Equal(t, db.ToUTC(*newAuthSession.OTPCodeLastSentAt).String(), db.ToUTC(*userAuthSession.OTPCodeLastSentAt).String())
	})
}

func userAuthSessionsTestTruncate(ctx context.Context, conn *pgxpool.Pool) error {
	_, err := conn.Exec(ctx, `
			truncate user_auth_sessions cascade;
		`)
	return err
}
