package storage

import (
	"context"
	"strings"
	"time"

	sq "github.com/Masterminds/squirrel"
	"github.com/google/uuid"
	"github.com/jackc/pgx/v4"
	"github.com/pkg/errors"

	"gitlab.mooncascade.net/roman/go-starter-kit/internal/db"
)

type UserRole string

const (
	UserRoleTypeAdmin     UserRole = "admin"
	UserRoleTypeModerator UserRole = "moderator"
	UserRoleTypeGuest     UserRole = "guest"
)

var (
	ValidUserRoles []UserRole = []UserRole{
		UserRoleTypeAdmin,
		UserRoleTypeModerator,
		UserRoleTypeGuest,
	}
)

type User struct {
	ID           uuid.UUID `db:"id"`
	FirstName    string    `db:"first_name"`
	LastName     string    `db:"last_name"`
	EmailAddress string    `db:"email_address"`
	Role         string    `db:"role"`
	CreatedAt    time.Time `db:"created_at"`
	ModifiedAt   time.Time `db:"modified_at"`
}

// UserUpdate are the fields in the users table that can be editted by a
// user. The ID is required to find the user, and cannot be edited.
type UserUpdate struct {
	ID        uuid.UUID `db:"id"`
	FirstName *string   `db:"first_name"`
	LastName  *string   `db:"last_name"`
	Role      *string   `db:"role"`
}

type UsersQuery struct {
	IDs    []uuid.UUID
	Emails []string
	Limit  uint64
	Offset uint64
	Sort   SortOptions
}

func (d *dbManager) CreateUser(ctx context.Context, user User) (*User, error) {
	createdAt := db.ToUTC(time.Now())
	modifiedAt := createdAt

	// Ensure email is lowercased before insert
	email := strings.ToLower(user.EmailAddress)
	// If no role is provided, default to `guest`
	if user.Role == "" {
		user.Role = string(UserRoleTypeGuest)
	}

	insertQuery, args, err := d.StatementBuilder.
		Insert("users").
		Columns(
			"first_name", "last_name", "email_address",
			"role",
			"created_at", "modified_at",
		).
		Values(
			user.FirstName, user.LastName,
			email,
			user.Role,
			createdAt, modifiedAt,
		).
		Suffix("ON CONFLICT (email_address) DO NOTHING").
		Suffix(`
			RETURNING id, first_name, last_name, email_address,
			role,
			created_at, modified_at
		`).
		ToSql()
	if err != nil {
		return nil, errors.Wrap(err, "constructing user insert query")
	}

	var newUser User
	err = d.Conn.QueryRow(ctx, insertQuery, args...).
		Scan(
			&newUser.ID,
			&newUser.FirstName, &newUser.LastName,
			&newUser.EmailAddress, &newUser.Role,
			&newUser.CreatedAt, &newUser.ModifiedAt,
		)
	if err != nil {
		if err == pgx.ErrNoRows {
			return nil, ErrAlreadyExists
		}
		return nil, errors.Wrap(err, "creating user")
	}
	return &newUser, nil
}

// GetUserByUUID retrieves an active user given their ID.
func (d *dbManager) GetUserByUUID(ctx context.Context,
	userUUID uuid.UUID) (*User, error) {
	selectQuery, args, err := d.StatementBuilder.
		Select(`
			id,
			first_name, last_name, email_address,
			role,
			created_at, modified_at
		`).
		From("users").
		Where(sq.Eq{"id": userUUID}).
		ToSql()
	if err != nil {
		return nil, errors.Wrap(err, "creating user select query")
	}

	var user User
	err = d.Conn.QueryRow(ctx, selectQuery, args...).
		Scan(
			&user.ID,
			&user.FirstName, &user.LastName, &user.EmailAddress,
			&user.Role,
			&user.CreatedAt, &user.ModifiedAt,
		)
	if err != nil {
		if err == pgx.ErrNoRows {
			return nil, ErrNotFound
		}
		return nil, errors.Wrapf(err, "finding user")
	}
	return &user, nil
}

// GetUserByEmail retrieves an active user by their email, after lower-casing
// it to search.
func (d *dbManager) GetUserByEmail(ctx context.Context,
	emailAddress string) (*User, error) {
	selectQuery, args, err := d.StatementBuilder.
		Select(`
			id, first_name, last_name, email_address,
			role,
			created_at, modified_at
		`).
		From("users").
		Where(sq.Eq{"email_address": strings.ToLower(emailAddress)}).
		ToSql()
	if err != nil {
		return nil, errors.Wrap(err, "creating user select query")
	}

	var user User
	err = d.Conn.QueryRow(ctx, selectQuery, args...).
		Scan(
			&user.ID,
			&user.FirstName, &user.LastName, &user.EmailAddress,
			&user.Role,
			&user.CreatedAt, &user.ModifiedAt,
		)
	if err != nil {
		if err == pgx.ErrNoRows {
			return nil, ErrNotFound
		}
		return nil, errors.Wrapf(err, "finding user")
	}
	return &user, nil
}

func (d *dbManager) RemoveUser(ctx context.Context, userUUID uuid.UUID) error {
	tx, err := d.Conn.Begin(ctx)
	if err != nil {
		return errors.Wrap(err, "opening transaction")
	}
	defer tx.Rollback(ctx) // nolint: errcheck

	err = d.RemoveUserAuthSessionByUserID(ctx, tx, userUUID)
	if err != nil && err != ErrNotFound {
		return errors.Wrap(err, "removing user auth session")
	}

	removeQuery, args, err := d.StatementBuilder.
		Delete("users").
		Where("id = ?", userUUID).
		ToSql()
	if err != nil {
		return errors.Wrap(err, "constructing remove statement")
	}

	commandTag, err := tx.Exec(ctx, removeQuery, args...)
	if err != nil {
		return errors.Wrap(err, "removing user")
	}
	if commandTag.RowsAffected() == 0 {
		return ErrNotFound
	}

	if err := tx.Commit(ctx); err != nil {
		return errors.Wrap(err, "committing tx")
	}

	return nil
}

// EditUser edits information about a user.  The allowable fields that can be
// changed are passed in through the UserUpdate struct.  Any nil value in the
// UserUpdate remains unchanged.
func (d *dbManager) EditUser(ctx context.Context,
	userUpdate UserUpdate) (*User, error) {
	// Construct a map of fields to update
	updates := make(map[string]string)
	if userUpdate.FirstName != nil {
		updates["first_name"] = *userUpdate.FirstName
	}
	if userUpdate.LastName != nil {
		updates["last_name"] = *userUpdate.LastName
	}
	if userUpdate.Role != nil {
		updates["role"] = *userUpdate.Role
	}

	modifiedAt := db.ToUTC(time.Now())
	updateQuery := d.StatementBuilder.
		Update("users").
		Set("modified_at", modifiedAt).
		Where("id = ?", userUpdate.ID).
		Suffix(`
			RETURNING id, first_name, last_name, email_address,
			role,
			created_at, modified_at
		`)

	for key, value := range updates {
		updateQuery = updateQuery.Set(key, value)
	}

	editQuery, args, err := updateQuery.ToSql()
	if err != nil {
		return nil, errors.Wrap(err, "constructing update query")
	}

	var user User
	err = d.Conn.
		QueryRow(ctx, editQuery, args...).
		Scan(
			&user.ID,
			&user.FirstName, &user.LastName, &user.EmailAddress,
			&user.Role,
			&user.CreatedAt, &user.ModifiedAt,
		)
	if err != nil {
		if err == pgx.ErrNoRows {
			return nil, ErrNotFound
		}
		return nil, errors.Wrap(err, "editing user")
	}
	return &user, nil
}

// FindUsers returns users that match the provided user query.  The query constructs
// `AND` between the where clauses, meaning the user must meet all parts of a query.
func (d *dbManager) FindUsers(ctx context.Context, query UsersQuery) ([]User, error) {
	// Create the base select query
	selectQuery := d.StatementBuilder.
		Select(`
			id,
			first_name, last_name, email_address,
			role,
			created_at, modified_at
		`).
		From("users")

	// Add specific where clauses based on provided user query
	if query.Emails != nil {
		var emails []string
		for _, email := range query.Emails {
			emails = append(emails, strings.ToLower(email))
		}
		selectQuery = selectQuery.Where(sq.Eq{"email_address": emails})
	}

	if query.IDs != nil {
		selectQuery = selectQuery.Where(sq.Eq{"id": query.IDs})
	}

	if query.Limit > 0 {
		selectQuery = selectQuery.Limit(query.Limit)
	}

	if query.Offset > 0 {
		selectQuery = selectQuery.Offset(query.Offset)
	}

	if query.Sort != nil {
		var u User
		selectQuery = generateOrderBy(selectQuery, query.Sort, u)
	}

	findQuery, args, err := selectQuery.ToSql()
	if err != nil {
		return nil, errors.Wrap(err, "constructing find query")
	}

	rows, err := d.Conn.Query(ctx, findQuery, args...)
	if err != nil {
		return nil, errors.Wrap(err, "finding users")
	}

	var users []User
	for rows.Next() {
		var u User
		err = rows.Scan(
			&u.ID,
			&u.FirstName, &u.LastName, &u.EmailAddress,
			&u.Role,
			&u.CreatedAt, &u.ModifiedAt,
		)
		if err != nil {
			return nil, errors.Wrapf(err, "constructing users")
		}
		users = append(users, u)
	}

	if len(users) == 0 {
		return nil, nil
	}

	return users, err
}
