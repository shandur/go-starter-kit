package storage

import (
	"context"
	"fmt"

	sq "github.com/Masterminds/squirrel"
	"github.com/google/uuid"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
)

// DBManager interface represents available actions on the all tables.
//
// All methods accept an optional transaction.  If no transaction is provided,
// one will be created and committed at completion of the method.  Otherwise,
// the caller is responsible for committing after all operations are completed.
type DBManager interface {
	// User methods found in user.go
	CreateUser(ctx context.Context, user User) (*User, error)
	GetUserByUUID(ctx context.Context, userUUID uuid.UUID) (*User, error)
	GetUserByEmail(ctx context.Context, email string) (*User, error)
	RemoveUser(ctx context.Context, userUUID uuid.UUID) error
	EditUser(ctx context.Context, userUpdate UserUpdate) (*User, error)
	FindUsers(ctx context.Context, query UsersQuery) ([]User, error)

	// User auth sessions methods found in user_auth_session.go
	UpsertUserAuthSession(ctx context.Context, authSession UserAuthSession) (*UserAuthSession, error)
	GetUserAuthSessionByUserID(ctx context.Context, userUUID uuid.UUID) (*UserAuthSession, error)
	RemoveUserAuthSessionByUserID(ctx context.Context, tx pgx.Tx, userUUID uuid.UUID) error
}

// DBConfig maintains dependencies for a db manager.
type DBConfig struct {
	Conn             *pgxpool.Pool
	StatementBuilder *sq.StatementBuilderType
}

// dbManager provides convenience methods to interact with the Postgres tables.
type dbManager struct {
	DBConfig
}

// NewDBManager creates a new instance of DBManager, which provides
// convenience methods to interact with the Postgres tables.
func NewDBManager(config DBConfig) (DBManager, error) {
	if config.Conn == nil {
		return nil, fmt.Errorf("missing postgres connection pool")
	}
	if config.StatementBuilder == nil {
		return nil, fmt.Errorf("missing squirrel statement builder")
	}
	return &dbManager{config}, nil
}

var _ DBManager = &dbManager{}
