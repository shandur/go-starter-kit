package storage

import sq "github.com/Masterminds/squirrel"

// NewStatementBuilder creates a new Squirrel statement builder for queries.
// `sq.Dollar` is used as a placeholder type.
func NewStatementBuilder() *sq.StatementBuilderType {
	statementBuilder := sq.StatementBuilder.PlaceholderFormat(sq.Dollar)

	return &statementBuilder
}
