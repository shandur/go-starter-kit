package auth

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/require"

	"gitlab.mooncascade.net/roman/go-starter-kit/internal/authorization"
	"gitlab.mooncascade.net/roman/go-starter-kit/internal/testutil"
)

func TestLogoutHandlerNoCookieSuccess(t *testing.T) {
	// Arrange.
	fixtures := setupAuthTests(t)
	defer fixtures.cleanup()

	// Act.
	server := httptest.NewServer(fixtures.router)
	status, response := testutil.MakeRequest(t, server, http.MethodPost, "/v1/auth/logout", nil)

	// Assert.
	require.Nil(t, response.Data)
	require.Equal(t, http.StatusNoContent, status)
}

func TestLogoutHandlerSuccess(t *testing.T) {
	fixtures := setupAuthTests(t)
	defer fixtures.cleanup()

	// Arrange.
	authCookie := &http.Cookie{
		Name:  authorization.AuthCookieUserKey,
		Value: "AccessToken1",
	}

	// Act.
	server := httptest.NewServer(fixtures.router)
	req, err := http.NewRequest(http.MethodPost, fmt.Sprintf("%s%s", server.URL, "/v1/auth/logout"), nil)
	require.NoError(t, err)

	req.AddCookie(authCookie)

	status, response, responseRaw := testutil.MakeRequestRaw(t, server, req)
	defer responseRaw.Body.Close()

	// Assert.
	require.Nil(t, response.Data)
	require.Nil(t, response.Errors)
	require.Equal(t, http.StatusNoContent, status)

	// Assert cookie.
	cookie := responseRaw.Header.Get("Set-Cookie")

	require.NotEmpty(t, cookie)
	require.Contains(t, cookie, fmt.Sprintf("%s=;", authCookie.Name)) // empty token
	require.Contains(t, cookie, "Max-Age=0")                          // should expire immediately
}
