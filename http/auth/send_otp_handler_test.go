package auth

import (
	"context"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	"github.com/stretchr/testify/require"

	"gitlab.mooncascade.net/roman/go-starter-kit/internal/handler"
	"gitlab.mooncascade.net/roman/go-starter-kit/internal/strutil"
	"gitlab.mooncascade.net/roman/go-starter-kit/internal/testutil"
	"gitlab.mooncascade.net/roman/go-starter-kit/storage"
)

func SendOTPHandlerInvalidJSONErrors(t *testing.T) {
	// Arrange.
	fixtures := setupAuthTests(t)
	defer fixtures.cleanup()

	// Act.
	invalidBody := `{`

	server := httptest.NewServer(fixtures.router)
	status, response := testutil.MakeRequest(t, server, http.MethodPost, "/v1/auth/otp", &invalidBody)

	// Assert.
	expected := handler.Response{
		Errors: []handler.ResponseError{
			{
				Title: "cannot decode JSON payload",
				Code:  handler.ErrClientCode,
			},
		},
	}

	require.Equal(t, http.StatusBadRequest, status)
	require.EqualValues(t, expected, response)
}

func SendOTPHandlerRequestValidationErrors(t *testing.T) {
	// Arrange.
	fixtures := setupAuthTests(t)
	defer fixtures.cleanup()

	input := SendOTPSchema{}

	// Act.
	body := testutil.EncodeRequest(t, input)

	server := httptest.NewServer(fixtures.router)
	status, response := testutil.MakeRequest(t, server, http.MethodPost, "/v1/auth/otp", &body)

	// Assert.
	expected := handler.Response{
		Errors: []handler.ResponseError{
			{
				Code: "required",
				Source: handler.ResponseErrorSource{
					Pointer: handler.ConvertToErrorSourcePointer("email"),
				},
			},
		},
	}

	require.Equal(t, http.StatusBadRequest, status)
	require.EqualValues(t, expected, response)
}

func SendOTPHandlerUserNotFoundError(t *testing.T) {
	fixtures := setupAuthTests(t)
	defer fixtures.cleanup()

	// Arrange.
	input := SendOTPSchema{
		Email: "user1@test.com",
	}

	// Act.
	body := testutil.EncodeRequest(t, input)

	server := httptest.NewServer(fixtures.router)
	status, response := testutil.MakeRequest(t, server, http.MethodPost, "/v1/auth/otp", &body)

	// Assert.
	expected := handler.Response{
		Errors: []handler.ResponseError{
			{
				Title: "user not found",
				Code:  handler.ErrNotFound,
			},
		},
	}

	require.EqualValues(t, expected, response)
	require.Equal(t, http.StatusNotFound, status)
}

func SendOTPHandlerOTPIssuedTooEarlyError(t *testing.T) {
	fixtures := setupAuthTests(t)
	defer fixtures.cleanup()

	// Arrange.
	user, err := fixtures.dbManager.CreateUser(context.Background(), storage.User{
		FirstName:    "FirstName1",
		LastName:     "LastName1",
		EmailAddress: "user1@test.com",
		Role:         string(storage.UserRoleTypeModerator),
	})
	require.NoError(t, err)

	// Create auth session details.
	otpIssueTime := time.Now().UTC().Add(OTPIssuanceTimeoutDuration)
	_, err = fixtures.dbManager.UpsertUserAuthSession(context.Background(), storage.UserAuthSession{
		UserID:            user.ID,
		OTPCodeLastSentAt: &otpIssueTime,
	})
	require.NoError(t, err)

	input := SendOTPSchema{
		Email: user.EmailAddress,
	}

	// Act.
	body := testutil.EncodeRequest(t, input)

	server := httptest.NewServer(fixtures.router)
	status, response := testutil.MakeRequest(t, server, http.MethodPost, "/v1/auth/otp", &body)

	// Assert.
	expected := handler.Response{
		Errors: []handler.ResponseError{
			{
				Title: "OTP has been issued too early",
				Code:  handler.ErrClientCode,
			},
		},
	}

	require.EqualValues(t, expected, response)
	require.Equal(t, http.StatusTooManyRequests, status)
}

func SendOTPHandlerSuccess(t *testing.T) {
	fixtures := setupAuthTests(t)
	defer fixtures.cleanup()

	// Arrange.
	user, err := fixtures.dbManager.CreateUser(context.Background(), storage.User{
		FirstName:    "FirstName1",
		LastName:     "LastName1",
		EmailAddress: "test1@starter.com",
		Role:         string(storage.UserRoleTypeModerator),
	})
	require.NoError(t, err)

	input := SendOTPSchema{
		Email: user.EmailAddress,
	}

	// Act.
	body := testutil.EncodeRequest(t, input)

	server := httptest.NewServer(fixtures.router)
	status, response := testutil.MakeRequest(t, server, http.MethodPost, "/v1/auth/otp", &body)

	// Assert.
	require.Equal(t, http.StatusNoContent, status)
	require.Empty(t, response.Errors)

	_, ok := response.Data.(map[string]interface{})
	require.False(t, ok)

	// Assert auth session has been created.
	authSession, err := fixtures.dbManager.GetUserAuthSessionByUserID(context.Background(), user.ID)
	require.NoError(t, err)
	require.NotEmpty(t, authSession)
	require.NotEmpty(t, authSession.OTPCodeLastSentAt)
}

func SendOTPHandlerPassOTPThresholdTimeSuccess(t *testing.T) {
	fixtures := setupAuthTests(t)
	defer fixtures.cleanup()

	// Arrange.
	user, err := fixtures.dbManager.CreateUser(context.Background(), storage.User{
		FirstName:    "FirstName1",
		LastName:     "LastName1",
		EmailAddress: "test1@starter.com",
		Role:         string(storage.UserRoleTypeModerator),
	})
	require.NoError(t, err)

	// Create auth session details.
	otpIssueTime := time.Now().UTC().AddDate(0, 0, -1)
	_, err = fixtures.dbManager.UpsertUserAuthSession(context.Background(), storage.UserAuthSession{
		UserID:            user.ID,
		OTPCode:           strutil.StringToPointer("OTPCode1"),
		OTPCodeLastSentAt: &otpIssueTime,
	})
	require.NoError(t, err)

	input := SendOTPSchema{
		Email: user.EmailAddress,
	}

	// Act.
	body := testutil.EncodeRequest(t, input)

	server := httptest.NewServer(fixtures.router)
	status, response := testutil.MakeRequest(t, server, http.MethodPost, "/v1/auth/otp", &body)

	// Assert.
	require.Equal(t, http.StatusNoContent, status)
	require.Empty(t, response.Errors)

	// Assert empty body.
	_, ok := response.Data.(map[string]interface{})
	require.False(t, ok)

	// Assert auth session has been created.
	authSession, err := fixtures.dbManager.GetUserAuthSessionByUserID(context.Background(), user.ID)
	require.NoError(t, err)
	require.NotEmpty(t, authSession)
	require.NotEmpty(t, authSession.OTPCodeLastSentAt)
}
