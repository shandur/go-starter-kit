package auth

import (
	"net/http"
	"time"

	"github.com/go-playground/validator/v10"
	"github.com/pkg/errors"
	"go.uber.org/zap"

	"gitlab.mooncascade.net/roman/go-starter-kit/config"
	"gitlab.mooncascade.net/roman/go-starter-kit/internal/handler"
	"gitlab.mooncascade.net/roman/go-starter-kit/internal/logging"
	"gitlab.mooncascade.net/roman/go-starter-kit/internal/otp"
	"gitlab.mooncascade.net/roman/go-starter-kit/storage"
)

const (
	// OTPIssuanceTimeoutDuration defines the timeout when the user cannot issue a new OTP again.
	OTPIssuanceTimeoutDuration = 15 * time.Second
)

func NewSendOTPHandler(v *validator.Validate, dbManager storage.DBManager, generateOTP otp.GenerateOTPFunc, configs config.Config) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var request SendOTPSchema

		err := handler.BindRequestJSON(w, r, &request)
		if err != nil {
			handler.WriteErrorResponse(r.Context(), w, http.StatusBadRequest, errors.New("cannot decode JSON payload"), handler.ErrClientCode)
			return
		}

		validationErrors := handler.ValidateRequest(v, request)
		if len(validationErrors) > 0 {
			handler.WriteValidationErrorResponse(r.Context(), w, validationErrors)
			return
		}

		// 1. Check if user exists in our database.
		user, err := dbManager.GetUserByEmail(r.Context(), request.Email)
		if err != nil {
			if err == storage.ErrNotFound {
				handler.WriteErrorResponse(r.Context(), w, http.StatusNotFound, errors.New("user not found"), handler.ErrNotFound)
				return
			}

			handler.WriteErrorResponse(r.Context(), w, http.StatusInternalServerError, errors.Wrap(err, "cannot get user by email"), handler.ErrInternalCode)
			return
		}

		// 2a. Check if OTP has been already issued.
		userAuthSession, err := dbManager.GetUserAuthSessionByUserID(r.Context(), user.ID)
		if err != nil {
			// If it's been issued for the 1st time.
			if err != storage.ErrNotFound {
				handler.WriteErrorResponse(r.Context(), w, http.StatusInternalServerError, errors.Wrap(err, "cannot get user by email"), handler.ErrInternalCode)
				return
			}
		}

		// 2b. Check if OTP has been issued too early.
		if userAuthSession != nil && userAuthSession.OTPCodeLastSentAt != nil {
			// Calculate the time threshold.
			timeThresholdOTP := userAuthSession.OTPCodeLastSentAt.Add(OTPIssuanceTimeoutDuration)
			if time.Now().UTC().Before(timeThresholdOTP) {
				handler.WriteErrorResponse(r.Context(), w, http.StatusTooManyRequests, errors.New("OTP has been issued too early"), handler.ErrClientCode)
				return
			}
		}

		// 3. Generate OTP code to start Passwordless flow.
		otpCode, err := generateOTP()
		if err != nil {
			handler.WriteErrorResponse(r.Context(), w, http.StatusInternalServerError, errors.Wrap(err, "failed to generate OTP"), handler.ErrOTPCode)
			return
		}

		// 4. Upsert the user auth session details with OTP issued timestamp.
		otpIssueTime := time.Now().UTC()
		_, err = dbManager.UpsertUserAuthSession(r.Context(), storage.UserAuthSession{
			UserID:            user.ID,
			OTPCodeLastSentAt: &otpIssueTime,
			OTPCode:           &otpCode,
		})

		// Don't return an error since OTP has already been issued.
		if err != nil {
			handler.WriteErrorResponse(r.Context(), w, http.StatusInternalServerError, errors.Wrap(err, "failed to store auth session"), handler.ErrInternalCode)
			return
		}

		// Note: We can send OTP by email/sms at this point.

		if configs.Observability.EnableDebugLogs {
			logging.GetLogger(r.Context()).Debug("one time password", zap.String("otp_code", otpCode))
		}

		handler.WriteSuccessResponseJSON(r.Context(), w, http.StatusNoContent, nil)
	}
}
