package auth

import (
	"context"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	"github.com/stretchr/testify/require"

	"gitlab.mooncascade.net/roman/go-starter-kit/internal/handler"
	"gitlab.mooncascade.net/roman/go-starter-kit/internal/strutil"
	"gitlab.mooncascade.net/roman/go-starter-kit/internal/testutil"
	"gitlab.mooncascade.net/roman/go-starter-kit/storage"
)

func TestLoginHandlerInvalidJSONErrors(t *testing.T) {
	// Arrange.
	fixtures := setupAuthTests(t)
	defer fixtures.cleanup()

	// Act.
	invalidBody := `{`

	server := httptest.NewServer(fixtures.router)
	status, response := testutil.MakeRequest(t, server, http.MethodPost, "/v1/auth/login", &invalidBody)

	// Assert.
	expected := handler.Response{
		Errors: []handler.ResponseError{
			{
				Title: "cannot decode JSON payload",
				Code:  handler.ErrClientCode,
			},
		},
	}

	require.Equal(t, http.StatusBadRequest, status)
	require.EqualValues(t, expected, response)
}

func TestLoginHandlerRequestValidationErrors(t *testing.T) {
	// Arrange.
	fixtures := setupAuthTests(t)
	defer fixtures.cleanup()

	tests := []struct {
		name     string
		input    LoginSchema
		expected handler.Response
	}{
		{
			name:  "1. With empty request",
			input: LoginSchema{},
			expected: handler.Response{
				Errors: []handler.ResponseError{
					{
						Code: "required",
						Source: handler.ResponseErrorSource{
							Pointer: handler.ConvertToErrorSourcePointer("email"),
						},
					},
					{
						Code: "required",
						Source: handler.ResponseErrorSource{
							Pointer: handler.ConvertToErrorSourcePointer("code"),
						},
					},
				},
			},
		},
		{
			name: "2. With invalid email",
			input: LoginSchema{
				Email: "user@",
				Code:  "123456",
			},
			expected: handler.Response{
				Errors: []handler.ResponseError{
					{
						Code: "email",
						Source: handler.ResponseErrorSource{
							Pointer: handler.ConvertToErrorSourcePointer("email"),
						},
					},
				},
			},
		},
		{
			name: "3. With invalid code - numeric",
			input: LoginSchema{
				Email: "user1@test.com",
				Code:  "abcabc",
			},
			expected: handler.Response{
				Errors: []handler.ResponseError{
					{
						Code: "numeric",
						Source: handler.ResponseErrorSource{
							Pointer: handler.ConvertToErrorSourcePointer("code"),
						},
					},
				},
			},
		},
		{
			name: "4. With invalid code - long length",
			input: LoginSchema{
				Email: "user1@test.com",
				Code:  "123456789",
			},
			expected: handler.Response{
				Errors: []handler.ResponseError{
					{
						Code: "len",
						Source: handler.ResponseErrorSource{
							Pointer: handler.ConvertToErrorSourcePointer("code"),
						},
					},
				},
			},
		},
		{
			name: "5. With invalid code - short length",
			input: LoginSchema{
				Email: "user1@test.com",
				Code:  "123",
			},
			expected: handler.Response{
				Errors: []handler.ResponseError{
					{
						Code: "len",
						Source: handler.ResponseErrorSource{
							Pointer: handler.ConvertToErrorSourcePointer("code"),
						},
					},
				},
			},
		},
	}

	// Act.

	for _, test := range tests {
		test := test
		t.Run(test.name, func(t *testing.T) {
			t.Parallel()

			// Act.
			body := testutil.EncodeRequest(t, test.input)

			server := httptest.NewServer(fixtures.router)
			status, response := testutil.MakeRequest(t, server, http.MethodPost, "/v1/auth/login", &body)

			// Assert.
			require.EqualValues(t, test.expected, response)
			require.Equal(t, http.StatusBadRequest, status)
		})
	}
}

func TestLoginHandlerUserNotFoundError(t *testing.T) {
	fixtures := setupAuthTests(t)
	defer fixtures.cleanup()

	// Arrange.
	input := LoginSchema{
		Email: "user1@test.com",
		Code:  "123456",
	}

	// Act.
	body := testutil.EncodeRequest(t, input)

	server := httptest.NewServer(fixtures.router)
	status, response := testutil.MakeRequest(t, server, http.MethodPost, "/v1/auth/login", &body)

	// Assert.
	expected := handler.Response{
		Errors: []handler.ResponseError{
			{
				Title: "user not authorised",
				Code:  handler.ErrNotAuthenticatedCode,
			},
		},
	}

	require.EqualValues(t, expected, response)
	require.Equal(t, http.StatusUnauthorized, status)
}

func TestLoginHandlerRemovedUserAttemptsToLoginError(t *testing.T) {
	fixtures := setupAuthTests(t)
	defer fixtures.cleanup()

	// Arrange.
	user, err := fixtures.dbManager.CreateUser(context.Background(), storage.User{
		FirstName:    "FirstName1",
		LastName:     "LastName1",
		EmailAddress: "user1@test.com",
		Role:         string(storage.UserRoleTypeModerator),
	})
	require.NoError(t, err)

	// Remove the user.
	err = fixtures.dbManager.RemoveUser(context.Background(), user.ID)
	require.NoError(t, err)

	input := LoginSchema{
		Email: user.EmailAddress,
		Code:  "123456",
	}

	// Act.
	body := testutil.EncodeRequest(t, input)

	server := httptest.NewServer(fixtures.router)
	status, response := testutil.MakeRequest(t, server, http.MethodPost, "/v1/auth/login", &body)

	// Assert.
	expected := handler.Response{
		Errors: []handler.ResponseError{
			{
				Title: "user not authorised",
				Code:  handler.ErrNotAuthenticatedCode,
			},
		},
	}

	require.EqualValues(t, expected, response)
	require.Equal(t, http.StatusUnauthorized, status)
}

func TestLoginHandlerUserAuthSessionNotFoundError(t *testing.T) {
	fixtures := setupAuthTests(t)
	defer fixtures.cleanup()

	// Arrange.
	user, err := fixtures.dbManager.CreateUser(context.Background(), storage.User{
		FirstName:    "FirstName1",
		LastName:     "LastName1",
		EmailAddress: "user1@test.com",
		Role:         string(storage.UserRoleTypeModerator),
	})
	require.NoError(t, err)

	input := LoginSchema{
		Email: user.EmailAddress,
		Code:  "123456",
	}

	// Act.
	body := testutil.EncodeRequest(t, input)

	server := httptest.NewServer(fixtures.router)
	status, response := testutil.MakeRequest(t, server, http.MethodPost, "/v1/auth/login", &body)

	// Assert.
	expected := handler.Response{
		Errors: []handler.ResponseError{
			{
				Title: "user not authorised",
				Code:  handler.ErrNotAuthenticatedCode,
			},
		},
	}

	require.EqualValues(t, expected, response)
	require.Equal(t, http.StatusUnauthorized, status)
}

func TestLoginHandlerUserAuthSessionOTPNotIssuedError(t *testing.T) {
	t.Run("no OTP last sent time", func(t *testing.T) {
		fixtures := setupAuthTests(t)
		defer fixtures.cleanup()

		// Arrange.
		user, err := fixtures.dbManager.CreateUser(context.Background(), storage.User{
			FirstName:    "FirstName1",
			LastName:     "LastName1",
			EmailAddress: "user1@test.com",
			Role:         string(storage.UserRoleTypeModerator),
		})
		require.NoError(t, err)

		// Create auth session with empty OTP issue time.
		_, err = fixtures.dbManager.UpsertUserAuthSession(context.Background(), storage.UserAuthSession{
			UserID:            user.ID,
			OTPCodeLastSentAt: nil, // no last sent time
		})
		require.NoError(t, err)

		input := LoginSchema{
			Email: user.EmailAddress,
			Code:  "123456",
		}

		// Act.
		body := testutil.EncodeRequest(t, input)

		server := httptest.NewServer(fixtures.router)
		status, response := testutil.MakeRequest(t, server, http.MethodPost, "/v1/auth/login", &body)

		// Assert.
		expected := handler.Response{
			Errors: []handler.ResponseError{
				{
					Title: "user OTP not issued",
					Code:  handler.ErrOTPCode,
				},
			},
		}

		require.EqualValues(t, expected, response)
		require.Equal(t, http.StatusUnauthorized, status)
	})
	t.Run("no OTP code", func(t *testing.T) {
		fixtures := setupAuthTests(t)
		defer fixtures.cleanup()

		// Arrange.
		user, err := fixtures.dbManager.CreateUser(context.Background(), storage.User{
			FirstName:    "FirstName1",
			LastName:     "LastName1",
			EmailAddress: "user1@test.com",
			Role:         string(storage.UserRoleTypeModerator),
		})
		require.NoError(t, err)

		// Create auth session with empty OTP issue time.
		otpLastSent := time.Now().UTC()
		_, err = fixtures.dbManager.UpsertUserAuthSession(context.Background(), storage.UserAuthSession{
			UserID:            user.ID,
			OTPCodeLastSentAt: &otpLastSent,
			OTPCode:           nil, // no code
		})
		require.NoError(t, err)

		input := LoginSchema{
			Email: user.EmailAddress,
			Code:  "123456",
		}

		// Act.
		body := testutil.EncodeRequest(t, input)

		server := httptest.NewServer(fixtures.router)
		status, response := testutil.MakeRequest(t, server, http.MethodPost, "/v1/auth/login", &body)

		// Assert.
		expected := handler.Response{
			Errors: []handler.ResponseError{
				{
					Title: "user OTP not issued",
					Code:  handler.ErrOTPCode,
				},
			},
		}

		require.EqualValues(t, expected, response)
		require.Equal(t, http.StatusUnauthorized, status)
	})
}

func TestLoginHandlerOTPHasExpiredError(t *testing.T) {
	fixtures := setupAuthTests(t)
	defer fixtures.cleanup()

	// Arrange.
	user, err := fixtures.dbManager.CreateUser(context.Background(), storage.User{
		FirstName:    "FirstName1",
		LastName:     "LastName1",
		EmailAddress: "user1@test.com",
		Role:         string(storage.UserRoleTypeModerator),
	})
	require.NoError(t, err)

	// Create auth session.
	otpIssueTime := time.Now().UTC().Add(2 * OTPExpiredTimeoutDuration) // Ensure that token is expired.
	_, err = fixtures.dbManager.UpsertUserAuthSession(context.Background(), storage.UserAuthSession{
		UserID:            user.ID,
		OTPCodeLastSentAt: &otpIssueTime,
	})
	require.NoError(t, err)

	input := LoginSchema{
		Email: user.EmailAddress,
		Code:  "123456",
	}

	// Act.
	body := testutil.EncodeRequest(t, input)

	server := httptest.NewServer(fixtures.router)
	status, response := testutil.MakeRequest(t, server, http.MethodPost, "/v1/auth/login", &body)

	// Assert.
	expected := handler.Response{
		Errors: []handler.ResponseError{
			{
				Title: "user OTP not issued",
				Code:  handler.ErrOTPCode,
			},
		},
	}

	require.EqualValues(t, expected, response)
	require.Equal(t, http.StatusUnauthorized, status)
}

func TestLoginHandlerOTPIsDifferentError(t *testing.T) {
	fixtures := setupAuthTests(t)
	defer fixtures.cleanup()

	// Arrange.
	user, err := fixtures.dbManager.CreateUser(context.Background(), storage.User{
		FirstName:    "FirstName1",
		LastName:     "LastName1",
		EmailAddress: "user1@test.com",
		Role:         string(storage.UserRoleTypeModerator),
	})
	require.NoError(t, err)

	// Create auth session.
	otpIssueTime := time.Now().UTC()
	_, err = fixtures.dbManager.UpsertUserAuthSession(context.Background(), storage.UserAuthSession{
		UserID:            user.ID,
		OTPCodeLastSentAt: &otpIssueTime,
		OTPCode:           strutil.StringToPointer("1111"),
	})
	require.NoError(t, err)

	input := LoginSchema{
		Email: user.EmailAddress,
		Code:  "123456", // different OTP
	}

	// Act.
	body := testutil.EncodeRequest(t, input)

	server := httptest.NewServer(fixtures.router)
	status, response := testutil.MakeRequest(t, server, http.MethodPost, "/v1/auth/login", &body)

	// Assert.
	expected := handler.Response{
		Errors: []handler.ResponseError{
			{
				Title: "user OTP is invalid",
				Code:  handler.ErrOTPCode,
			},
		},
	}

	require.EqualValues(t, expected, response)
	require.Equal(t, http.StatusUnauthorized, status)
}

func TestLoginHandler_Success(t *testing.T) {
	fixtures := setupAuthTests(t)
	defer fixtures.cleanup()

	// Arrange.
	user, err := fixtures.dbManager.CreateUser(context.Background(), storage.User{
		FirstName:    "FirstName1",
		LastName:     "LastName1",
		EmailAddress: "test1@starter.com",
		Role:         string(storage.UserRoleTypeModerator),
	})
	require.NoError(t, err)

	// Create auth session.
	otpIssueTime := time.Now().UTC()
	_, err = fixtures.dbManager.UpsertUserAuthSession(context.Background(), storage.UserAuthSession{
		UserID:            user.ID,
		OTPCodeLastSentAt: &otpIssueTime,
		OTPCode:           strutil.StringToPointer("123456"),
	})
	require.NoError(t, err)

	input := LoginSchema{
		Email: user.EmailAddress,
		Code:  "123456",
	}

	// Act.
	body := testutil.EncodeRequest(t, input)

	server := httptest.NewServer(fixtures.router)
	status, response := testutil.MakeRequest(t, server, http.MethodPost, "/v1/auth/login", &body)

	// Assert.
	require.Equal(t, http.StatusNoContent, status)
	require.Empty(t, response.Errors)
}
