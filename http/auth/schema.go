package auth

// SendOTPSchema is used to start Passwordless login flow for a user.
type SendOTPSchema struct {
	Email string `json:"email" validate:"required,email"`
}

// LoginSchema is used to issue an access token from Auth0.
type LoginSchema struct {
	Email string `json:"email" validate:"required,email"`
	Code  string `json:"code" validate:"required,numeric,len=6"`
}
