package auth

import (
	"context"
	"testing"

	"github.com/go-chi/chi/v5"
	"github.com/stretchr/testify/require"

	"gitlab.mooncascade.net/roman/go-starter-kit/config"
	"gitlab.mooncascade.net/roman/go-starter-kit/internal/db"
	"gitlab.mooncascade.net/roman/go-starter-kit/internal/testutil"
	"gitlab.mooncascade.net/roman/go-starter-kit/internal/validator"
	"gitlab.mooncascade.net/roman/go-starter-kit/storage"
)

type authTestFixtures struct {
	dbManager storage.DBManager
	router    chi.Router
	cleanup   func()
}

func setupAuthTests(t *testing.T) *authTestFixtures {
	t.Helper()

	// Read configs.
	ctx := context.Background()
	env, err := config.ReadEnvVars(ctx)
	require.NoError(t, err, "parsing environment variables")

	// Setup database.
	pgClient, err := db.GetPostgresClient(ctx, &env)
	require.NoError(t, err, "creating postgres client")

	sb := storage.NewStatementBuilder()
	conn := pgClient.Conn

	runMigration := true
	err = db.Migrate(&env, pgClient.DB.DB, runMigration)
	require.NoError(t, err, "running migration")

	// Setup DBManager.
	dbManager, err := storage.NewDBManager(storage.DBConfig{
		Conn:             conn,
		StatementBuilder: sb,
	})
	require.NoError(t, err, "creating user manager")

	// Setup services.
	v := validator.NewValidator()

	// Register routes.
	router := chi.NewRouter()
	router.Route("/v1/auth", GetRouter(v, dbManager, env))

	// Cleanup.
	cleanup := func() {
		err := testutil.TruncateUsers(ctx, conn)
		require.NoError(t, err, "Could not clean up users tables")

		err = testutil.TruncateUserAuthSessions(ctx, conn)
		require.NoError(t, err, "Could not clean up user auth sessions tables")

		conn.Close()
	}

	return &authTestFixtures{
		dbManager: dbManager,
		router:    router,
		cleanup:   cleanup,
	}
}
