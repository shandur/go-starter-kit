package auth

import (
	"net/http"

	"gitlab.mooncascade.net/roman/go-starter-kit/internal/authorization"
	"gitlab.mooncascade.net/roman/go-starter-kit/internal/handler"
	"gitlab.mooncascade.net/roman/go-starter-kit/storage"
)

func NewLogoutHandler(
	dbManager storage.DBManager,
	authCookie *authorization.AuthCookie,
) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		_, err := authCookie.GetCookie(r)
		if err != nil {
			// Don't return an error.
			handler.WriteSuccessResponseJSON(r.Context(), w, http.StatusNoContent, nil)
			return
		}

		// Flush up auth cookie.
		authCookie.FlushCookie(w)

		handler.WriteSuccessResponseJSON(r.Context(), w, http.StatusNoContent, nil)
	}
}
