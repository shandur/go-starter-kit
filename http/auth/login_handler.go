package auth

import (
	"net/http"
	"time"

	"github.com/go-playground/validator/v10"
	"github.com/pkg/errors"
	"go.uber.org/zap"

	"gitlab.mooncascade.net/roman/go-starter-kit/internal/authorization"
	"gitlab.mooncascade.net/roman/go-starter-kit/internal/handler"
	"gitlab.mooncascade.net/roman/go-starter-kit/internal/logging"
	"gitlab.mooncascade.net/roman/go-starter-kit/storage"
)

const (
	// OTPExpiredTimeoutDuration is a default timeout when OTP gets expired.
	OTPExpiredTimeoutDuration = 5 * time.Minute
)

func NewLoginHandler(v *validator.Validate, dbManager storage.DBManager, authCookie *authorization.AuthCookie) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var request LoginSchema

		err := handler.BindRequestJSON(w, r, &request)
		if err != nil {
			handler.WriteErrorResponse(r.Context(), w, http.StatusBadRequest, errors.New("cannot decode JSON payload"), handler.ErrClientCode)
			return
		}

		validationErrors := handler.ValidateRequest(v, request)
		if len(validationErrors) > 0 {
			handler.WriteValidationErrorResponse(r.Context(), w, validationErrors)
			return
		}

		logger := logging.GetLogger(r.Context())

		// 1. Check if user exists in our database.
		user, err := dbManager.GetUserByEmail(r.Context(), request.Email)
		if err != nil {
			if err == storage.ErrNotFound {
				logger.Warn("issue_access_token: unknown user attempts to login", zap.String("user_email", request.Email))

				handler.WriteErrorResponse(r.Context(), w, http.StatusUnauthorized, errors.New("user not authorised"), handler.ErrNotAuthenticatedCode)
				return
			}

			handler.WriteErrorResponse(r.Context(), w, http.StatusInternalServerError, errors.Wrap(err, "cannot get user by email"), handler.ErrInternalCode)
			return
		}

		// 2. We should check if the user has issued an OTP.
		authSession, err := dbManager.GetUserAuthSessionByUserID(r.Context(), user.ID)
		if err != nil {
			if err == storage.ErrNotFound {
				logger.Warn("issue_access_token: user OTP not issued - no auth session", zap.String("user_id", user.ID.String()))

				handler.WriteErrorResponse(r.Context(), w, http.StatusUnauthorized, errors.New("user not authorised"), handler.ErrNotAuthenticatedCode)
				return
			}

			handler.WriteErrorResponse(r.Context(), w, http.StatusInternalServerError, errors.Wrap(err, "cannot get user auth session"), handler.ErrInternalCode)
			return
		}

		// 3. Check if OTP is set.
		if authSession.OTPCodeLastSentAt == nil || authSession.OTPCode == nil {
			logger.Warn("issue_access_token: user OTP not issued", zap.String("user_id", user.ID.String()), zap.Any("authSession", authSession))

			handler.WriteErrorResponse(r.Context(), w, http.StatusUnauthorized, errors.New("user OTP not issued"), handler.ErrOTPCode)
			return
		}

		// 4. Check if OTP has been issued no later than 5 minutes ago.
		if authSession.OTPCodeLastSentAt.After(time.Now().UTC().Add(OTPExpiredTimeoutDuration)) {
			handler.WriteErrorResponse(r.Context(), w, http.StatusUnauthorized, errors.New("user OTP is expired"), handler.ErrOTPCode)
			return
		}
		// 5. Check if OTPs are same.
		if *authSession.OTPCode != request.Code {
			handler.WriteErrorResponse(r.Context(), w, http.StatusUnauthorized, errors.New("user OTP is invalid"), handler.ErrOTPCode)
			return
		}

		// 6. Set auth cookie.
		authCookie.SetCookie(w, user.ID.String(), authorization.AuthCookieDefaultMaxAgeSec)

		handler.WriteSuccessResponseJSON(r.Context(), w, http.StatusNoContent, nil)
	}
}
