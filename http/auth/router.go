package auth

import (
	"github.com/go-chi/chi/v5"
	"github.com/go-playground/validator/v10"

	"gitlab.mooncascade.net/roman/go-starter-kit/config"
	"gitlab.mooncascade.net/roman/go-starter-kit/internal/authorization"
	"gitlab.mooncascade.net/roman/go-starter-kit/internal/otp"
	"gitlab.mooncascade.net/roman/go-starter-kit/internal/testutil/mocks"
	"gitlab.mooncascade.net/roman/go-starter-kit/storage"
)

func GetRouter(
	v *validator.Validate,
	dbManager storage.DBManager,
	configs config.Config,
) func(r chi.Router) {
	// Create auth cookie service.
	authCookie := authorization.NewAuthCookie(configs.Service.APIDomain, !configs.IsLocalEnvironment())

	otpGenerator := otp.GenerateOTP
	// Mock OTP generator for local dev.
	if configs.IsLocalEnvironment() && configs.Authentication.MockOTPCode != "" {
		mockedOTP := mocks.MockGenerateOTP{Result: configs.Authentication.MockOTPCode}
		otpGenerator = mockedOTP.GenerateOTP
	}

	sendOTPHandler := NewSendOTPHandler(v, dbManager, otpGenerator, configs)
	loginHandler := NewLoginHandler(v, dbManager, authCookie)
	logoutHandler := NewLogoutHandler(dbManager, authCookie)

	return func(r chi.Router) {
		r.Post("/otp", sendOTPHandler)
		r.Post("/login", loginHandler)
		r.Post("/logout", logoutHandler)
	}
}
