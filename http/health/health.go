package health

import (
	"net/http"

	"github.com/go-chi/chi/v5"
	"github.com/jackc/pgx/v4/pgxpool"
	"go.uber.org/zap"

	"gitlab.mooncascade.net/roman/go-starter-kit/config"
	"gitlab.mooncascade.net/roman/go-starter-kit/internal/handler"
	"gitlab.mooncascade.net/roman/go-starter-kit/internal/logging"
)

type Schema struct {
	DBAvailable bool `json:"dbAvailable"`
}

func healthcheckHandler(dbConnPool *pgxpool.Pool) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		logger := logging.GetLogger(r.Context())

		// Default response: healthy!
		status := http.StatusOK
		healthResponse := Schema{
			DBAvailable: true,
		}

		// Unhealthy response if DB ping fails.
		err := dbConnPool.Ping(r.Context())
		if err != nil {
			logger.Error("Error pinging postgres", zap.Error(err))
			healthResponse.DBAvailable = false
			status = http.StatusInternalServerError
		}

		handler.WriteSuccessResponseJSON(r.Context(), w, status, healthResponse)
	}
}

func GetRouter(r chi.Router, c config.Config, dbConnPool *pgxpool.Pool) func(r chi.Router) {
	return func(r chi.Router) {
		r.Get("/", healthcheckHandler(dbConnPool))
	}
}
