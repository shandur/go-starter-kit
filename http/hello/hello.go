package hello

import (
	"net/http"

	"github.com/go-chi/chi/v5"

	"gitlab.mooncascade.net/roman/go-starter-kit/internal/handler"
)

type HelloWorldReponse struct {
	Name string `json:"name"`
}

func healthcheckHandler() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		response := handler.Response{
			Data: HelloWorldReponse{
				Name: "Indrek",
			},
		}

		handler.WriteSuccessResponseJSON(r.Context(), w, http.StatusOK, response)
	}
}

func GetRouter(r chi.Router) func(r chi.Router) {
	return func(r chi.Router) {
		r.Get("/", healthcheckHandler())
	}
}
