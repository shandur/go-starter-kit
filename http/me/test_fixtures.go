package me

import (
	"context"
	"testing"

	"github.com/go-chi/chi/v5"
	"github.com/google/uuid"
	"github.com/stretchr/testify/require"

	"gitlab.mooncascade.net/roman/go-starter-kit/config"
	"gitlab.mooncascade.net/roman/go-starter-kit/internal/db"
	"gitlab.mooncascade.net/roman/go-starter-kit/internal/testutil"
	"gitlab.mooncascade.net/roman/go-starter-kit/internal/testutil/mocks"
	"gitlab.mooncascade.net/roman/go-starter-kit/storage"
)

type testFixtures struct {
	dbManager storage.DBManager
	router    chi.Router
	cleanup   func()
}

func setupTests(t *testing.T) *testFixtures {
	t.Helper()

	// Read configs.
	ctx := context.Background()
	env, err := config.ReadEnvVars(ctx)
	require.NoError(t, err, "parsing environment variables")

	// Setup database.
	pgClient, err := db.GetPostgresClient(ctx, &env)
	require.NoError(t, err, "creating postgres client")

	sb := storage.NewStatementBuilder()
	conn := pgClient.Conn

	runMigration := true
	err = db.Migrate(&env, pgClient.DB.DB, runMigration)
	require.NoError(t, err, "running migration")

	// Setup DB Manager.
	dbManager, err := storage.NewDBManager(storage.DBConfig{
		Conn:             conn,
		StatementBuilder: sb,
	})
	require.NoError(t, err, "failed to create DB manager")

	// Register routes.
	router := chi.NewRouter()

	// Cleanup.
	cleanup := func() {
		err := testutil.TruncateUsers(ctx, conn)
		require.NoError(t, err, "Could not clean up users tables")

		conn.Close()
	}

	return &testFixtures{
		dbManager: dbManager,
		router:    router,
		cleanup:   cleanup,
	}
}

// registerRoutes is a helper func to register middlewares
// and routes.
func (f *testFixtures) registerRoutes(userUUID *uuid.UUID) {
	f.router.Use(mocks.MockAuthClientContextMiddleware(f.dbManager, userUUID))

	f.router.Route("/v1/me", GetRouter(f.dbManager))
}
