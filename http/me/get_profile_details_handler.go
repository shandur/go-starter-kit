package me

import (
	"net/http"

	"github.com/google/uuid"
	"github.com/pkg/errors"
	"go.uber.org/zap"

	"gitlab.mooncascade.net/roman/go-starter-kit/internal/handler"
	"gitlab.mooncascade.net/roman/go-starter-kit/internal/logging"
	"gitlab.mooncascade.net/roman/go-starter-kit/storage"
)

func NewGetProfileDetailsHandler(dbManager storage.DBManager) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		logger := logging.GetLogger(r.Context())

		userUUID, ok := r.Context().Value(handler.ContextUserIDKey).(uuid.UUID)
		if !ok {
			logger.Warn("get_profile_details: user UUID is not set")

			handler.WriteErrorResponse(r.Context(), w, http.StatusUnauthorized, errors.New("user UUID is empty"), handler.ErrNotAuthenticatedCode)
			return
		}

		user, err := dbManager.GetUserByUUID(r.Context(), userUUID)
		if err != nil {
			logger.Error("get_profile_details: failed to find user", zap.String("user_id", userUUID.String()), zap.Error(err))

			handler.WriteErrorResponse(r.Context(), w, http.StatusInternalServerError, errors.New("failed to get user details"), handler.ErrInternalCode)
			return
		}

		handler.WriteSuccessResponseJSON(r.Context(), w, http.StatusOK, marshalUser(*user))
	})
}
