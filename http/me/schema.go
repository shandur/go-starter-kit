package me

import (
	"github.com/google/uuid"

	"gitlab.mooncascade.net/roman/go-starter-kit/storage"
)

type UserSchema struct {
	ID           uuid.UUID        `json:"id"`
	FirstName    string           `json:"firstName"`
	LastName     string           `json:"lastName"`
	EmailAddress string           `json:"email"`
	Role         storage.UserRole `json:"role"`
}

func marshalUser(user storage.User) UserSchema {
	return UserSchema{
		ID:           user.ID,
		FirstName:    user.FirstName,
		LastName:     user.LastName,
		EmailAddress: user.EmailAddress,
		Role:         storage.UserRole(user.Role),
	}
}
