package me

import (
	"context"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/google/uuid"
	"github.com/stretchr/testify/require"
	"gitlab.mooncascade.net/roman/go-starter-kit/internal/handler"
	"gitlab.mooncascade.net/roman/go-starter-kit/internal/testutil"
	"gitlab.mooncascade.net/roman/go-starter-kit/storage"
)

func TestGetProfileDetails_InvalidUserUUIDError(t *testing.T) {
	t.Run("empty user UUID", func(t *testing.T) {
		fixtures := setupTests(t)
		defer fixtures.cleanup()

		// Act.
		fixtures.registerRoutes(nil)
		server := httptest.NewServer(fixtures.router)
		status, response := testutil.MakeRequest(t, server, http.MethodGet, "/v1/me", nil)

		// Assert.
		expected := handler.Response{
			Errors: []handler.ResponseError{
				{
					Title: "user UUID is empty",
					Code:  handler.ErrNotAuthenticatedCode,
				},
			},
		}

		require.EqualValues(t, expected, response)
		require.Equal(t, http.StatusUnauthorized, status)
	})
}
func TestGetProfileDetails_UserNotExistError(t *testing.T) {
	fixtures := setupTests(t)
	defer fixtures.cleanup()

	// Arrange.
	unknownUserUUID := uuid.New()

	// Act.
	fixtures.registerRoutes(&unknownUserUUID)
	server := httptest.NewServer(fixtures.router)
	status, response := testutil.MakeRequest(t, server, http.MethodGet, "/v1/me", nil)

	// Assert.
	expected := handler.Response{
		Errors: []handler.ResponseError{
			{
				Title: handler.ErrNotAuthorized.Error(),
				Code:  handler.ErrNotAuthenticatedCode,
			},
		},
	}

	require.EqualValues(t, expected, response)
	require.Equal(t, http.StatusUnauthorized, status)
}

func TestGetProfileDetails_Success(t *testing.T) {
	fixtures := setupTests(t)
	defer fixtures.cleanup()

	// Arrange.
	user, err := fixtures.dbManager.CreateUser(context.Background(), storage.User{
		FirstName:    "FirstName1",
		LastName:     "LastName1",
		EmailAddress: "user1@test,com",
	})
	require.NoError(t, err)
	require.NotEmpty(t, user)

	// Act.
	fixtures.registerRoutes(&user.ID)
	server := httptest.NewServer(fixtures.router)
	status, response := testutil.MakeRequest(t, server, http.MethodGet, "/v1/me", nil)

	// Assert.

	require.Equal(t, http.StatusOK, status)
	require.Empty(t, response.Errors)

	data := response.Data.(map[string]interface{})

	require.NotEmpty(t, data["id"])
	require.Equal(t, user.EmailAddress, data["email"])
	require.Equal(t, user.FirstName, data["firstName"])
	require.Equal(t, user.LastName, data["lastName"])
	require.Equal(t, string(storage.UserRoleTypeGuest), data["role"])
}
