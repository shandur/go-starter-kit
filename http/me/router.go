package me

import (
	"github.com/go-chi/chi/v5"

	"gitlab.mooncascade.net/roman/go-starter-kit/storage"
)

func GetRouter(
	dbManager storage.DBManager,
) func(r chi.Router) {
	getProfileDetailsHandler := NewGetProfileDetailsHandler(dbManager)

	return func(r chi.Router) {
		r.Get("/", getProfileDetailsHandler)
	}
}
