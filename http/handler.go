package httphandler

import (
	"net/http"

	"github.com/go-chi/chi/v5"
	chiMiddleware "github.com/go-chi/chi/v5/middleware"
	chiCors "github.com/go-chi/cors"
	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/pkg/errors"
	"go.uber.org/zap"

	"gitlab.mooncascade.net/roman/go-starter-kit/config"
	"gitlab.mooncascade.net/roman/go-starter-kit/handler/http/auth"
	"gitlab.mooncascade.net/roman/go-starter-kit/handler/http/health"
	"gitlab.mooncascade.net/roman/go-starter-kit/handler/http/me"
	"gitlab.mooncascade.net/roman/go-starter-kit/handler/http/users"
	"gitlab.mooncascade.net/roman/go-starter-kit/internal/authorization"
	"gitlab.mooncascade.net/roman/go-starter-kit/internal/logging"
	"gitlab.mooncascade.net/roman/go-starter-kit/internal/middleware"
	"gitlab.mooncascade.net/roman/go-starter-kit/internal/testutil/mocks"
	"gitlab.mooncascade.net/roman/go-starter-kit/internal/validator"
	"gitlab.mooncascade.net/roman/go-starter-kit/storage"
)

const (
	GzipCompressionLevel int = 5
)

// Handler structure.
type Handler struct {
	router *chi.Mux
}

// Make sure that Handler implements http.Handler.
var _ http.Handler = (*Handler)(nil)

// New is a Handler constructor.
func New(configs config.Config, dbConnPool *pgxpool.Pool) (*Handler, error) {
	r := chi.NewRouter()
	logger := logging.GetLogger()

	// Chi middleware docs: https://github.com/go-chi/chi#middlewares
	r.Use(chiMiddleware.RequestID)
	r.Use(middleware.ClientRealIPMiddleware)
	r.Use(middleware.ZapLogger(logger))
	r.Use(middleware.RecoverAndLog(logger))
	r.Use(chiMiddleware.CleanPath)
	r.Use(chiMiddleware.RedirectSlashes)
	r.Use(chiMiddleware.NoCache)
	r.Use(chiMiddleware.Compress(GzipCompressionLevel))

	// TODO: set more restrictive scope for CORS.
	r.Use(chiCors.AllowAll().Handler)

	// TODO: uncomment once context deadlines are respected in handlers
	// r.Use(chiMiddleware.Timeout(
	// 	time.Duration(configs.Service.HTTPRequestTimeoutSecs) * time.Second,
	// ))

	v := validator.NewValidator()
	permissions, err := authorization.LoadPermissions()
	if err != nil {
		return nil, errors.Wrap(err, "httphandler.New: error loading permissions")
	}

	dbManager, err := storage.NewDBManager(storage.DBConfig{
		Conn:             dbConnPool,
		StatementBuilder: storage.NewStatementBuilder(),
	})
	if err != nil {
		return nil, errors.Wrap(err, "httphandler.New storage.NewDBManager error")
	}

	// Should be out of groups with middleware due to Chi complaints.
	r.Route("/v1/healthz", health.GetRouter(r, configs, dbConnPool))

	// Login routes.
	r.Route("/v1/auth", auth.GetRouter(v, dbManager, configs))

	r.Route("/v1", func(r chi.Router) {
		// Mocking logged-in user ID for local env.
		if configs.IsLocalEnvironment() && configs.Authentication.MockUserUUID != "" {
			mockedUserUUID := uuid.MustParse(configs.Authentication.MockUserUUID)
			r.Use(mocks.MockAuthClientContextMiddleware(dbManager, &mockedUserUUID))
			logger.Warn("mocking client authentication", zap.String("mock_user_id", mockedUserUUID.String()))
		} else {
			r.Use(middleware.AuthClientContextMiddleware(dbManager, authorization.NewAuthCookie(configs.Service.APIDomain, !configs.IsLocalEnvironment())))
		}

		// Authorization (RBAC).
		r.Use(middleware.UserRoleAuthorizationMiddleware(permissions))

		r.Route("/users", users.GetRouter(v, dbManager))
		r.Route("/me", me.GetRouter(dbManager))
	})

	return &Handler{
		router: r,
	}, nil
}

// ServeHTTP is a compatibility method to work with generic HTTP server.
func (h *Handler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	h.router.ServeHTTP(w, r)
}
