package users

import (
	"net/http"

	"github.com/go-playground/validator/v10"
	"github.com/pkg/errors"

	"gitlab.mooncascade.net/roman/go-starter-kit/internal/handler"
	"gitlab.mooncascade.net/roman/go-starter-kit/internal/strutil"
	"gitlab.mooncascade.net/roman/go-starter-kit/storage"
)

// NewCreateUserHandler handles requests to create a user.
func NewCreateUserHandler(v *validator.Validate, dbManager storage.DBManager) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var request UserCreateSchema

		err := handler.BindRequestJSON(w, r, &request)
		if err != nil {
			handler.WriteErrorResponse(r.Context(), w, http.StatusBadRequest, errors.New("cannot decode JSON payload"), handler.ErrClientCode)
			return
		}

		validationErrors := handler.ValidateRequest(v, request)
		if len(validationErrors) > 0 {
			handler.WriteValidationErrorResponse(r.Context(), w, validationErrors)
			return
		}

		user, err := dbManager.GetUserByEmail(r.Context(), request.EmailAddress)
		// Return the user if exists - skip error handling.
		if err == nil {
			handler.WriteSuccessResponseJSON(r.Context(), w, http.StatusOK, marshalUser(user))
			return
		}

		// TODO: add check on who can update user's role.
		user, err = dbManager.CreateUser(r.Context(), storage.User{
			FirstName:    request.FirstName,
			LastName:     request.LastName,
			EmailAddress: request.EmailAddress,
			Role:         strutil.StringPointerToString(request.Role),
		})

		if err != nil {
			handler.WriteErrorResponse(r.Context(), w, http.StatusInternalServerError,
				errors.Wrap(err, "failed to get user"),
				handler.ErrInternalCode,
			)
			return
		}

		// Return the response.
		handler.WriteSuccessResponseJSON(r.Context(), w, http.StatusCreated, marshalUser(user))
	}
}
