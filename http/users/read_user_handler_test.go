package users

import (
	"context"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/require"

	"gitlab.mooncascade.net/roman/go-starter-kit/internal/handler"
	"gitlab.mooncascade.net/roman/go-starter-kit/internal/testutil"
	"gitlab.mooncascade.net/roman/go-starter-kit/storage"
)

func TestReadUserHandler_InvalidPathParamUserUUIDError(t *testing.T) {
	// Arrange.
	fixtures := setupUserTests(t)
	defer fixtures.cleanup()

	// Act.
	server := httptest.NewServer(fixtures.router)
	status, response := testutil.MakeRequest(
		t, server, http.MethodGet, "/v1/users/invalid-UUID", nil,
	)

	// Assert.
	expected := handler.Response{
		Errors: []handler.ResponseError{
			{
				Title: "invalid user id: invalid UUID length: 12",
				Code:  handler.ErrBadInput,
			},
		},
	}

	require.EqualValues(t, expected, response)
	require.Equal(t, http.StatusBadRequest, status)
}

func TestReadUserHandler_UserNotFoundError(t *testing.T) {
	// Arrange.
	fixtures := setupUserTests(t)
	defer fixtures.cleanup()

	// Act.
	server := httptest.NewServer(fixtures.router)
	status, response := testutil.MakeRequest(
		t, server, http.MethodGet, fmt.Sprintf("/v1/users/%s", testutil.MustUUID(t)), nil,
	)

	// Assert.
	expected := handler.Response{
		Errors: []handler.ResponseError{
			{
				Title: "user not found: row(s) not found",
				Code:  handler.ErrNotFound,
			},
		},
	}

	require.EqualValues(t, expected, response)
	require.Equal(t, http.StatusNotFound, status)
}

func TestReadUserHandler_ReadUserSuccess(t *testing.T) {
	fixtures := setupUserTests(t)
	defer fixtures.cleanup()

	// Arrange.
	user, err := fixtures.dbManager.CreateUser(context.Background(), storage.User{
		FirstName:    "FirstName1",
		LastName:     "LastName1",
		EmailAddress: "test1@starter.com",
		Role:         string(storage.UserRoleTypeModerator),
	})
	require.NoError(t, err)

	// Act.
	server := httptest.NewServer(fixtures.router)
	status, response := testutil.MakeRequest(
		t, server, http.MethodGet, fmt.Sprintf("/v1/users/%s", user.ID.String()), nil,
	)

	// Assert.
	require.Empty(t, response.Errors)
	require.Equal(t, http.StatusOK, status)

	data := response.Data.(map[string]interface{})

	require.Equal(t, user.ID.String(), data["id"])
	require.Equal(t, user.FirstName, data["firstName"])
	require.Equal(t, user.LastName, data["lastName"])
	require.Equal(t, user.Role, data["role"])
	require.Equal(t, user.EmailAddress, data["email"])
}
