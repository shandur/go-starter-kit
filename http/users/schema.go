package users

import (
	"github.com/google/uuid"

	"gitlab.mooncascade.net/roman/go-starter-kit/storage"
)

// UserSchema is used for User Create, Read, List actions.
type UserSchema struct {
	ID           uuid.UUID        `json:"id"`
	FirstName    string           `json:"firstName"`
	LastName     string           `json:"lastName"`
	EmailAddress string           `json:"email"`
	Role         storage.UserRole `json:"role"`
}

// UserListSchema is used for User list responses.
type UserListSchema struct {
	Users []UserSchema `json:"users"`
}

// UserCreateSchema is used for User create action.
type UserCreateSchema struct {
	FirstName    string  `json:"firstName"   validate:"required"`
	LastName     string  `json:"lastName"    validate:"required"`
	EmailAddress string  `json:"email"       validate:"required,email"`
	Role         *string `json:"role" validate:"omitempty,oneof=admin moderator guest"`
}

// UserUpdateSchema is used for User update action, and contains no required fields.
type UserUpdateSchema struct {
	FirstName *string `json:"firstName"    validate:"omitempty"`
	LastName  *string `json:"lastName"     validate:"omitempty"`
	Role      *string `json:"role" validate:"omitempty,oneof=admin moderator guest"`
}

func marshalUser(user *storage.User) UserSchema {
	return UserSchema{
		ID:           user.ID,
		FirstName:    user.FirstName,
		LastName:     user.LastName,
		EmailAddress: user.EmailAddress,
		Role:         storage.UserRole(user.Role),
	}
}

func marshalUsers(sites []storage.User) UserListSchema {
	var usersSchema UserListSchema
	usersSchema.Users = make([]UserSchema, 0)
	for i := range sites {
		usersSchema.Users = append(
			usersSchema.Users, marshalUser(&sites[i]),
		)
	}
	return usersSchema
}
