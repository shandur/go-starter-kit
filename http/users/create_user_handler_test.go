package users

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/require"

	"gitlab.mooncascade.net/roman/go-starter-kit/internal/handler"
	"gitlab.mooncascade.net/roman/go-starter-kit/internal/strutil"
	"gitlab.mooncascade.net/roman/go-starter-kit/internal/testutil"
	"gitlab.mooncascade.net/roman/go-starter-kit/storage"
)

func TestCreateUserHandler_InvalidJSONErrors(t *testing.T) {
	// Arrange.
	fixtures := setupUserTests(t)
	defer fixtures.cleanup()
	body := `{`

	// Act.
	server := httptest.NewServer(fixtures.router)
	status, response := testutil.MakeRequest(t, server, http.MethodPost, "/v1/users", &body)

	// Assert.
	expected := handler.Response{
		Errors: []handler.ResponseError{
			{
				Title: "cannot decode JSON payload",
				Code:  handler.ErrClientCode,
			},
		},
	}

	require.Equal(t, http.StatusBadRequest, status)
	require.EqualValues(t, expected, response)
}

func TestCreateUserHandler_RequestValidationErrors(t *testing.T) {
	// Arrange.
	fixtures := setupUserTests(t)
	defer fixtures.cleanup()

	tests := []struct {
		name     string
		input    UserCreateSchema
		expected handler.Response
	}{
		{
			name:  "1. With empty request",
			input: UserCreateSchema{},
			expected: handler.Response{
				Errors: []handler.ResponseError{
					{
						Code: "required",
						Source: handler.ResponseErrorSource{
							Pointer: handler.ConvertToErrorSourcePointer("firstName"),
						},
					},
					{
						Code: "required",
						Source: handler.ResponseErrorSource{
							Pointer: handler.ConvertToErrorSourcePointer("lastName"),
						},
					},
					{
						Code: "required",
						Source: handler.ResponseErrorSource{
							Pointer: handler.ConvertToErrorSourcePointer("email"),
						},
					},
				},
			},
		},
		{
			name: "2. With invalid email",
			input: UserCreateSchema{
				FirstName:    "FirstName1",
				LastName:     "LastName1",
				EmailAddress: "invalid_email@",
			},
			expected: handler.Response{
				Errors: []handler.ResponseError{
					{
						Code: "email",
						Source: handler.ResponseErrorSource{
							Pointer: handler.ConvertToErrorSourcePointer("email"),
						},
					},
				},
			},
		},
	}

	for _, test := range tests {
		test := test
		t.Run(test.name, func(t *testing.T) {
			// Act.
			body := testutil.EncodeRequest(t, test.input)

			server := httptest.NewServer(fixtures.router)
			status, response := testutil.MakeRequest(t, server, http.MethodPost, "/v1/users", &body)

			// Assert.
			require.EqualValues(t, test.expected, response)
			require.Equal(t, http.StatusBadRequest, status)
		})
	}
}

func TestCreateUserHandler_CreateNewUserSuccess(t *testing.T) {
	t.Run("creates a new user with default role", func(t *testing.T) {
		fixtures := setupUserTests(t)
		defer fixtures.cleanup()

		// Arrange.
		input := UserCreateSchema{
			FirstName:    "FirstName1",
			LastName:     "LastName1",
			EmailAddress: "user1@test.com",
		}

		// Act.
		body := testutil.EncodeRequest(t, input)

		server := httptest.NewServer(fixtures.router)
		status, response := testutil.MakeRequest(t, server, http.MethodPost, "/v1/users", &body)

		// Assert.
		require.Empty(t, response.Errors)
		require.Equal(t, http.StatusCreated, status)

		data := response.Data.(map[string]interface{})

		require.NotEmpty(t, data["id"])
		require.Equal(t, input.EmailAddress, data["email"])
		require.Equal(t, input.FirstName, data["firstName"])
		require.Equal(t, input.LastName, data["lastName"])
		require.Equal(t, string(storage.UserRoleTypeGuest), data["role"]) // default role
	})

	t.Run("creates a new user with role", func(t *testing.T) {
		fixtures := setupUserTests(t)
		defer fixtures.cleanup()

		// Arrange.
		input := UserCreateSchema{
			FirstName:    "FirstName1",
			LastName:     "LastName1",
			EmailAddress: "user1@test.com",
			Role:         strutil.StringToPointer(string(storage.UserRoleTypeAdmin)),
		}

		// Act.
		body := testutil.EncodeRequest(t, input)

		server := httptest.NewServer(fixtures.router)
		status, response := testutil.MakeRequest(t, server, http.MethodPost, "/v1/users", &body)

		// Assert.
		require.Empty(t, response.Errors)
		require.Equal(t, http.StatusCreated, status)

		data := response.Data.(map[string]interface{})

		require.NotEmpty(t, data["id"])
		require.Equal(t, input.EmailAddress, data["email"])
		require.Equal(t, input.FirstName, data["firstName"])
		require.Equal(t, input.LastName, data["lastName"])
		require.Equal(t, string(storage.UserRoleTypeAdmin), data["role"])
	})
}
