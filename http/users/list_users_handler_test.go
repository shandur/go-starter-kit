package users

import (
	"context"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/require"

	"gitlab.mooncascade.net/roman/go-starter-kit/internal/testutil"
	"gitlab.mooncascade.net/roman/go-starter-kit/storage"
)

func TestListUsersHandler_ReadUserSuccess(t *testing.T) {
	fixtures := setupUserTests(t)
	defer fixtures.cleanup()

	// Arrange.
	user, err := fixtures.dbManager.CreateUser(context.Background(), storage.User{
		FirstName:    "FirstName1",
		LastName:     "LastName1",
		EmailAddress: "test1@starter.com",
		Role:         string(storage.UserRoleTypeModerator),
	})
	require.NoError(t, err)

	// Act.
	server := httptest.NewServer(fixtures.router)
	status, response := testutil.MakeRequest(
		t, server, http.MethodGet, fmt.Sprintf("/v1/users/%s", user.ID.String()), nil,
	)

	// Assert.
	require.Equal(t, http.StatusOK, status)
	require.Empty(t, response.Errors)

	data := response.Data.(map[string]interface{})

	require.Equal(t, user.ID.String(), data["id"])
	require.Equal(t, user.FirstName, data["firstName"])
	require.Equal(t, user.LastName, data["lastName"])
	require.Equal(t, user.Role, data["role"])
	require.Equal(t, user.EmailAddress, data["email"])
}

func TestListUsersHandler(t *testing.T) {
	fixtures := setupUserTests(t)
	defer fixtures.cleanup()
	server := httptest.NewServer(fixtures.router)

	// Create two users
	userA, err := fixtures.dbManager.CreateUser(context.Background(), storage.User{
		FirstName:    "Bob",
		LastName:     "LastName",
		EmailAddress: "email2@starter.com",
		Role:         string(storage.UserRoleTypeModerator),
	})
	require.NoError(t, err)
	userB, err := fixtures.dbManager.CreateUser(context.Background(), storage.User{
		FirstName:    "Alice",
		LastName:     "LastName",
		EmailAddress: "email1@starter.com",
		Role:         string(storage.UserRoleTypeModerator),
	})
	require.NoError(t, err)

	t.Run("list users, sorted", func(t *testing.T) {
		// List users by name ascending (default)
		status, response := testutil.MakeRequest(
			t, server, http.MethodGet, "/v1/users", nil,
		)
		require.Equal(t, http.StatusOK, status)
		data := response.Data.(map[string]interface{})
		users := data["users"].([]interface{})
		require.Equal(t, 2, len(users))
		require.Equal(t,
			userB.EmailAddress,
			users[0].(map[string]interface{})["email"].(string),
		)
		require.Equal(t,
			userA.EmailAddress,
			users[1].(map[string]interface{})["email"].(string),
		)

		// List users by first name descending
		status, response = testutil.MakeRequest(
			t, server, http.MethodGet, "/v1/users?sort=-firstName", nil,
		)
		require.Equal(t, http.StatusOK, status)
		data = response.Data.(map[string]interface{})
		users = data["users"].([]interface{})
		require.Equal(t, 2, len(users))
		require.Equal(t,
			userA.FirstName,
			users[0].(map[string]interface{})["firstName"].(string),
		)
		require.Equal(t,
			userB.FirstName,
			users[1].(map[string]interface{})["firstName"].(string),
		)
	})
	t.Run("list users, filtering by email", func(t *testing.T) {
		status, response := testutil.MakeRequest(
			t, server, http.MethodGet, "/v1/users?email=email1@starter.com", nil,
		)
		require.Equal(t, http.StatusOK, status)
		data := response.Data.(map[string]interface{})
		users := data["users"].([]interface{})
		require.Equal(t, 1, len(users))
		require.Equal(t,
			userB.EmailAddress,
			users[0].(map[string]interface{})["email"].(string),
		)
	})
	t.Run("list users, with limit and offset", func(t *testing.T) {
		status, response := testutil.MakeRequest(
			t, server, http.MethodGet, "/v1/users?limit=1&offset=1", nil,
		)
		require.Equal(t, http.StatusOK, status)
		data := response.Data.(map[string]interface{})
		users := data["users"].([]interface{})
		require.Equal(t, 1, len(users))
		require.Equal(t,
			userA.EmailAddress,
			users[0].(map[string]interface{})["email"].(string),
		)
	})
}
