package users

import (
	"net/http"

	"github.com/go-chi/chi/v5"
	"github.com/go-playground/validator/v10"
	"github.com/google/uuid"

	"gitlab.mooncascade.net/roman/go-starter-kit/storage"
)

func GetRouter(
	validator *validator.Validate,
	dbManager storage.DBManager,
) func(r chi.Router) {
	// Handlers.
	postCreateUserHandler := NewCreateUserHandler(validator, dbManager)
	readUserHandler := NewReadUserHandler(validator, dbManager)
	updateUserHandler := NewUpdateUserHandler(validator, dbManager)
	deleteUserHandler := NewDeleteUserHandler(validator, dbManager)
	listUsersHandler := NewListUsersHandler(validator, dbManager)

	// Register handlers.
	return func(r chi.Router) {
		r.Post("/", postCreateUserHandler)
		r.Get("/", listUsersHandler)
		r.Route("/{userUUID}", func(r chi.Router) {
			r.Get("/", readUserHandler)
			r.Patch("/", updateUserHandler)
			r.Delete("/", deleteUserHandler)
		})
	}
}

// TODO: Extract to `internal/handler` package.
func parseUserIDFromURL(r *http.Request) (uuid.UUID, error) {
	userUUIDStr := chi.URLParam(r, "userUUID")
	return uuid.Parse(userUUIDStr)
}
