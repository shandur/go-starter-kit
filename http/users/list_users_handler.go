package users

import (
	"net/http"
	"net/url"
	"strings"

	"github.com/go-playground/validator/v10"
	"github.com/pkg/errors"

	"gitlab.mooncascade.net/roman/go-starter-kit/internal/handler"
	"gitlab.mooncascade.net/roman/go-starter-kit/storage"
)

var (
	DefaultListSortOptions storage.SortOptions = []storage.SortPair{
		{
			Field:     "firstName",
			Direction: storage.SortDirectionAscending,
		},
		{
			Field:     "lastName",
			Direction: storage.SortDirectionAscending,
		},
	}
	DefaultListLimit uint64 = 100
)

type ListQueryParams struct {
	// Filters
	// TODO: Support more as needs arise
	Emails []string
}

func NewListUsersHandler(v *validator.Validate, dbManager storage.DBManager) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		query := ListQueryParamsToQuery(r.URL.Query())
		users, err := dbManager.FindUsers(r.Context(), query)

		if err != nil {
			handler.WriteErrorResponse(r.Context(), w, http.StatusInternalServerError, errors.Wrap(err, "listing users"), handler.ErrInternalCode)
			return
		}

		handler.WriteSuccessResponseJSON(
			r.Context(), w, http.StatusOK, marshalUsers(users),
		)
	}
}

func ListQueryParamsToQuery(params url.Values) storage.UsersQuery {
	userQueryParams, commonQueryParams := parseListQueryParams(params)

	// Use sort and limit defaults if not already set
	sort := DefaultListSortOptions
	if commonQueryParams.Sort != nil {
		sort = commonQueryParams.Sort
	}
	limit := DefaultListLimit
	if commonQueryParams.Limit > 0 {
		limit = commonQueryParams.Limit
	}

	return storage.UsersQuery{
		Emails: userQueryParams.Emails,
		Limit:  limit,
		Offset: commonQueryParams.Offset,
		Sort:   sort,
	}
}

func parseListQueryParams(params url.Values) (ListQueryParams, handler.CommonQueryParams) {
	var emails []string

	emailsRaw := strings.ToLower(params.Get("email"))
	if emailsRaw != "" {
		emails = strings.Split(emailsRaw, ",")
	}

	return ListQueryParams{
		Emails: emails,
	}, handler.ParseCommonQueryParams(params)
}
