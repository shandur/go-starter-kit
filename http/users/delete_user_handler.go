package users

import (
	"net/http"

	"github.com/go-playground/validator/v10"
	"github.com/pkg/errors"

	"gitlab.mooncascade.net/roman/go-starter-kit/internal/handler"
	"gitlab.mooncascade.net/roman/go-starter-kit/storage"
)

func NewDeleteUserHandler(v *validator.Validate, dbManager storage.DBManager) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		userUUID, err := parseUserIDFromURL(r)
		if err != nil {
			handler.WriteErrorResponse(
				r.Context(),
				w,
				http.StatusBadRequest,
				errors.Wrap(err, "invalid user id"),
				handler.ErrBadInput,
			)
			return
		}

		err = dbManager.RemoveUser(r.Context(), userUUID)
		if err == storage.ErrNotFound {
			handler.WriteErrorResponse(
				r.Context(),
				w,
				http.StatusNotFound,
				errors.Wrap(err, "user not found"),
				handler.ErrNotFound,
			)
			return
		}
		if err != nil {
			handler.WriteErrorResponse(
				r.Context(),
				w,
				http.StatusInternalServerError,
				errors.Wrap(err, "deleting user"),
				handler.ErrInternalCode,
			)
			return
		}

		// 204 status code does not allow body, so just write header
		w.WriteHeader(http.StatusNoContent)
	}
}
