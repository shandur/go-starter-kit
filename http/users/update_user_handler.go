package users

import (
	"net/http"

	"github.com/go-chi/chi/v5"
	"github.com/go-playground/validator/v10"
	"github.com/google/uuid"
	"github.com/pkg/errors"

	"gitlab.mooncascade.net/roman/go-starter-kit/internal/handler"
	"gitlab.mooncascade.net/roman/go-starter-kit/storage"
)

// NewUpdateUserHandler handles requests to update a user.
func NewUpdateUserHandler(
	v *validator.Validate,
	dbManager storage.DBManager,
) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		userUUIDStr := chi.URLParam(r, "userUUID")
		userUUID, err := uuid.Parse(userUUIDStr)
		if err != nil {
			handler.WriteErrorResponse(r.Context(), w, http.StatusBadRequest, errors.New("invalid UUID"), handler.ErrBadInput)
			return
		}

		var request UserUpdateSchema

		err = handler.BindRequestJSON(w, r, &request)
		if err != nil {
			handler.WriteErrorResponse(r.Context(), w, http.StatusBadRequest, errors.New("cannot decode JSON payload"), handler.ErrClientCode)
			return
		}

		validationErrors := handler.ValidateRequest(v, request)
		if len(validationErrors) > 0 {
			handler.WriteValidationErrorResponse(r.Context(), w, validationErrors)
			return
		}

		// Update user details.
		user, err := dbManager.EditUser(r.Context(), storage.UserUpdate{
			ID:        userUUID,
			FirstName: request.FirstName,
			LastName:  request.LastName,
			Role:      request.Role,
		})

		if err != nil {
			if err == storage.ErrNotFound {
				handler.WriteErrorResponse(r.Context(), w, http.StatusNotFound, errors.New("user not found"), handler.ErrNotFound)
				return
			}

			handler.WriteErrorResponse(r.Context(), w, http.StatusInternalServerError, errors.Wrap(err, "failed to edit user"), handler.ErrInternalCode)
			return
		}

		// Return the response.
		handler.WriteSuccessResponseJSON(r.Context(), w, http.StatusOK, marshalUser(user))
	}
}
