package users

import (
	"context"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/require"

	"gitlab.mooncascade.net/roman/go-starter-kit/internal/handler"
	"gitlab.mooncascade.net/roman/go-starter-kit/internal/strutil"
	"gitlab.mooncascade.net/roman/go-starter-kit/internal/testutil"
	"gitlab.mooncascade.net/roman/go-starter-kit/storage"
)

func TestUpdateUserHandler_InvalidPathParamUserUUIDError(t *testing.T) {
	// Arrange.
	fixtures := setupUserTests(t)
	defer fixtures.cleanup()

	// Act.
	server := httptest.NewServer(fixtures.router)
	status, response := testutil.MakeRequest(t, server, http.MethodPatch, "/v1/users/invalid-UUID", nil)

	// Assert.
	expected := handler.Response{
		Errors: []handler.ResponseError{
			{
				Title: "invalid UUID",
				Code:  handler.ErrBadInput,
			},
		},
	}

	require.EqualValues(t, expected, response)
	require.Equal(t, http.StatusBadRequest, status)
}

func TestUpdateUserHandler_InvalidJSONErrors(t *testing.T) {
	// Arrange.
	fixtures := setupUserTests(t)
	defer fixtures.cleanup()

	// Act.
	invalidBody := `{`

	server := httptest.NewServer(fixtures.router)
	status, response := testutil.MakeRequest(t, server, http.MethodPatch, fmt.Sprintf("/v1/users/%s", testutil.MustUUID(t)), &invalidBody)

	// Assert.
	expected := handler.Response{
		Errors: []handler.ResponseError{
			{
				Title: "cannot decode JSON payload",
				Code:  handler.ErrClientCode,
			},
		},
	}

	require.EqualValues(t, expected, response)
	require.Equal(t, http.StatusBadRequest, status)
}

func TestUpdateUserHandler_RequestValidationErrors(t *testing.T) {
	// Arrange.
	fixtures := setupUserTests(t)
	defer fixtures.cleanup()

	tests := []struct {
		name     string
		input    UserUpdateSchema
		expected handler.Response
	}{
		{
			name: "1. With invalid role",
			input: UserUpdateSchema{
				FirstName: strutil.StringToPointer("FirstName1"),
				LastName:  strutil.StringToPointer("LastName1"),
				Role:      strutil.StringToPointer(string("invalid_role")),
			},
			expected: handler.Response{
				Errors: []handler.ResponseError{
					{
						Code: "oneof",
						Source: handler.ResponseErrorSource{
							Pointer: handler.ConvertToErrorSourcePointer("role"),
						},
					},
				},
			},
		},
	}

	for _, test := range tests {
		test := test
		t.Run(test.name, func(t *testing.T) {
			// Act.
			body := testutil.EncodeRequest(t, test.input)

			server := httptest.NewServer(fixtures.router)
			status, response := testutil.MakeRequest(t, server, http.MethodPatch, fmt.Sprintf("/v1/users/%s", testutil.MustUUID(t)), &body)

			// Assert.
			require.Equal(t, http.StatusBadRequest, status)
			require.EqualValues(t, test.expected, response)
		})
	}
}

func TestUpdateUserHandler_UserNotFoundError(t *testing.T) {
	fixtures := setupUserTests(t)
	defer fixtures.cleanup()

	// Arrange.
	input := UserUpdateSchema{
		FirstName: strutil.StringToPointer("FirstName1"),
		LastName:  strutil.StringToPointer("LastName1"),
		Role:      strutil.StringToPointer(string(storage.UserRoleTypeModerator)),
	}

	// Act.
	body := testutil.EncodeRequest(t, input)

	server := httptest.NewServer(fixtures.router)
	status, response := testutil.MakeRequest(t, server, http.MethodPatch, fmt.Sprintf("/v1/users/%s", testutil.MustUUID(t)), &body)

	// Assert.
	expected := handler.Response{
		Errors: []handler.ResponseError{
			{
				Title: "user not found",
				Code:  handler.ErrNotFound,
			},
		},
	}

	require.EqualValues(t, expected, response)
	require.Equal(t, http.StatusNotFound, status)
}

func TestUpdateUserHandler_UpdateUserSuccess(t *testing.T) {
	fixtures := setupUserTests(t)
	defer fixtures.cleanup()

	// Arrange.
	user, err := fixtures.dbManager.CreateUser(context.Background(), storage.User{
		FirstName:    "FirstName1",
		LastName:     "LastName1",
		EmailAddress: "test1@starter.com",
		Role:         string(storage.UserRoleTypeModerator),
	})
	require.NoError(t, err)

	input := UserUpdateSchema{
		FirstName: strutil.StringToPointer("FirstName1"),
		LastName:  strutil.StringToPointer("LastName1"),
		Role:      strutil.StringToPointer(string(storage.UserRoleTypeModerator)),
	}

	// Act.
	body := testutil.EncodeRequest(t, input)

	server := httptest.NewServer(fixtures.router)
	status, response := testutil.MakeRequest(t, server, http.MethodPatch, fmt.Sprintf("/v1/users/%s", user.ID.String()), &body)

	// Assert.
	require.Empty(t, response.Errors)
	require.Equal(t, http.StatusOK, status)

	data := response.Data.(map[string]interface{})

	require.Equal(t, user.ID.String(), data["id"])
	require.Equal(t, strutil.StringPointerToString(input.FirstName), data["firstName"])
	require.Equal(t, strutil.StringPointerToString(input.LastName), data["lastName"])
	require.Equal(t, strutil.StringPointerToString(input.Role), data["role"])
	require.Equal(t, user.EmailAddress, data["email"])
}
