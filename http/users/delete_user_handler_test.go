package users

import (
	"context"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/require"

	"gitlab.mooncascade.net/roman/go-starter-kit/internal/handler"
	"gitlab.mooncascade.net/roman/go-starter-kit/internal/strutil"
	"gitlab.mooncascade.net/roman/go-starter-kit/internal/testutil"
	"gitlab.mooncascade.net/roman/go-starter-kit/storage"
)

func TestDeleteUserHandler_InvalidPathParamUserUUIDError(t *testing.T) {
	// Arrange.
	fixtures := setupUserTests(t)
	defer fixtures.cleanup()

	// Act.
	server := httptest.NewServer(fixtures.router)
	status, response := testutil.MakeRequest(
		t, server, http.MethodDelete, "/v1/users/invalid-UUID", nil,
	)

	// Assert.
	expected := handler.Response{
		Errors: []handler.ResponseError{
			{
				Title: "invalid user id: invalid UUID length: 12",
				Code:  handler.ErrBadInput,
			},
		},
	}

	require.EqualValues(t, expected, response)
	require.Equal(t, http.StatusBadRequest, status)
}

func TestDeleteUserHandler_UserNotFoundError(t *testing.T) {
	// Arrange.
	fixtures := setupUserTests(t)
	defer fixtures.cleanup()

	// Act.
	server := httptest.NewServer(fixtures.router)
	status, response := testutil.MakeRequest(
		t, server, http.MethodDelete, fmt.Sprintf("/v1/users/%s", testutil.MustUUID(t)), nil,
	)

	// Assert.
	expected := handler.Response{
		Errors: []handler.ResponseError{
			{
				Title: "user not found: row(s) not found",
				Code:  handler.ErrNotFound,
			},
		},
	}

	require.Equal(t, http.StatusNotFound, status)
	require.EqualValues(t, expected, response)
}

func TestDeleteUserHandler_DeleteUserSuccess(t *testing.T) {
	fixtures := setupUserTests(t)
	defer fixtures.cleanup()

	// Arrange.
	user, err := fixtures.dbManager.CreateUser(context.Background(), storage.User{
		FirstName:    "FirstName1",
		LastName:     "LastName1",
		EmailAddress: "test1@starter.com",
		Role:         string(storage.UserRoleTypeModerator),
	})
	require.NoError(t, err)
	userAuthSession := storage.UserAuthSession{
		UserID:  user.ID,
		OTPCode: strutil.StringToPointer("OTPCode1"),
	}
	_, err = fixtures.dbManager.UpsertUserAuthSession(context.Background(), userAuthSession)
	require.NoError(t, err)

	// Act.
	server := httptest.NewServer(fixtures.router)
	status, response := testutil.MakeRequest(
		t, server, http.MethodDelete, fmt.Sprintf("/v1/users/%s", user.ID.String()), nil,
	)

	// Assert.
	require.Equal(t, http.StatusNoContent, status)
	require.Empty(t, response.Errors)

	// Confirm user and user auth session cannot be read
	_, err = fixtures.dbManager.GetUserByUUID(context.Background(), user.ID)
	require.Equal(t, storage.ErrNotFound, err)
	_, err = fixtures.dbManager.GetUserAuthSessionByUserID(context.Background(), user.ID)
	require.Equal(t, storage.ErrNotFound, err)
}
