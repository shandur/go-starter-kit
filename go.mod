module gitlab.mooncascade.net/roman/go-starter-kit

go 1.16

require (
	github.com/Masterminds/squirrel v1.5.0
	github.com/aws/aws-sdk-go v1.38.12
	github.com/go-chi/chi/v5 v5.0.2
	github.com/go-chi/cors v1.2.0
	github.com/go-playground/validator/v10 v10.4.1
	github.com/gofrs/uuid v4.0.0+incompatible // indirect
	github.com/golang-migrate/migrate/v4 v4.14.1
	github.com/google/uuid v1.2.0
	github.com/hashicorp/errwrap v1.1.0 // indirect
	github.com/hashicorp/go-multierror v1.1.1 // indirect
	github.com/iancoleman/strcase v0.1.3
	github.com/jackc/pgx/v4 v4.11.0
	github.com/jmoiron/sqlx v1.3.1
	github.com/leodido/go-urn v1.2.1 // indirect
	github.com/lib/pq v1.10.0
	github.com/patrickmn/go-cache v2.1.0+incompatible
	github.com/pkg/errors v0.9.1
	github.com/plaid/go-envvar v1.1.0
	github.com/stretchr/testify v1.7.0
	go.uber.org/zap v1.16.0
	golang.org/x/sys v0.0.0-20210403161142-5e06dd20ab57 // indirect
	golang.org/x/text v0.3.6 // indirect
	golang.org/x/tools v0.0.0-20200825202427-b303f430e36d // indirect
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b
)
