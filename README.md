# platform

starter platform

## Local development requirements

* Go version 1.16.x; or
* [Docker](https://docs.docker.com/get-docker/)

## Makefile

Consists of useful commands to facilitate development process.

## Developing

`make tidy` - cleans and formats code.

`make lint` - runs **golangci-lint** over the app files.

`make up` - runs API & Postgres as Docker daemons.

`make down` - stops Docker services.

`make logs` - tails Docker logs.

`make psql` - launches psql, connected to Postgres running in Docker.

`make migration` - generates a migration with up/down files and timestamp as a filename prefix (use `make migration name=some_migration_name`).

`make test` - run tests. supports custom arguments e.g. `make test ARGS="-run TestProjectCRUD"`

`make run` - run API outside of Docker (i.e. `go run ...`).

### Running locally

There are two ways to run the API locally:

```
$ go run cmd/platform/*.go
```

For which you will need to export all the required environment variables.

Running via `make` is also supported, and all required environment variables will be set for you:

```
$ make run
```

### Running via docker

All environment variables are already configured in the `docker-compose.yml`. Once you build the api image, it will install
all dependencies.

To launch API with database, run:
```
$ make up
```

### Building docker image

```
$ docker build -f ./deploy/platform/Dockerfile -t starter/platform:latest .
```

### Import 3rd party packages

After you import your package, e.g., `go get -u github...`
Run

```
$ make tidy
```

#### Add dependency from private repo.

This is the real reason why we commit vendor.

Sometimes you would want to add private dependencies of your own that won't work.
Like this one:

```
go: finding module for package gitlab.mooncascade.net/roman/go-starter-kit-proto/go/api/v1
gitlab.mooncascade.net/roman/go-starter-kit/handler/grpc imports
        gitlab.mooncascade.net/roman/go-starter-kit-proto/go/api/v1: module gitlab.mooncascade.net/roman/go-starter-kit-proto/go/api: git ls-remote -q origin in /Users/kostkobohdan/bcgdv/pkg/mod/cache/vcs/7d6afc6e126daccfc8d0fe538118a63da0e164d4ca33954f9013b4b87719e38c: exit status 128:
        fatal: unable to access 'https://gitlab.com/starter.git/': Failed to connect to gitlab.com port 443: Operation timed out
```

To be able to add it locally, it's recommended to attach this configuration to allow you to add such dependencies to vendor.

```toml
git config --global url."git@gitlab.com:".insteadOf "https://gitlab.com/"
```

In case you encounter errors like the following when running `go list` or `go get`:

```
go: gitlab.mooncascade.net/roman/go-starter-kit-proto@v0.0.0-20200406035012-316b687feb9a: invalid version: git fetch -f origin refs/heads/*:refs/heads/* refs/tags/*:refs/tags/* in /Users/xiaoyi/go/pkg/mod/cache/vcs/7d6afc6e126daccfc8d0fe538118a63da0e164d4ca33954f9013b4b87719e38c: exit status 128:
	remote:
	remote: ========================================================================
	remote:
	remote: The project you were looking for could not be found.
	remote:
	remote: ========================================================================
	remote:
	fatal: Could not read from remote repository.
```

You will need to [generate a Personal Access Token on GitLab](https://gitlab.com/profile/personal_access_tokens) and add it to your `$HOME/.netrc` ([see this link for why](https://golang.org/doc/faq#git_https)):

```
machine gitlab.com login YOUR_EMAIL password YOUR_TOKEN
```

If the error persists, try removing cached mod files.

**Update dependency from private repo**

In case `go get -u` fails with a `410 Gone` error, set `GOPROXY` to direct and `GOSUMDB` to off:

```
GOPROXY=direct GOSUMDB=off go get -u gitlab.mooncascade.net/roman/go-starter-kit-proto
```

### Environment variables

Source of truth: [`.env/api-local.env`](./.env/api-local.env)

| Name                    | Local dev value            |
| ----------------------- | ---------------------------|
| ENVIRONMENT             | local                      |
| SERVICE_API_DOMAIN      | localhost                  |
| GRACE_SHUT_TIMEOUT_SECS | 0                          |
| DB_HOST                 | localhost                  |
| DB_PORT                 | 5432                       |
| DB_SSL                  | disable                    |
| DB_NAME                 | starter                    |
| DB_PASSWORD             | starter                    |
| DB_USERNAME             | starter                    |
| DB_MIGRATION_PATH       | storage/postgres/migration |
| DB_MIGRATION_VERSION    | <LATEST_MIGRATION_VERSION> |
| SSM_PATH                |                            |
| RUN_MIGRATION           | true                       |
| ENABLE_DEBUG_LOGS       | true                       |
| AUTH_MOCK_USER_ID       | <user id>                  |
| AUTH_MOCK_OTP_CODE      | 123123                     |


#### Using the VSCode

You can leverage the already preset environment variables that are committed to this repo.

You could've just run the application in the debug mode. That way you could also debug your application while you're developing it.

Just go to debug part of your VSCode and run it.

### Health endpoint

```
$ curl http://localhost:8080/v1/healthz
```

### Integration tests

```
$ make test
```

### Database migration

Database migration application for convenience is provided by the platform itself.
All you need to do to enable it is to run it in migration mode.
To enable it - provide `RUN_MIGRATION=true` as environment variable:
application will run migrations from provided path via `DB_MIGRATION_PATH` towards
specific version defined in `DB_MIGRATION_VERSION`.

Justification for this approach is to provide a way to have a separate process
specifically dedicated to run the database migrations without introducing separate image
or any of additional dependencies.

Migration version is a bunch of numbers before the first underscore in the migration filename.
I.e. in `20200414155849_add_class_name_and_icon_to_channels.up.sql` the version would be `20200414155849`.

When adding a new migration, remember to also update the version number in the `./internal/db/migration.go`.

### Default admin user

There is a default user being created on the first migration run.
You can find details in `insert_default_admin_user.up.sql` file.
If you want to have an admin user with your credentials for local testing, then update the file accordingly before running migrations (or run `make down up` to rebuilt the app from the scratch).

## Project structure

### Subpackages

```
cmd
```

Everything that is related to the starting the binary, like initialization, all the necessary setups and etc (according to the convention).

```
config
```

Application config and secret management.

```
deploy
```

All the necessary artifacts that are needed for deployment like Dockerfiles.

```
handler
```

Currently, all the HTTP handlers and related logic.

```
internal
```

All the internally used packages (according to the convention).

```
storage
```

Abstracted away persistance layer with the specific implementations.

```
swagger
```

Swagger/OpenAPI spec. Currently using version 2.0.
